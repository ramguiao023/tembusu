<!DOCTYPE html>
<html>
<head>
	<title>A participant joined your event</title>
</head>
<body>
	<h4>Dear {{ $event->contact_person }},</h4>
	
	<p>
		A participant joined your event. Here's the participant details:
	</p>

	<table>
		<tr>
			<td style="color: blue;">Participant:</td>
			<td style="color: blue;">
				{{ $participant->salutation . " " . $participant->first_name . " " . $participant->last_name }}
			</td>
		</tr>
		<tr>
			<td style="color: blue;">Birthdate:</td>
			<td style="color: blue;">
				{{ date("M d, Y", strtotime($participant->birthdate)) }}
			</td>
		</tr>
		<tr>
			<td style="color: blue;">Diet Preferences:</td>
			<td style="color: blue;">
				{{ $participant->diet_preferences }}
			</td>
		</tr>
		<tr>
			<td style="color: blue;">Diet Notes:</td>
			<td style="color: blue;">
				{{ $participant->diet_notes }}
			</td>
		</tr>
	</table>
</body>
</html>