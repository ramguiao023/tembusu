<!DOCTYPE html>
<html>
<head>
	<title>Confirmation</title>
</head>
<body>
	@php
		$content = str_replace("[event-name]", $event->title, $mail_content->content);

		$content = str_replace("[participant-salutation]", $participant->salutation, $content);
		$content = @str_replace("[participant-name]", $participant->name, $content);

		$banner_image = str_replace("http://localhost", 'https://legendrotor.com', $event->banner_image);

		$content = str_replace("[event-banner]", "<img src='".$banner_image."' />", $content);

		$date = "";

		if($event->end_date != "") {
			$date = $event->start_date . " to " . $event->end_date;
		} else {
			$date = $event->start_date;
		}

		$content = str_replace("[event-date]", $date, $content);

		$time = "";

		if($event->end_time != "") {
			$time = $event->start_time . " to " . $event->end_time;
		} else {
			$time = $event->start_time;
		}

		$content = str_replace("[event-time]", $time, $content);

		$credential = "Username: " . $participant->username . " <br />";
		$credential .= "Password: " . $participant->temporary_password . " <br /><br />";

		$content = str_replace("[participant-credential]", $credential, $content);

		$content = str_replace("[event-time]", $time, $content);

		$content = @str_replace("[event-venue]", $event->venue->name, $content);
		
		$content = str_replace("[event-contact-email]", $event->company->contact_email, $content);
		
		$content = str_replace("[event-reg-close]", $event->registration_closing_date, $content);
		
		if($participant->qrid != "") {
			$content = str_replace("[participant-qr]", "Here's your QR Code <small>(Please make sure to allow image in your email photo to display the image.)</small>: <br /><img src='https://api.qrserver.com/v1/create-qr-code/?data=123&amp;size=200x200' style='border: 1px solid black;' width='200' height='200' />", $content);
		}

		$url = "https://legendrotor.com/";

		$content = str_replace("[event-link]", "<a href='" . $url . "event-checker" . "/" . $event->id . "/". $participant->id . "'>here</a>", $content);
	@endphp

	{!! html_entity_decode($content) !!}
</body>
</html>