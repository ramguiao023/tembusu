<!DOCTYPE html>
<html>
<head>
	<title>Complete Registration</title>
</head>
<body>
	<h4>Hi, {{ $participant->salutation . " " . $participant->name }}!</h4>

	<p>
		Welcome to Tembusu Events! To complete your registration to your company ({{ $participant->company->name }}), please go to <a href="{{ url('/corp/'.$participant->company->slug) }}">{{ url('/corp/'.$participant->company->slug) }}</a> and fill-out the necessary fields.
	</p>

	<strong>Temporary Credentials:</strong>
	
	<table>
		<tr>
			<td>Username:</td>
			<td>{{ $participant->username }}</td>
		</tr>
		<tr>
			<td>Password:</td>
			<td>{{ $participant->temporary_password }}</td>
		</tr>
	</table>

	<br />
	
	<p>
		Thanks, <br />
		Tembusu Events Team
	</p>
</body>
</html>