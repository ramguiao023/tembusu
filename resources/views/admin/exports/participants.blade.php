<table>
    <thead>
    <tr>
        <th>Employee Number</th>
        <th>Name</th>
        <th>Birthdate</th>
        <th>Contact Number</th>
        <th>Email</th>
        <th>Username</th>
        <th>Diet Preferences</th>
        <th>Other Request</th>
        <th>QRID</th>
        <th>Join Date</th>
        <th>Answers</th>
        <th>Status</th>
        <th>Is VIP</th>
    </tr>
    </thead>
    <tbody>
    	@foreach($participants as $participant)
		    <tr>
		    	<td>
					{{ $participant->participant->employee_number }}
		    	</td>
		    	<td>
					{{ $participant->participant->salutation . " " .$participant->participant->name }}
		    	</td>
		    	<td>
					{{ $participant->participant->birthdate }}
		    	</td>
		    	<td>
					{{ $participant->participant->contact_number }}
		    	</td>
		    	<td>
					{{ $participant->participant->email }}
		    	</td>
		    	<td>
					{{ $participant->participant->username }}
		    	</td>
		    	<td>
					{{ $participant->participant->diet_preferences }}
		    	</td>
		    	<td>
					{{ $participant->participant->diet_notes }}
		    	</td>
		    	<td>
					{{ $participant->participant->qrid }}
		    	</td>
		    	<td>
					{{ $participant->created_at }}
		    	</td>
		    	<td>
		    		@php
		    			$ctr = 1;
		    		@endphp

		    		@foreach($participant->questions as $question)
						{{$ctr}}. {{ @$question->question }} <br />
						- {{ @$question->answer->answer }}
						<br />	
						<br />	
		    		@endforeach
		    	</td>
		    	<td>
		    		{!!
		    		    ($participant->status == 1)
		    		    ?
		    		        "GOING"
		    		    :
		    		        ""
		    		!!}

		    		{!!
		    		    ($participant->status == 2)
		    		    ?
		    		       "CHECKED-IN"
		    		    :
		    		         ""
		    		!!}

		    		{!!
		    		    ($participant->status == 0)
		    		    ?
		    		        "DECLINED"
		    		    :
		    		        ""
		    		!!}

		    		{!!
		    		    ($participant->status == 3)
		    		    ?
		    		        "PENDING"
		    		    :
		    		        ""
		    		!!}

		    		{!!
		    		    ($participant->status == 4)
		    		    ?
		    		        "EMAIL SENT"
		    		    :
		    		        ""
		    		!!}
		    	</td>
		    	<td>
		    		{{ $participant->participant->is_vip }}
		    	</td>
		    </tr>
    	@endforeach
    </tbody>
</table>