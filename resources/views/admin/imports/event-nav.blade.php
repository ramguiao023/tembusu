<?php
	$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

	$index = "";
	$images = "";
	$venue = "";
	$contacts = "";
	$lucky_draw = "";
	$participants = "";

	if(strpos($url, 'images')) {
		$images = "active";
	} else if(strpos($url, 'venue')) {
		$venue = "active";
	} else if(strpos($url, 'contacts')) {
		$contacts = "active";
	} else {
		$index = "active";
	}
?>

<a href="{{ url('admin/events/'.$event->id) }}" class="col-md-2">
	<div class="event-nav-item {{ $index }}">
		<h1>
			<i class="glyphicon glyphicon-th-list"></i>
		</h1>
		Basic Info
	</div>
</a>
<a href="{{ url('admin/events/'.$event->id.'/images') }}" class="col-md-2">
	<div class="event-nav-item {{ $images }}">
		<h1>
			<i class="wb-image"></i>
		</h1>
		Images
	</div>
</a>
<a href="{{ url('admin/events/'.$event->id.'/venue') }}" class="col-md-2">
	<div class="event-nav-item {{ $venue }}">
		<h1>
			<i class="glyphicon glyphicon-map-marker"></i>
		</h1>
		Venue
	</div>
</a>
<a href="{{ url('admin/events/'.$event->id.'/contacts') }}" class="col-md-2">
	<div class="event-nav-item {{ $contacts }}">
		<h1>
			<i class="glyphicon glyphicon-phone-alt"></i>
		</h1>
		Contacts
	</div>
</a>
<a href="{{ url('admin/events/'.$event->id.'/lucky-draw') }}" class="col-md-2">
    <div class="event-nav-item">
        <h1>
            <i class="fas fa-star"></i>
        </h1>
        Lucky Draw
    </div>
</a>
<a href="{{ url('admin/events/'.$event->id.'/participants') }}" class="col-md-2">
    <div class="event-nav-item">
        <h1>
            <i class="fas fa-users"></i>
        </h1>
        Participants
    </div>
</a>