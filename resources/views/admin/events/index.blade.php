@extends('admin.layouts.master')

@section('title', 'Events')

@section('style')
    @include('admin.imports.datatable-styles')
@stop

@section('content')
    <div class="float-left">
        <h2>Events</h2>
        <p>Manage your events</p>
    </div>

    <a href="{{ url('admin/events/create') }}" class="btn btn-success float-right" title="Add New User">
        <i class="icon glyphicon glyphicon-plus"></i> Create New Event
    </a>

    <div class="clearfix"></div>

    <div class="panel">
    	<header class="panel-heading">
    	  <div class="panel-actions">
    	  	
    	  </div>   
    	  <h3 class="panel-title"></h3>
    	</header>

        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success alert-icon alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="icon wb-check" aria-hidden="true"></i>
                    <h4>Nice!</h4>
                    <p>
                        {{ session('success') }}
                    </p>
                </div>
            @endif

            <table class="table table-hover dataTable table-striped w-full" id="eventTable">
                <thead>
                    <tr>
                        <th>Event</th>
                        <th>Company</th>
                        <th>Event Contact</th>
                        <th>Created by</th>
                        <th>Venue</th>
                        <th>Event Date</th>
                        <th>Status</th>
                        <th width="240">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Event</th>
                        <th>Company</th>
                        <th>Event Contact</th>
                        <th>Created by</th>
                        <th>Venue</th>
                        <th>Event Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($events as $event)
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal{{ $event->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel{{ $event->id }}">What do you want to do?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <a href="{{ url('admin/events/'.$event->id.'/'.'invite/'.$event->company->id) }}" class="btn btn-lg btn-info block" style="color: #FFF;">
                                            <i class="icon glyphicon glyphicon-send"></i> Send Event Invitation
                                        </a>
                                        <p>Send event invitation to employee of the company</p>
                                    </div>
                                    <div class="form-group">
                                            <a href="{{ url('admin/events/'.$event->id.'/'.'participants/') }}" class="btn btn-lg btn-danger block" style="color: #FFF">
                                                <i class="icon fas fa-users"></i> See Participants List
                                            </a>
                                            <p>The list of all invited participants</p>
                                    </div>
                                    <div class="form-group">
                                            <a href="{{ url('admin/events/'.$event->id.'/'.'mails/') }}" class="btn btn-lg btn-success block" style="color: #FFF">
                                                <i class="icon fas fa-envelope"></i>
                                                Manage Emails
                                            </a>
                                            <p>Manage email content for your event</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <a href="{{ url('admin/events/'.$event->id.'/'.'remind/'.$event->company->id) }}" class="btn btn-lg btn-default block">
                                            <i class="icon fas fa-send"></i> Send Reminder Email
                                        </a>
                                        <p>
                                            Event email reminder
                                        </p>
                                    </div>

                                    <div class="form-group">
                                        <a href="{{ url('admin/events/'.$event->id.'/questions') }}" class="btn btn-lg btn-primary block" style="color: #FFF;">
                                            <i class="icon fas fa-question"></i> Generate Questions
                                        </a>
                                        <p>
                                            Manage event's question for the event
                                        </p>
                                    </div>

                                    <div class="form-group">
                                        <a href="{{ url('admin/events/'.$event->id.'/lucky-draw') }}" class="btn btn-lg btn-warning block" style="color: #FFF;">
                                            <i class="icon fas fa-star"></i> Manage Lucky Draw
                                        </a>
                                        <p>
                                            Manage lucky draws of the event
                                        </p>
                                    </div>
                                </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <tr>
                        <td>
                            {{ $event->title }}
                        </td>
                        <td>
                            {{ $event->company->name }}
                        </td>
                        <td>
                            {{ $event->contact_person }}
                        </td>
                        <td>
                            {{ $event->user->name }}
                        </td>
                        <td>
                            {{ @$event->venue->name }}
                        </td>
                        <td>
                            {!!
                                ($event->start_date) ? date('M d, Y', strtotime($event->start_date)) : "" 
                            !!}
                            {!!
                                ($event->end_date) ? " <strong> - to - </strong> " . date('M d, Y', strtotime($event->end_date)) : ""
                            !!}

                            @if(is_event_happening($event->start_date, $event->end_date) == "1")
                                <h4><span class='badge badge-md badge-success'>Currently Happening</span></h4>
                            @elseif(is_event_happening($event->start_date, $event->end_date) == "2")
                                <h4><span class='badge badge-md badge-dark'>Past Event</span></h4>
                            @elseif(is_event_happening($event->start_date, $event->end_date) == "0")
                                <h4><span class='badge sbadge-md badge-warning'>Upcoming Event</span></h4>
                            @else
                                <h4><span class='badge badge-md badge-danger'>No Dates Yet</span></h4>
                            @endif
                        </td>
                        <td>
                            @if($event->status == 1)
                                <h4><span class='badge badge-md badge-success'>Active</span></h4>
                            @elseif($event->status == 0)
                                <h4><span class='badge badge-md badge-danger'>Disabled</span></h4>
                            @endif
                        </td>
                        <td>
                            <a href="{{ url('admin/events/'.$event->id) }}" class="btn btn-primary">
                                <i class="icon glyphicon glyphicon-edit"></i> Edit
                            </a>
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal{{ $event->id }}">
                                <i class="icon glyphicon glyphicon-cog"></i> More Actions
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
          </table>
        </div>
    </div>
@stop

@section('script')
    <script type="text/javascript" src="{{ asset('admin_assets/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin_assets/global/js/Plugin/datatables.js') }}"></script>

    @include('admin.imports.datatable-scripts')

    <script type="text/javascript">
        $("#eventTable").DataTable({
            "aaSorting": []
        });
    </script>
@stop