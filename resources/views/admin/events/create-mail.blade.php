@extends('admin.layouts.master')

@section('title', 'Events')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ url('admin_assets/assets/js/editor/dist/ui/trumbowyg.min.css') }}">
    @include('admin.imports.datatable-styles')
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('content')
    <div class="float-left">
        <h3>Manage Emails of {{ $event->title }}</h3>
        <br />
    </div>

    <div class="float-right">
    	<a href="{{ url('admin/events/'.$event->id.'/mails') }}" class="btn btn-default">
            <i class="icon fas fa-chevron-left"></i> Go Back
        </a>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-8">
            <div class="panel">
                <header class="panel-heading">
                  <div class="panel-actions">
                    
                  </div>   
                  <h3 class="panel-title"></h3>
                </header>

                <div class="panel-body">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger alert-icon alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <i class="icon wb-warning" aria-hidden="true"></i>
                            <h4>Uh oh!</h4>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ url('admin/events/'.$event->id.'/mails') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="event_id" value="{{ $event->id }}" />
                        <div class="form-group">
                            <label>Name/Title <span class="required">*</span></label>
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Subject <span class="required">*</span></label>
                            <input type="text" name="subject" value="{{ old('subject') }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Dynamic Fields</label>
                            <div class="alert alert-warning">
                                You can use this fields to have a dynamic field which will be based on the actual data from the database. Just click the button below to copy to clipboard and paste it to textarea below.
                            </div>
                            <span class="btn btn-primary m-3" onclick="copyToClipboard('[event-name]')">[event-name]</span>
                            <span class="btn btn-primary m-3" onclick="copyToClipboard('[event-banner]')">[event-banner]</span>
                            <span class="btn btn-primary m-3" onclick="copyToClipboard('[event-date]')">[event-date]</span>
                            <span class="btn btn-primary m-3" onclick="copyToClipboard('[event-time]')">[event-time]</span>
                            <span class="btn btn-primary m-3" onclick="copyToClipboard('[event-venue]')">[event-venue]</span>
                            <span class="btn btn-primary m-3" onclick="copyToClipboard('[event-link]')">[event-link]</span>
                            <span class="btn btn-primary m-3" onclick="copyToClipboard('[event-reg-close]')">[event-reg-close]</span>
                            <span class="btn btn-primary m-3" onclick="copyToClipboard('[event-contact-email]')">[event-contact-email]</span>
                            <span class="btn btn-primary m-3" onclick="copyToClipboard('[participant-salutation]')">[participant-salutation]</span>
                            <span class="btn btn-primary m-3" onclick="copyToClipboard('[participant-name]')">[participant-name]</span>

                            <hr />

                            <textarea id="trumbowyg" name="content">{{ old('content') }}</textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                <i class="fas fa-save icon"></i> Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>  
        </div>
    </div>
@stop

@section('script')
    <script src="{{ asset('admin_assets/global/vendor/alertify/alertify.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/notie/notie.js') }}"></script>

    <script src="{{ asset('admin_assets/global/js/Plugin/alertify.js') }}"></script>
    <script src="{{ asset('admin_assets/global/js/Plugin/notie-js.js') }}"></script>
    <script type="text/javascript" src="{{ url('/admin_assets/assets/js/editor/dist/trumbowyg.min.js') }}"></script>
    <script type="text/javascript">
        $('#trumbowyg').trumbowyg();

        function copyToClipboard(string) {
          /* Get the text field */
          $("#textToCopy").val(string);

          var copyText = document.getElementById("textToCopy");

          /* Select the text field */
          copyText.select();
          copyText.setSelectionRange(0, 99999); /*For mobile devices*/

          /* Copy the text inside the text field */
          document.execCommand("copy");

          /* Alert the copied text */
          // alert("Copied the text: " + copyText.value);
          alertify.success('Copied to clipboard: ' + copyText.value);
        }
    </script>
@stop