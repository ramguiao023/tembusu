@extends('admin.layouts.master')

@section('title', 'Update Event images')
@section('style')
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/dropify/dropify.css') }}">

    <style type="text/css">
        /* Always set the map height explicitly to define the size of the div
        * element that contains the map. */
        #map {
            height: 300px;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #description {
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
        }

        #infowindow-content .title {
            font-weight: bold;
        }

        #infowindow-content {
            display: none;
        }

        #map #infowindow-content {
            display: inline;
        }

        .pac-card {
            margin: 10px 10px 0 0;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            background-color: #fff;
            font-family: Roboto;
        }

        #pac-container {
            padding-bottom: 12px;
            margin-right: 12px;
        }

        .pac-controls {
            display: inline-block;
            padding: 5px 11px;
        }

        .pac-controls label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 400px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        #title {
            color: #fff;
            background-color: #4d90fe;
            font-size: 15px;
            font-weight: 500;
            padding: 6px 12px;
        }
    </style>
@stop

@section('content')
    <div class="row mb-10">
        <div class="col-md-8">
            <div class="container-fluid">
                <div class="row">
                    @include('admin.imports.event-nav')
                </div>          
            </div>
        </div>
        <div class="col-md-4 text-right">
            <a href="{{ url('admin/events') }}" class="btn btn-default" title="Add New User">
                <i class="icon glyphicon glyphicon-chevron-left"></i>
                Go Back
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            @if(session('success'))
                <div class="alert alert-success">
                    <h3>Nice!</h3>
                    {{ session('success') }}
                </div>
            @endif
            <div class="panel">
                <header class="panel-heading">
                  <div class="panel-actions">
                    
                  </div>   
                  <h3 class="panel-title">
                    <div class="float-left">
                        Update Event Venue (Part 3)
                        <br />
                        <small>Set the location of the event</small>
                    </div>

                    <div class="float-right">
                    </div>
                    <div class="clearfix"></div>
                  </h3>
                </header>
                <div class="panel-body">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger alert-icon alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <i class="icon wb-warning" aria-hidden="true"></i>
                            <h4>Uh oh!</h4>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ url('admin/events/'.$event->id) }}" method="POST" autocomplete="off" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @method('PUT')

                        <input type="hidden" name="event_id" id="event_id" value="{{ $event->id }}">
                        <input type="hidden" name="type" value="venue" />

                        <div class="form-group">
                            @if(@$_GET['venue_id'])
                            <select name="venue_id" class="form-control" id="venue_id">
                                <option value="">Choose Venue of the Event</option>
                                @foreach($venues as $venue)
                                    <option 
                                        {{ 
                                            (
                                                (@$_GET['venue_id']==$venue->id)
                                            ) 

                                            ? 
                                            "selected='selected'"
                                            :
                                            "" 
                                        }} 

                                        value="{{ $venue->id }}" data-lat='{{ $venue->lat }}' data-lng='{{ $venue->lng }}'>{{ $venue->name }}</option>
                                @endforeach
                            </select>
                            @else
                            <select name="venue_id" class="form-control" id="venue_id">
                                <option value="">Choose Venue of the Event</option>
                                @foreach($venues as $venue)
                                    <option 
                                        {{ 
                                            (
                                                ($venue->id == $event->venue_id)
                                            ) 

                                            ? 
                                            "selected='selected'"
                                            :
                                            "" 
                                        }} 

                                        value="{{ $venue->id }}" data-lat='{{ $venue->lat }}' data-lng='{{ $venue->lng }}'>{{ $venue->name }}</option>
                                @endforeach
                            </select>
                            @endif
                        </div>


                        @if((@$_GET['long'] and $_GET['lat']))
                        <div class="form-group">
                            <iframe 
                              width="100%" 
                              height="300" 
                              frameborder="0" 
                              scrolling="no" 
                              marginheight="0" 
                              marginwidth="0"
                              id="venue-map" 
                              src="https://www.google.com/maps?q={{ @$_GET['lat'] }},{{ @$_GET['long'] }}&hl=es;z%3D14&amp;output=embed"
                             >
                             </iframe>
                        </div>
                        @elseif($event->venue_id)
                        <div class="form-group">
                            <iframe 
                              width="100%" 
                              height="300" 
                              frameborder="0" 
                              scrolling="no" 
                              marginheight="0" 
                              marginwidth="0"
                              id="venue-map" 
                              src="https://www.google.com/maps?q={{ @$event->venue->lat }},{{ @$event->venue->lng }}&hl=es;z%3D14&amp;output=embed"
                             >
                             </iframe>
                        </div>
                        @endif
                        
                        <div class="form-group text-right">
                            <button type="submit" name="create" class="btn btn-primary">
                                <i class="icon glyphicon glyphicon-floppy-save"></i>
                                Update Event Venue
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script src="{{ asset('admin_assets/global/vendor/jquery-ui/jquery-ui.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-tmpl/tmpl.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-load-image/load-image.all.min.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-process.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-image.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-audio.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-video.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-validate.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-ui.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/dropify/dropify.min.js') }}"></script>

    <script type="text/javascript">
        $("#venue_id").change(function() {
            var url = $("#baseurl").attr('content');

            var event_id = $("#event_id").val(),
                venue_id = $(this).val(),
                long = $(this).children('option:selected').data('lng'),
                lat = $(this).children('option:selected').data('lat');

            window.location = url + "/admin/events/"+event_id+"/venue?long="+long+"&lat="+lat+"&venue_id="+venue_id;
        });
    </script>
@stop