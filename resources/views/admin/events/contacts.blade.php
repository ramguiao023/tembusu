@extends('admin.layouts.master')

@section('title', 'Update Event images')

@section('style')
   
@stop

@section('content')
    <div class="row mb-10">
        <div class="col-md-8">
            <div class="container-fluid">
                <div class="row">
                    @include('admin.imports.event-nav')
                </div>          
            </div>
        </div>
        <div class="col-md-4 text-right">
            <a href="{{ url('admin/events') }}" class="btn btn-default" title="Add New User">
                <i class="icon glyphicon glyphicon-chevron-left"></i>
                Go Back
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            @if(session('success'))
                <div class="alert alert-success">
                    <h3>Nice!</h3>
                    {{ session('success') }}
                </div>
            @endif
            <div class="panel">
                <header class="panel-heading">
                  <div class="panel-actions">
                    
                  </div>   
                  <h3 class="panel-title">
                    <div class="float-left">
                        Update Event's Contact (Part 4)
                        <br />
                        <small>Set the contact information of the event</small>
                    </div>

                    <div class="float-right">
                    </div>
                    <div class="clearfix"></div>
                  </h3>
                </header>
                <div class="panel-body">
                    <div id="error-details" class="alert alert-danger alert-icon alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <i class="icon wb-warning" aria-hidden="true"></i>
                        <h4>Uh oh!</h4>
                        <ul>
                            <li>Please complete all the contact details.</li>
                        </ul>
                    </div>

                    <div id="success-details" class="alert alert-success alert-icon alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <i class="icon wb-warning" aria-hidden="true"></i>

                        <h4>Nice!</h4>

                        <ul>
                            <li>Saved successfully.</li>
                        </ul>
                    </div>

                    <form id="update-contact" action="{{ url('admin/events/'.$event->id) }}" method="POST" autocomplete="off" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @method('PUT')
                        
                        <input type="hidden" id="event_id" value="{{ $event->id }}" />
                        <input type="hidden" name="type" value="contact">
                        
                        <div id="contact-forms">
                            @foreach($event->contacts as $contact)
                                
                                <div>
                                    <div class="form-group">
                                        <h4 class="float-left">Contact Info</h4>

                                        <a href="javascript:void(0)" class="btn btn-sm btn-icon btn-danger float-right delete-contact">
                                            <i class="fas fa-times"></i>
                                        </a>

                                        <div class="clearfix"></div>

                                        <label>
                                            Event Contact Person <span class="required">*</span>
                                        </label>

                                        <input type="text" name="contact_person" value="{{ $contact->contact_person }}" class="contact_person form-control" placeholder="Who is the contact person for this event?">
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>
                                                    Contact Person's Email Address <span class="required">*</span>
                                                </label>

                                                <input type="text" name="contact_person_email" placeholder="contact@example.com" value="{{ $contact->contact_person_email }}" class="contact_person_email form-control">
                                            </div>

                                            <div class="col-md-6">
                                                <label>
                                                    Contact Person's Contact Number
                                                    <span class="required">*</span>
                                                </label>

                                                <input type="text" value="{{ $contact->contact_person_number }}" name="contact_person_number" placeholder="### ### ###" class="contact_person_number form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <hr><br>

                                </div>
                            @endforeach
                        </div>

                        <a href="javascript:void(0)" class="btn btn-icon btn-default" id="add-contact">
                            <i class="fas fa-plus"></i>
                        </a>

                        <div class="form-group text-right">
                            <button type="submit" name="create" class="btn btn-primary">
                                <i class="icon glyphicon glyphicon-floppy-save"></i>
                                Update Event Contact
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script type="text/javascript">
        $("#add-contact").click(function() {
            var html = '<div><div class="form-group">' +
                            '<h4 class="float-left">Contact Info</h4>' + 
                            '<a href="javascript:void(0)" class="btn btn-sm btn-icon btn-danger float-right delete-contact"><i class="fas fa-times"></i></a><div class="clearfix"></div>' + 
                            '<label>Event Contact Person <span class="required">*</span></label>' +
                           '<input type="text" name="contact_person" value="" class="contact_person form-control" placeholder="Who is the contact person for this event?" />' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<div class="row">' +
                                '<div class="col-md-6">' +
                                    '<label>Contact Person\'s Email Address <span class="required">*</span></label>' +
                                    '<input type="text" name="contact_person_email" placeholder="contact@example.com" value="" class="contact_person_email form-control" />' +
                                '</div>' +
                                '<div class="col-md-6">' +
                                    '<label>Contact Person\'s Contact Number <span class="required">*</span></label>' +
                                    '<input type="text" value="" name="contact_person_number" placeholder="### ### ###" class="contact_person_number form-control" />' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<hr /><br /></div>';

            $("#contact-forms").append(html);
        });

        $(document).on('click', ".delete-contact", function() {
            $(this).parent().parent().remove();
        });

        $("#update-contact").submit(function() {
            var url = $("#baseurl").attr('content');

            var event_id = $("#event_id").val();

            var contact_person = [],
                contact_person_email = [],
                contact_person_number = [];

            var errors = 0;

            $(".contact_person").each(function() {
                var this_value = $(this);

                if(this_value.val() == "") {
                    errors = 1;
                } else {
                    contact_person.push(this_value.val());
                }
            });

            $(".contact_person_email").each(function() {
                var this_value = $(this);
                if(this_value.val() == "") {
                    errors = 1;
                } else {
                    contact_person_email.push(this_value.val());
                }
            });

            $(".contact_person_number").each(function() {
                var this_value = $(this);
                if(this_value.val() == "") {
                    errors = 1;
                } else {
                    contact_person_number.push(this_value.val());
                }
            });

            var data = [];

            data.push(contact_person, contact_person_email, contact_person_number);

            if(errors == 1) {
                $("#error-details").show();
                $("#success-details").hide();
            } else {
                $("#error-details").hide();
                $("#success-details").show();

                $.post(url + "/admin/events/" + event_id + "/update-contacts", { "_token": $("#token").attr('content'), data }, function(data) {
                    console.log(data);
                });
            }

            window.scrollTo(0, 0);

            return false;
        });
    </script>
@stop