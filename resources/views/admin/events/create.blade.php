@extends('admin.layouts.master')

@section('title', 'Create New Event')

@section('style')
    @include('admin.imports.datepicker-styles')
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('content')
    <div class="float-left">
        <h2>Create New Event (Part 1)</h2>
        <p>Have an upcoming event for your company?</p>
    </div>

    <a href="{{ url('admin/events') }}" class="btn btn-default float-right" title="Add New User">
        <i class="icon glyphicon glyphicon-chevron-left"></i>
        Go Back
    </a>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger alert-icon alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <i class="icon wb-warning" aria-hidden="true"></i>
                            <h4>Uh oh!</h4>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ url('admin/events') }}" method="POST" autocomplete="off">
                        {{ csrf_field() }}
                        <input type="hidden" name="created_by" value="{{ Auth::user()->id }}" />
                        <div class="form-group">
                            <label>Title <span class="required">*</span></label>
                            <input type="text" name="title" value="{{ old('title') }}" placeholder="Ex: Coolest Event Ever 2019" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Caption <span class="required">*</span></label>
                            <input type="text" name="caption" value="{{ old('caption') }}" placeholder="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Write-up <span class="required">*</span></label>
                            <input type="text" name="write_up" value="{{ old('write_up') }}" placeholder="" class="form-control">
                        </div>
                        <div id="basicExample" class="form-group">
                            <h4>From:</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Start Date: <span class="required">*</span></label>
                                        <input type="text" name="start_date" value="{{ old('start_date') }}" class="date start form-control" />
                                    </div>
                                    <div class="col-md-6">
                                        <label>Start Time:</label>
                                         <input type="text" name="start_time" value="{{ old('start_time') }}" name="start_time" class="time start form-control" />
                                    </div>
                                </div>
                            </div>
                            <h4>To:</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>End Date: <span class="required">*</span></label>
                                         <input type="text" name="end_date" value="{{ old('end_date') }}" class="date end form-control" />
                                    </div>
                                    <div class="col-md-6">
                                        <label>End Time:</label>
                                        <input type="text" value="{{ old('end_time') }}" name="end_time" class="time end form-control" />
                                    </div>
                                </div>
                            </div>
                            <h4>Registation Closing Date:</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Closing Date: <span class="required">*</span></label>
                                         <input type="text" name="registration_closing_date" value="{{ old('registration_closing_date') }}" class="date form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Company <span class="required">*</span></label>

                                    @if(Auth::user()->hasRole('superadmin')) 
                                    <select name="company_id" class="form-control">
                                        <option value="">Choose Company</option>
                                        @foreach($companies as $company)
                                        <option {{ ($company->id == old('company_id')) ? "selected='selected'" : "" }} value="{{ $company->id }}">{{ $company->name }}</option>
                                        @endforeach
                                    </select>
                                    @endif

                                    @if(!Auth::user()->hasRole('superadmin')) 
                                    <input type="hidden" name="company_id" value="{{ Auth::user()->company_id }}" />

                                    <strong>{{ @Auth::user()->company->name }}</strong>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label>Type <span class="required">*</span></label>
                                    <select name="event_type" class="form-control">
                                        <option value="">Choose Type</option>
                                        @foreach($types as $type)
                                            <option {{ (old("event_type") == $type) ? "selected='selected'" : "" }} value="{{ $type }}">{{ $type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Access Type: <span class="required">*</span></label>
                                    <select name="access_type" class="form-control">
                                        <option {{ (old('access_type') == 1) ? "selected='selected'" : "" }} value="1">Login with Username & Password</option>
                                        <option {{ (old('access_type') == 2) ? "selected='selected'" : "" }} value="2">Verify participant's information</option>
                                        <option {{ (old('access_type') == 3) ? "selected='selected'" : "" }} value="3">Login without any verification</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button type="submit" name="create" class="btn btn-primary">
                                <i class="icon glyphicon glyphicon-floppy-save"></i>
                                Create Event
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    @include('admin.imports.datepicker-scripts')

    <script type="text/javascript">
        // initialize input widgets first
        $('#basicExample .time').timepicker({
            'showDuration': true,
            'timeFormat': 'g:ia'
        });

        $('#basicExample .date').datepicker({
            'format': 'yyyy-mm-dd',
            'autoclose': true
        });

        // initialize datepair
        var basicExampleEl = document.getElementById('basicExample');
        var datepair = new Datepair(basicExampleEl);

        $("#start_date").change(function() {
            var startDateValue = $(this).val(),
                dateParsed = Date.parse(startDateValue).toString("yyyy-MM-dd");

            $("input[name='start_date'").val(dateParsed);
        });

        $("#end_date").change(function() {
            var startDateValue = $(this).val(),
                dateParsed = Date.parse(startDateValue).toString("yyyy-MM-dd");

            $("input[name='end_date'").val(dateParsed);
        });

        $("#show-date").click(function() {
            if($(this).is(":checked")) {
                $("#date-picker").show();

                $("input[name='start_date']").removeAttr('disabled');
                $("#start_date").removeAttr('disabled');
                $("input[name='start_time']").removeAttr('disabled');

                $("input[name='end_date']").removeAttr('disabled');
                $("#end_date").removeAttr('disabled');
                $("input[name='end_time']").removeAttr('disabled');

            } else {
                $("#date-picker").hide();

                $("input[name='start_date']").addAttr('disabled');
                $("#start_date").addAttr('disabled');
                $("input[name='start_time']").addAttr('disabled');

                $("input[name='end_date']").addAttr('disabled');
                $("#end_date").addAttr('disabled');
                $("input[name='end_time']").addAttr('disabled');
            }
        });
    </script>
@stop