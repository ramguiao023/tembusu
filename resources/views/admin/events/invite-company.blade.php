@extends('admin.layouts.master')

@section('title', 'Invite Participants')

@section('style')
    @include('admin.imports.datatable-styles')
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
@stop

@section('content')
<div class="preloader-wrap">
    <div class="preloader">
        <h4>Please Wait...</h4>
        <h1>Sending Event Invite to Participants</h1>
    </div>
</div>
    <div class="float-left">
        <h2>Invite Participants</h2>
        <p>Manage Your Invitations</p>
    </div>

    <a href="{{ url('admin/events') }}" class="btn btn-default float-right" title="Go Back to Event Listing">
        <i class="icon glyphicon glyphicon-chevron-left"></i> Go Back
    </a>

    <div class="clearfix"></div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <header class="panel-heading">
                  <div class="panel-actions">
                    
                  </div>   
                  <h3 class="panel-title"></h3>
                </header>

                <div class="panel-body">
                    @if(session('success'))
                        <div class="alert alert-success alert-icon alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <i class="icon wb-check" aria-hidden="true"></i>
                            <h4>Nice!</h4>
                            <p>
                                {{ session('success') }}
                            </p>
                        </div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-danger alert-icon alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <h4>Uh oh!</h4>
                            <p>
                                {{ session('error') }}
                            </p>
                        </div>
                    @endif
            
                    <form id="invite_form" method="POST" action="{{ url("admin/events/invite") }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <h3>{{ $event->title }} for {{ $company->name }}</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" style="display: none;">
                                    <label>To which company will you send the invite?</label>
                                    <input type="hidden" name="event_id" id="event_id" value="{{ $event->id }}" />
                                    <select name="company_id" id="company_id" class="form-control">
                                        <option value="">Choose a Company</option>
                                        @foreach($companies as $company)
                                            <option {{ ($company->id == $company_id) ? "selected='selected'" : ""}} value="{{ $company->id }}">{{ $company->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <h4>Send invite to the whole company?</h4>
                            
                            <label>
                                <input type="checkbox" value="1" name="send_to_company_wide" checked="" id="send_to_company_wide" /> Yes
                            </label>
                        </div>

                        <div id="participant-table">
                            <div class="alert alert-warning">
                                <h4>Note</h4>
                                <p>Choose an employee to send the event invite.</p>
                            </div>
                            <div id="participant-form"></div>

                            <table class="table" id="eventTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Employee Number</th>
                                        <th>Employee</th>
                                        <th>Email</th>
                                        <th>Company</th>
                                        <th>Department</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($participants as $participant)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="participants" value="{{ $participant->participant_id }}" />
                                            </td>
                                            <td>
                                                {{ $participant->participant->employee_number }}
                                            </td>
                                            <td>
                                                {{ $participant->participant->salutation . " " . $participant->participant->name  }}
                                            </td>
                                            <td>
                                                {{ $participant->participant->email }}
                                            </td>
                                            <td>
                                                {{ $participant->participant->company->name }}
                                            </td>
                                            <td>
                                                {{ @$participant->participant->department->name }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        
                        <div class="form-group">
                            <a href="javascript:void(0)" id="send-invite" class="btn btn-primary">
                                <i class="icon glyphicon glyphicon-send"></i>
                                Send Invite
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script type="text/javascript" src="{{ asset('admin_assets/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin_assets/global/js/Plugin/datatables.js') }}"></script>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>

    @include('admin.imports.datatable-scripts')

    <script type="text/javascript">
        $("#eventTable").DataTable();
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#company_id").change(function() {
                var value = $(this).val(),
                    url = $("#baseurl").attr('content'),
                    event_id = $("#event_id").val();

                window.location = url + "/admin/events/" + event_id + "/invite/" + value;
            });

            $("#send_to_company_wide").click(function() {
                var self = $(this);

                if(self.prop('checked') == true) {
                    $("#participant-table").hide();
                } else {
                    $("#participant-table").show();
                }
            });

            $("#send-invite").click(function() {
                $(".preloader-wrap").show();
                $("#invite_form").submit();
            });

            $(document).on('click', '.participants', function() {
                var self = $(this);

                if(self.prop("checked") == true){
                    $("#participant-form").append("<input type='hidden' name='participants[]' id='participant"+self.val()+"' value='"+self.val()+"' />");
                } else {
                    $("#participant"+self.val()).remove();
                }

                
            });
        });
    </script>
@stop