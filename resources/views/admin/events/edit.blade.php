@extends('admin.layouts.master')

@section('title', 'Edit Event')

@section('style')
	@include('admin.imports.datepicker-styles')
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('content')
	<div class="row mb-10">
		<div class="col-md-10">
			<div class="container-fluid">
				<div class="row">
					@include('admin.imports.event-nav')
				</div>			
			</div>
		</div>
		<div class="col-md-2 text-right">
			<a href="{{ url('admin/events') }}" class="btn btn-default" title="Add New User">
			    <i class="icon glyphicon glyphicon-chevron-left"></i>
			    Go Back
			</a>
		</div>
	</div>

    <div class="row">
        <div class="col-md-10">
        	@if(session('success'))
        		<div class="alert alert-success">
        			<h3>Nice!</h3>
        			{{ session('success') }}
        		</div>
        	@endif
            <div class="panel">
            	<header class="panel-heading">
            	  <div class="panel-actions">
            	  	
            	  </div>   
            	  <h3 class="panel-title">
            	  	<div class="float-left">
            	  		Update Event (Part 1)
            	  		<br />
            	  		<small>Basic Event Information</small>
            	  	</div>

            	  	<div class="float-right">
            	  		@if($event->status == 1)
                            <a href="javascript:void(0)" data-id='{{ $event->id }}' class="btn btn-default disable-event float-left">
                                <i class="icon glyphicon glyphicon-remove"></i>
                                Disable Event
                            </a>
                        @endif

                        @if($event->status == 0)
                            <a href="javascript:void(0)" data-id='{{ $event->id }}' class="btn btn-success activate-event float-left">
                                <i class="icon glyphicon glyphicon-check"></i>
                                Activate Event
                            </a>
                        @endif
            	  	</div>
            	  	<div class="clearfix"></div>
            	  </h3>
            	</header>
                <div class="panel-body">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger alert-icon alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <i class="icon wb-warning" aria-hidden="true"></i>
                            <h4>Uh oh!</h4>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ url('admin/events/'.$event->id) }}" method="post" autocomplete="off">
                        {{ csrf_field() }}

                        @method('PUT')

                        <input type="hidden" name="created_by" value="{{ Auth::user()->id }}" />
                        <input type="hidden" name="type" value="editbasic" />
                        <div class="row">
                            <div class="col-md-6">
                                @if(is_event_happening($event->start_date, $event->end_date) == "1")
                                    <h4><span class='badge badge-md badge-success'>Currently Happening</span></h4>
                                @elseif(is_event_happening($event->start_date, $event->end_date) == "2")
                                    <h4><span class='badge badge-md badge-dark'>Passed Event</span></h4>
                                @elseif(is_event_happening($event->start_date, $event->end_date) == "0")
                                    <h4><span class='badge sbadge-md badge-warning'>Upcoming Event</span></h4>
                                @else
                                    <h4><span class='badge badge-md badge-danger'>Inactive (No Dates)</span></h4>
                                @endif
                            </div>
                        </div>
                        <br />
                        <div id="basicExample" class="form-group">
                        	<h4>From:</h4>
                        	<div class="form-group">
	                        	<div class="row">
	                        		<div class="col-md-6">
	                        			<label>Start Date:</label>
	                        			<input type="text" name="start_date" value="{{ ($event->start_date) ? date('Y-m-d', strtotime($event->start_date)) : '' }}" class="date start form-control" />
	                        		</div>
	                        		<div class="col-md-6">
	                        			<label>Start Time:</label>
	                        			 <input type="text" name="start_time" value="{{ old('start_time', $event->start_time) }}" name="start_time" class="time start form-control" />
	                        		</div>
	                        	</div>
                        	</div>
                        	<h4>To:</h4>
                        	<div class="form-group">
	                        	<div class="row">
	                        		<div class="col-md-6">
	                        			<label>End Date:</label>
	                        			 <input type="text" name="end_date" value="{{ ($event->end_date) ? date('Y-m-d', strtotime($event->end_date)) : '' }}" class="date end form-control" />
	                        		</div>
	                        		<div class="col-md-6">
	                        			<label>End Time:</label>
	                        			<input type="text" value="{{ old('end_time', $event->end_time) }}" name="end_time" class="time end form-control" />
	                        		</div>
	                        	</div>
                        	</div>

                            <h4>Registation Closing Date:</h4>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Closing Date:</label>
                                         <input type="text" name="registration_closing_date" value="{{ ($event->registration_closing_date) ? date('Y-m-d', strtotime($event->registration_closing_date)) : '' }}" class="date form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Title <span class="required">*</span></label>
                            <input type="text" required="" name="title" value="{{ old('title', $event->title) }}" placeholder="Ex: Coolest Event Ever 2019" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Caption <span class="required">*</span></label>
                            <input type="text" required="" name="caption" value="{{ old('caption', $event->caption) }}" placeholder="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Write-up <span class="required">*</span></label>
                            <input type="text" required="" name="write_up" value="{{ old('write_up', $event->write_up) }}" placeholder="" class="form-control">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Company <span class="required">*</span></label>

                                    @if(Auth::user()->hasRole('superadmin')) 
                                    <select required="" name="company_id" class="form-control">
                                        <option value="">Choose Company</option>
                                        @foreach($companies as $company)
                                        <option {{ ($company->id == old('company_id', $event->company_id)) ? "selected='selected'" : "" }} value="{{ $company->id }}">{{ $company->name }}</option>
                                        @endforeach
                                    </select>
                                    @endif

                                    @if(!Auth::user()->hasRole('superadmin')) 
                                    <input type="hidden" name="company_id" value="{{ Auth::user()->company_id }}" />

                                    <strong>{{ Auth::user()->company->name }}</strong>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label>Type <span class="required">*</span></label>
                                    <select name="event_type" class="form-control">
                                        <option value="">Choose Type</option>
                                        @foreach($types as $type)
                                            <option {{ (old("event_type", $event->event_type) == $type) ? "selected='selected'" : "" }} value="{{ $type }}">{{ $type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        {{-- <div class="form-group">
                            <label>
                                Allow participants to choose diet preferences?
                            </label>
                            <br />
                            <label>
                                <input type="radio" name="show_diet_preferences" value="1" {{ (old('show_diet_preferences', $event->show_diet_preferences) == 1) ? "checked='checked'" : "" }} /> Yes
                            </label>

                            <label>
                                <input type="radio" name="show_diet_preferences" value="0" {{ (old('show_diet_preferences', $event->show_diet_preferences) == 0) ? "checked='checked'" : "" }} /> No
                            </label>
                        </div>   --}}

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Access Type {{ $event->access_type }}: <span class="required">*</span></label>
                                    <select name="access_type" class="form-control">
                                        <option {{ (old('access_type', $event->access_type) == 1) ? "selected='selected'" : "" }} value="1">Login with Username & Password</option>
                                        <option {{ (old('access_type', $event->access_type) == 2) ? "selected='selected'" : "" }} value="2">Verify participant's information</option>
                                        <option {{ (old('access_type', $event->access_type) == 3) ? "selected='selected'" : "" }} value="3">Login without any verification</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="alert alert-warning">
                            <strong>Note: </strong>
                            <p>
                                You can activate/disable event by click the button in the left below.
                            </p>
                        </div>
                        <div class="form-group">
                            <a href="javascript:void(0)" data-id="{{ $event->id }}" class="float-left btn btn-danger delete-event">
                                <i class="icon glyphicon glyphicon-remove"></i> Delete Event
                            </a>
                            <button type="submit" name="create" class="btn btn-primary float-right">
                                <i class="icon glyphicon glyphicon-floppy-save"></i>
                                Update Event
                            </button>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
	@include('admin.imports.datepicker-scripts')
	<script type="text/javascript" src="{{ asset('admin_assets/assets/js/datejs/build/date.js') }}"></script>

    <script src="{{ asset('admin_assets/global/vendor/alertify/alertify.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/notie/notie.js') }}"></script>

    <script src="{{ asset('admin_assets/global/js/Plugin/alertify.js') }}"></script>
    <script src="{{ asset('admin_assets/global/js/Plugin/notie-js.js') }}"></script>

	<script type="text/javascript">
        $(".delete-event").on('click', function() {
            var self = $(this);

            alertify.confirm("Delete this event?", function() {
                var user_id = self.data('id'),
                    base_url = $("#baseurl").attr('content');

                window.location = base_url + "/admin/events/" + user_id + "/" + "delete";
            }, function() {
                alertify.error("Cancelled");
            });
        });

        $(".disable-event").on('click', function() {
            var self = $(this);

            alertify.confirm("Disable this event?", function() {
                var user_id = self.data('id'),
                    base_url = $("#baseurl").attr('content');

                window.location = base_url + "/admin/events/" + user_id + "/" + "disable";
            }, function() {
                alertify.error("Cancelled");
            });
        });

        $(".activate-event").on('click', function() {
            var self = $(this);

            alertify.confirm("Activate this event?", function() {
                var user_id = self.data('id'),
                    base_url = $("#baseurl").attr('content');

                window.location = base_url + "/admin/events/" + user_id + "/" + "activate";
            }, function() {
                alertify.error("Cancelled");
            });
        });

        // initialize input widgets first
        $('#basicExample .time').timepicker({
            'showDuration': true,
            'timeFormat': 'g:ia'
        });

        $('#basicExample .date').datepicker({
            'format': 'yyyy-mm-dd',
            'autoclose': true
        });

        // initialize datepair
        var basicExampleEl = document.getElementById('basicExample');
        var datepair = new Datepair(basicExampleEl);

		$("#start_date").change(function() {
			var startDateValue = $(this).val(),
				dateParsed = Date.parse(startDateValue).toString("yyyy-MM-dd");

			$("input[name='start_date'").val(dateParsed);
		});

		$("#end_date").change(function() {
			var startDateValue = $(this).val(),
				dateParsed = Date.parse(startDateValue).toString("yyyy-MM-dd");

			$("input[name='end_date'").val(dateParsed);
		});

		$("#show-date").click(function() {
			if($(this).is(":checked")) {
				$("#date-picker").show();

				$("input[name='start_date']").removeAttr('disabled');
				$("#start_date").removeAttr('disabled');
				$("input[name='start_time']").removeAttr('disabled');

				$("input[name='end_date']").removeAttr('disabled');
				$("#end_date").removeAttr('disabled');
				$("input[name='end_time']").removeAttr('disabled');

			} else {
				$("#date-picker").hide();

				$("input[name='start_date']").addAttr('disabled');
				$("#start_date").addAttr('disabled');
				$("input[name='start_time']").addAttr('disabled');

				$("input[name='end_date']").addAttr('disabled');
				$("#end_date").addAttr('disabled');
				$("input[name='end_time']").addAttr('disabled');
			}
		});
	</script>
@stop