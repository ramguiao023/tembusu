@extends('admin.layouts.master')

@section('title', 'Events')

@section('style')
    @include('admin.imports.datatable-styles')
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('content')
    <div class="float-left">
        <h3>Manage Emails of {{ $event->title }}</h3>
        <br />
    </div>

    <div class="float-right">
    	<!--   <a href="{{ url('admin/events/'.$event->id.'/mails/create') }}" class="btn btn-primary">
    		<i class="icon fas fa-plus"></i> Create Email
    	</a> -->
    </div>

    <div class="clearfix"></div>

    <div class="panel">
    	<header class="panel-heading">
    	  <div class="panel-actions">
    	  	
    	  </div>   
    	  <h3 class="panel-title"></h3>
    	</header>

        <div class="panel-body">
        	@if(session('success'))
        	    <div class="alert alert-success alert-icon alert-dismissible" role="alert">
        	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        	            <span aria-hidden="true">×</span>
        	        </button>
        	        <i class="icon wb-warning" aria-hidden="true"></i>
        	        <h4>Nice!</h4>
        	        <ul>
        	            <li>{{ session('success') }}</li>
        	        </ul>
        	    </div>
        	    <br />
        	@endif

        	<table class="table table-hover dataTable table-striped w-full" id="mailTable">
        		<thead>
        			<tr>
        				<th>Type</th>
        				<th>Subject</th>
        				<th>Action</th>
        			</tr>
        		</thead>
        		<tfoot>
        			<tr>
        				<th>Type</th>
        				<th>Subject</th>
        				<th>Action</th>
        			</tr>
        		</tfoot>
        		<tbody>
        			@foreach($mails as $mail)
        			<tr>
        				<td>
        					{{ $mail->name }}
        				</td>
        				<td>
        					{{ $mail->subject }}
        				</td>
        				<td>
        					<a href="{{ url('/admin/events/'.$event->id.'/mails/'.$mail->id ) }}" class="btn btn-primary">
        						<i class="fas fa-edit icon"></i>
        					</a>
        					<a href="javascript:void(0)" data-id="{{ $event->id }}" data-mailid="{{ $mail->id }}" class="delete-mail btn btn-danger">
        						<i class="fas fa-times icon"></i>
        					</a>
        				</td>
        			</tr>
        			@endforeach
        		</tbody>
        	</table>
        </div>
    </div>
@stop

@section('script')
    <script type="text/javascript" src="{{ asset('admin_assets/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin_assets/global/js/Plugin/datatables.js') }}"></script>

    <script src="{{ asset('admin_assets/global/vendor/alertify/alertify.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/notie/notie.js') }}"></script>

    <script src="{{ asset('admin_assets/global/js/Plugin/alertify.js') }}"></script>
    <script src="{{ asset('admin_assets/global/js/Plugin/notie-js.js') }}"></script>

    @include('admin.imports.datatable-scripts')

    <script type="text/javascript">
        $("#mailTable").DataTable();

        $(".delete-mail").on('click', function() {
            var self = $(this);

            alertify.confirm("Delete this mail?", function() {
                var event_id = self.data('id'),
                	mail_id = self.data('mailid'),
                    base_url = $("#baseurl").attr('content');

                window.location = base_url + "/admin/events/" + event_id + "/mails/" + mail_id + "/delete";
            }, function() {
                alertify.error("Cancelled");
            });
        });
    </script>
@stop