@extends('admin.layouts.master')

@section('title', 'Questions for ' . $event->title)

@section('style')
	<style>
		#error,
		#success{
			display: none;
		}
	</style>
@stop

@section('content')
    <div class="float-left">
        <h2>Questions for {{ $event->title }}</h2>
        <p>Set questions for your participants.</p>
    </div>

    <a href="{{ url('admin/events') }}" class="btn btn-default float-right" title="Add New User">
        <i class="icon glyphicon glyphicon-chevron-left"></i>
        Go Back
    </a>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <div class="alert alert-danger alert-icon alert-dismissible" id="error" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <i class="icon wb-warning" aria-hidden="true"></i>
                        <h4>Uh oh!</h4>
                        <p>
                        	Please make sure that there no blanks on the fields.
                        </p>
                    </div>

                    <div class="alert alert-success alert-icon alert-dismissible" id="success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <i class="icon fas fa-check" aria-hidden="true"></i>
                        <h4>Nice!</h4>
                        <p>
                        	Saved Successfully.
                        </p>
                    </div>

                    <div class="alert alert-default alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4>Note:</h4>
                        <p>
                        	Leave the question blank to delete the question.
                        </p>
                    </div>

                    <form id="update-question" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" id="event_id" value="{{ $event->id }}">

						<div id="html">
							@if(count($event->questions) <= 0)
								<div class="form-group">
									<label>Question:</label>
									<input type="hidden" class="question_id" value="" />
									<input type="text" placeholder="Your question" class="form-control questions" />
								</div>
							@else
								@foreach($event->questions as $question)
									<div class="form-group">
										<label>Question:</label>
										<input type="hidden" class="question_id" value="{{ $question->id }}" />
										<input type="text" placeholder="Your question" class="form-control questions" value="{{ $question->question }}" />
										<label>Type:</label>
										<select class="type">
											<option value="1" {{ ($question->type==1) ? "selected='selected'" : "" }}>Text</option>
											<option value="2" {{ ($question->type==2) ? "selected='selected'" : "" }}>True/False</option>
											<option value="3" {{ ($question->type==3) ? "selected='selected'" : "" }}>With Choices</option>
										</select>
											
										@if($question->type == 3)
											<div class="choices" style="padding-left: 20px; display: block;">
											<label>Add your choices here: (New line for each choices)</label> 
											<textarea class="answers form-control">{{ $question->choices }}</textarea>
										</div> 
										@else
											<div class="choices" style="padding-left: 20px; display: none;">
											<label>Add your choices here: (New line for each choices)</label> 
											<textarea class="answers form-control"></textarea>
										</div> 
										@endif
									</div>
								@endforeach
							@endif
							
						</div>

						<a href="#" class="btn btn-default btn-icon" id="add-question">
							<i class="fas fa-plus"></i>
						</a>
                       
                        <div class="form-group text-right">
                            <button type="submit" name="create" class="btn btn-primary">
                                <i class="icon glyphicon glyphicon-floppy-save"></i>
                                Update Questions
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script type="text/javascript">
		$("#add-question").click(function() {
			var html = '<div class="form-group">' +
							'<label>Question:</label>' +
							'<input type="hidden" class="question_id" value="" />' +
							'<input type="text" placeholder="Your question" class="form-control questions" />' +
							'<label class="mr-2">Type:</label>' +
							'<select class="type">'+
								'<option value="1">' + 'Text' + '</option>' +
								'<option value="2">' + 'True/False' + '</option>' +
								'<option value="3">' + 'With Choices' + '</option>' +
							'</select>' +
							'<div class="choices" style="padding-left: 20px; display: none">'+
								'<label>Add your choices here: (New line for each choices)</label>' + 
								'<textarea class="answers form-control"></textarea>' +
							'</div>' + 
						'</div>';

			$("#html").append(html);

			return false;
		});

		$("#update-question").submit(function() {
			var questions = [];
			var question_id = [];
			var event_id = [];
			var type = [];
			var answers = [];

			$(".questions").each(function() {
				var question = $(this);

				if(question.val() == "") {
					question.parent().hide();
				}

				questions.push(question.val());
				event_id.push($("#event_id").val());
			});
			$(".question_id").each(function() {
				var question_id_field = $(this);

				question_id.push(question_id_field.val());
			});
			$(".type").each(function() {
				var type_field = $(this);

				type.push(type_field.val());
			});

			$(".answers").each(function() {
				var answers_field = $(this);

				answers.push(answers_field.val());
			});

			console.log(answers);

			var token = $("#token").attr('content');
			var url = $("#baseurl").attr('content');
			
			$.post(url + "/admin/events/"+$("#event_id").val()+"/questions", {
				_token: token,
				questions,
				event_id,
				question_id,
				type,
				answers,
			}, function(data) {
				$("#success").show();
				window.scrollTo(0,0)
			});

			return false;
		});

		$(document).on("change", '.type', function() {
			var type = $(this).val();

			if(type == 3) {
				$(this).next().show();
			} else {
				$(this).next().hide();
			}
		});
    </script>
@stop