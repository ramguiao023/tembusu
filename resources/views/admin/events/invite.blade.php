@extends('admin.layouts.master')

@section('title', 'Invite Participants')

@section('style')
    @include('admin.imports.datatable-styles')
@stop

@section('content')
    <div class="float-left">
        <h2>Invite Participants</h2>
        <p>Manage Your Invitations</p>
    </div>

    <a href="{{ url('admin/events') }}" class="btn btn-default float-right" title="Go Back to Event Listing">
        <i class="icon glyphicon glyphicon-chevron-left"></i> Go Back
    </a>

    <div class="clearfix"></div>
    
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <header class="panel-heading">
                  <div class="panel-actions">
                    
                  </div>   
                  <h3 class="panel-title"></h3>
                </header>

                <div class="panel-body">
                    @if(session('success'))
                        <div class="alert alert-success alert-icon alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <i class="icon wb-check" aria-hidden="true"></i>
                            <h4>Nice!</h4>
                            <p>
                                {{ session('success') }}
                            </p>
                        </div>
                    @endif
            
                    <form action="">
                        <div class="form-group">
                            <h3>{{ $event->title }}</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>To which company will you send the invite?</label>
                                    <input type="hidden" name="event_id" id="event_id" value="{{ $event->id }}" />
                                    <select name="company_id" id="company_id" class="form-control">
                                        <option value="">Choose a Company</option>
                                        @foreach($companies as $company)
                                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#company_id").change(function() {
                var value = $(this).val(),
                    url = $("#baseurl").attr('content'),
                    event_id = $("#event_id").val();

                window.location = url + "/admin/events/" + event_id + "/invite/" + value;
            });
        });
    </script>
@stop