@extends('admin.layouts.master')

@section('title', 'Update Event images')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/dropify/dropify.css') }}">
@stop

@section('content')
    <div class="row mb-10">
        <div class="col-md-8">
            <div class="container-fluid">
                <div class="row">
                    @include('admin.imports.event-nav')
                </div>          
            </div>
        </div>
        <div class="col-md-4 text-right">
            <a href="{{ url('admin/events') }}" class="btn btn-default" title="Add New User">
                <i class="icon glyphicon glyphicon-chevron-left"></i>
                Go Back
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            @if(session('success'))
                <div class="alert alert-success">
                    <h3>Nice!</h3>
                    {{ session('success') }}
                </div>
            @endif
            <div class="panel">
                <header class="panel-heading">
                  <div class="panel-actions">
                    
                  </div>   
                  <h3 class="panel-title">
                    <div class="float-left">
                        Update Event Images (Part 2)
                        <br />
                        <small>Email Banner Image & Wallpaper</small>
                    </div>

                    <div class="float-right">
                    </div>
                    <div class="clearfix"></div>
                  </h3>
                </header>
                <div class="panel-body">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger alert-icon alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <i class="icon wb-warning" aria-hidden="true"></i>
                            <h4>Uh oh!</h4>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ url('admin/events/'.$event->id.'/images') }}" method="POST" autocomplete="off" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @method('PUT')

                        <input type="hidden" name="created_by" value="{{ Auth::user()->id }}" />
                        <input type="hidden" name="type" value="images" />

                        <div class="form-group form-material" data-plugin="formMaterial">
                            <label class="form-control-label" for="inputText">
                                Email Banner Image
                                <small>(600 x 400)</small>
                            </label>
                            <input type="file" id="input-file-now" name="banner_image" data-plugin="dropify" data-default-file="{{ $event->banner_image }}" />
                        </div>

                        <div class="form-group form-material" data-plugin="formMaterial">
                            <label class="form-control-label" for="inputText">
                                Wallpaper
                                <small>(1080 x 768)</small>
                            </label>
                            <input type="file" id="input-file-now" name="wallpaper" data-plugin="dropify" data-default-file="{{ $event->wallpaper }}" />
                        </div>  
                        
                        <div class="form-group text-right">
                            <button type="submit" name="create" class="btn btn-primary">
                                <i class="icon glyphicon glyphicon-floppy-save"></i>
                                Update Event Images
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script src="{{ asset('admin_assets/global/vendor/jquery-ui/jquery-ui.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-tmpl/tmpl.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-load-image/load-image.all.min.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-process.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-image.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-audio.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-video.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-validate.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-ui.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/dropify/dropify.min.js') }}"></script>
@stop