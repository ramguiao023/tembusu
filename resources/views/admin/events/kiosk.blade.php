@extends('admin.layouts.master')

@section('title', 'Events')

@section('style')
    @include('admin.imports.datatable-styles')
@stop

@section('content')
    
    <div class="float-left">
        <h2>Check-in Participant</h2>
        <p>Scan Participant's QR Code</p>
    </div>

    <div class="clearfix"></div>
    
    <div class="row">
        <div class="col-md-8">
            <div class="panel">
                <header class="panel-heading">
                  <div class="panel-actions">
                    
                  </div>   
                  <h3 class="panel-title"></h3>
                </header>

                <div class="panel-body">
                    <input type="hidden" value="{{ $event_id }}" id="event_id">
                    
                    <video id="preview" playsinline style="width: 100%;"></video>

                    <form id="searchqrid" action="">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="glyphicon glyphicon-qrcode"></i>
                                </span>
                            </div>
                            <input type="text" id="qrid" value="{{ @$participant->qrid }}" class="form-control" placeholder="Scan Participant's QR Code" style="font-size: 1.2rem !important;" />
                            <span class="input-group-append">
                                <button type="submit" class="btn btn-primary"><i class="icon wb-search" aria-hidden="true"></i></button>
                            </span>
                        </div>
                    </form>

                    <hr />

                    <div id="check-in-success" class="alert alert-success">
                        <h4>Nice!</h4>
                        <p>
                            Participant has been successfully checked-in.
                        </p>
                    </div>
                    
                    <div id="participant-info">
                        <input type="hidden" id="participant_id" value="">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="#" id="participant-picture" alt="" class="img-fluid" />
                            </div>
                            <div class="col-md-9">
                                <h4 class="float-left" id="participant-name">Ram Guiao</h4>
                                <span id="participant-status" class="float-right badge">GOING</span>
                                <div class="clearfix"></div>
                                <hr />
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="mb-10">
                                            <strong>
                                                <i class="glyphicon glyphicon-user"></i>
                                                Employee Number
                                            </strong>
                                            - <span id="participant-id">10001</span>
                                        </p>
                                        <p class="mb-10">
                                            <strong>
                                                <i class="glyphicon glyphicon-envelope"></i> Email Address
                                            </strong>
                                            - <span id="participant-email">ramhee@asd.com</span>
                                        </p>
                                        <p class="mb-10">
                                            <strong>
                                                <i class="glyphicon glyphicon-calendar"></i> Birthdate:
                                            </strong>
                                            - <span id="participant-birthdate">February 25, 1992</span>
                                        </p>
                                        <p class="mb-10">
                                            <strong>
                                                <i class="fas fa-hashtag"></i>
                                                Table Number
                                            </strong>
                                            - <span id="table-number"></span>
                                        </p>
                                        <p class="mb-10">
                                            <strong>
                                                <i class="fas fa-trophy"></i>
                                                Lucky Draw Code
                                            </strong>
                                            - <span id="raffle-code"></span>
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <strong>Diet Preferences</strong>
                                        <br />
                                        <span id="participant-diet-preferences"></span>
                                        <br /><br />
                                        <span id="participant-diet-notes"></span>
                                    </div>
                                </div>
                                <br />
                                <div class="form-group">
                                    <button id="check-in" class="btn btn-success">
                                        Check-in
                                    </button>
                                    <a href="{{ url(
                                        'admin/events/'.$event_id.'/participants') }}" id="go-back" class="btn btn-sm btn-primary">
                                        Go Back
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
<script type="text/javascript" src="{{ asset('admin_assets/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin_assets/global/js/Plugin/datatables.js') }}"></script>

@include('admin.imports.datatable-scripts')

<script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<script type="text/javascript" src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
<script type="text/javascript">
  let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
  scanner.addListener('scan', function (content) {
    console.log(content);
    $("#qrid").val(content);
    $("#searchqrid").trigger('submit');
  });
  Instascan.Camera.getCameras().then(function (cameras) {
    if (cameras.length > 0) {
       
            // scanner.start(cameras[0]);
        
        if(cameras[1]) {
            scanner.start(cameras[1]);
        } else {
            scanner.start(cameras[0]);
        }
    } else {
      console.error('No cameras found.');
    }
  }).catch(function (e) {
    console.error(e);
  });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#eventTable").DataTable();

        var qrid = $("#qrid").val();

        $("#searchqrid").submit(function() {
            var qrid = $("#qrid").val();

            searchParticipant(qrid);

            return false;
        });

        $("#check-in").click(function() {
            var url = $("#baseurl").attr('content'),
                participant_id = $("#participant_id").val(),
                event_id = $("#event_id").val();

            $.get(url + "/admin/events/"+event_id+"/"+participant_id+"/checkin", function(data) 
            {
                if(data) {
                    var status_message = "CHECKED-IN",
                        status_class = "badge-success";

                    $("#participant-status").text(status_message);
                    $("#participant-status").removeClass("badge-info");
                    $("#participant-status").removeClass("badge-danger");
                    $("#participant-status").addClass(status_class);

                    $("#check-in").hide();
                    $("#go-back").show();
                    $("#check-in-success").show();
                }
            });  
        });
    });

    function searchParticipant(qrid) {
        var url = $("#baseurl").attr('content'),
            event_id = $("#event_id").val();


        $.get(url + "/admin/participants/"+qrid+"/"+event_id+"/qrid", function(data) 
        {  
            console.log(data);
            $("#participant_id").val(data.id);
            $("#participant-name").text(data.salutation + " " + data.name);
            $("#participant-id").text(data.employee_number);
            $("#participant-email").text(data.email);
            $("#participant-birthdate").text(data.birthdate);
            $("#participant-diet-preferences").text(data.diet_preferences);
            $("#participant-diet-notes").text(data.diet_notes);
            $("#table-number").text(data.event_participant.table_number);
            $("#raffle-code").text(data.event_participant.raffle_code);
            $("#participant-picture").attr('src', data.profile_image);
            
            var status_message = "",
                status_class = "badge-info";
            if(data.event_participant.status == 1) {
                status_message = "GOING";
                status_class = "badge-info";
            } else if(data.event_participant.status == 0) {
                status_message = "DECLINED";
                status_class = "badge-danger";
            } else if(data.event_participant.status == 2) {
                status_message = "CHECKED-IN";
                status_class = "badge-success";
            }

            $("#participant-status").text(status_message);
            $("#participant-status").addClass(status_class);

            $("#participant-info").show();
        });
    }
</script>
@stop