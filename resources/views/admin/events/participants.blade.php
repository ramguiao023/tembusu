@extends('admin.layouts.master')

@section('title', 'Events')

@section('style')
    @include('admin.imports.datatable-styles')
@stop

@section('content')
    <div class="float-left">
        <h3>{{ $event->title }} Participants</h3>
        <p>Manage Participants</p>
    </div>
    
    <div class="float-right">
        Manage Participant
        <a href="{{ url('admin/participants/create/'.$event->id) }}" class="btn btn-success" title="Add New User">
            <i class="icon glyphicon glyphicon-plus"></i> Add Single
        </a>
        
        <a href="{{ url('admin/participants/bulk-upload/'.$event->id) }}" class="btn btn-success">
            <i class="icon glyphicon glyphicon-plus"></i> Add Bulk
        </a>

        <a href="{{ url('admin/events/'.$event->id.'/'.'bulk') }}" class="btn btn-warning">
            <i class="icon fas fa-upload"></i> Update Bulk
        </a>

        <a href="{{ url('admin/events/'.$event->id.'/kiosk') }}" class="btn btn-primary" title="Add New User">
            <i class="icon fas fa-qrcode"></i> Check-in
        </a>
    </div>
    
        
    <div class="clearfix"></div>

    <div class="panel">
    	<header class="panel-heading">
    	  <div class="panel-actions">
    	  	
    	  </div>   
    	  <h3 class="panel-title"></h3>
    	</header>

        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success alert-icon alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="icon wb-check" aria-hidden="true"></i>
                    <h4>Nice!</h4>
                    <p>
                        {{ session('success') }}
                    </p>
                </div>
            @endif

            <!-- <div class="note note-warning">
                <strong>Note:</strong>
                <p>
                    Buttons in "Action" were hidden because the participant should opt-in in the email in order to show the button
                </p>
            </div> -->

            <a href="{{ url('admin/events/'.$event->id.'/participant-download') }}" class="btn btn-primary">
                <i class="icon fas fa-download"></i>
                Download CSV
            </a>

            <br />
            <br />

            <table class="table table-hover dataTable table-striped w-full" id="eventTable">
                <thead>
                    <tr>
                        <th>Employee</th>
                        <th>Email</th>
                        <th>Diet Preferences</th>
                        <th>Status</th>
                        <th style="display: none;">Answers</th>
                        <th>Is Vip</th>
                        <th>Table Number</th>
                        <th>Lucky Draw Number</th>
                        <th>Date Opted</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Employee</th>
                        <th>Email</th>
                        <th>Diet Preferences</th>
                        <th>Status</th>
                        <th style="display: none;">Answers</th>
                        <th>Is Vip</th>
                        <th>Table Number</th>
                        <th>Lucky Draw Number</th>
                        <th>Date Opted</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($participants as $participant)
                        <tr>
                            <td>
                                {{ @$participant->participant->employee_number }}
                                <br />
                                {{
                                    @$participant->participant->salutation . " " . 
                                    @$participant->participant->name
                                }}
                            </td>
                            <td>
                                {{
                                    $participant->participant->email
                                }}
                            </td>
                            <td>
                                {{ @$participant->participant->diet_preferences }}
                                <br />
                                {{ @$participant->participant->diet_notes }}
                            </td>
                            <td>
                                {!!
                                    ($participant->status == 1)
                                    ?
                                        "<span class='badge badge-info'>GOING</span>"
                                    :
                                        ""
                                !!}

                                {!!
                                    ($participant->status == 2)
                                    ?
                                       "<span class='badge badge-success'>CHECKED-IN</span>"
                                    :
                                         ""
                                !!}

                                {!!
                                    ($participant->status == 0)
                                    ?
                                        "<span class='badge badge-danger'>DECLINED</span>"
                                    :
                                        ""
                                !!}

                                {!!
                                    ($participant->status == 3)
                                    ?
                                        "<span class='badge badge-default'>PENDING</span>"
                                    :
                                        ""
                                !!}

                                {!!
                                    ($participant->status == 4)
                                    ?
                                        "<span class='badge badge-warning'>EMAIL SENT</span>"
                                    :
                                        ""
                                !!}
                            </td>
                            <td style="display: none;">
                                <ul>
                                    
                                @foreach($participant->questions as $question)
                                    <li>{{ $question->question }}</li>
                                    <ul>
                                        <li>{{ ($question->answer['answer'] != "") ? $question->answer['answer'] : "No Answer" }}</li>
                                    </ul>      
                                @endforeach
                                </ul>
                            </td>
                            <td>
                                @if(@$participant->participant->is_vip == "Yes")
                                    <span class="badge badge-info">Yes</span>
                                @else
                                    <span class="badge badge-default">No</span>
                                @endif
                            </td>
                            <td>
                                {{ $participant->table_number }}
                            </td>
                            <td>
                                {{ $participant->raffle_code }}
                            </td>
                            <td>
                                {{ date("M d, Y h:i A", strtotime($participant->created_at)) }}
                            </td>
                            <td>
                                <!-- Don't allow participant that their status is -->
                                <!-- IS EMAIL NOT SENT -->
                                <!-- PENDING -->
                                <!-- CHECKED-IN -->
                                <?php //@if($participant->status != 4 and $participant->status != 3 and $participant->status != 2) ?>
                                    <a href="{{ url('/admin/events/'.$event->id.'/kiosk?participant_id='.@$participant->participant->id) }}" class="btn btn-success">
                                        
                                        <i class="fas fa-check"></i>
                                    </a>
                                <?php //@endif ?>

                                <a href="{{ url('/admin/participants/'.@$participant->participant->id.'/edit/'.$event->id) }}" class="btn btn-info">
                                    
                                    <i class="fas fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
          </table>
        </div>
    </div>
@stop

@section('script')
<script type="text/javascript" src="{{ asset('admin_assets/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin_assets/global/js/Plugin/datatables.js') }}"></script>

@include('admin.imports.datatable-scripts')

<script type="text/javascript">
    $("#eventTable").DataTable();
</script>
@stop