@extends('admin.layouts.master')

@section('style')
@stop

@section('title', 'Add New Company')

@section('content')
	<h2>Add New Company</h2>
    <p>Fill-out all the Necessary Fields</p>
<form method="POST" action="{{ url('admin/companies') }}">
	<div class="row">
    	<div class="col-md-12 text-right">
    		<button class="btn btn-primary mr-1">
    			<i class="icon glyphicon glyphicon-floppy-saved"></i>
    			Save Company
    		</button>
    		<a href="{{ url('/admin/companies') }}" class="btn btn-default">
    			Go Back
    		</a>
    	</div>
    </div>
    <br />
    @if(count($errors) > 0)
		<div class="alert alert-danger alert-icon alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="icon wb-warning" aria-hidden="true"></i>
            <h4>Oops!</h4>
            <ul>
            	@foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
				@endforeach
            </ul>
        </div>
	@endif
    <div class="row">
    	<div class="col-md-6">
		    <div class="panel">
				<div class="panel-body">
					{{ csrf_field() }}


					<h4>Basic Information</h4>

					<div class="form-group mt-10">
						<label>Business Name <span class="required">*</span></label>
						<input type="text" name="name" value="{{ old('name') }}" placeholder="What's the name of the business?" class="form-control" />	
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<label>Slug <span class="required">*</span></label>
								<small>
									{{ url('/')."/" }} <span id="slugs"></span>
								</small>
								<input type="text" name="slug" value="{{ old('slug') }}" class="form-control" />
							</div>
						</div>
					</div>

					<div class="form-group">
						<label>Description</label>
						<textarea class="form-control" name="description" placeholder="What's the business is doing?">{{ old('description') }}</textarea>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<label>Address <span class="required">*</span></label>
								
								<input type="text" name="address" value="{{ old('address') }}" placeholder="Where is it located?" class="form-control" />
							</div>
							<div class="col-md-6">
								<label>Country <span class="required">*</span></label>
								<select name="country" class="form-control">
									<option value="">Country</option>
									@foreach($countries as $country)
										<option {{ ($country->id == old('country')) ? "selected='selected'" : "" }} value="{{ $country->id }}">{{ $country->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label>Business Type <span class="required">*</span></label>
						<select name="type" class="form-control">
							<option value="">Which type of business?</option>
							@foreach($types as $type)
								<option {{ ($type->id == old('type')) ? "selected='selected'" : "" }} value="{{ $type->id }}">{{ $type->type }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
    	</div>
    	<div class="col-md-6">
		    <div class="panel">
				<div class="panel-body">
						{{ csrf_field() }}

						<h4>Contact Information</h4>

						<div class="form-group mt-10">
							<label>Business Contact Number <span class="required">*</span></label>
							<input type="text" name="mobile_number" value="{{ old('mobile_number') }}" placeholder="+65 XXXX XXXX" class="form-control" />	
						</div>
						<div class="form-group mt-10">
							<label>Contact Person Name <span class="required">*</span></label>
							<input type="text" name="contact_person" value="{{ old('contact_person') }}" placeholder="Who's the person to look for?" class="form-control" />	
						</div>

						<div class="form-group">
							<label>Contact Person Email Address <span class="required">*</span></label>
							<input type="text" name="contact_email" value="{{ old('contact_email') }}" placeholder="john@example.com" class="form-control" />
						</div>

						<div class="form-group">
							<label>Contact Person Number <span class="required">*</span></label>
							<input type="text" name="contact_number" value="{{ old('contact_number') }}" placeholder="+65 XXXX XXXX" class="form-control" />
						</div>

						<div class="form-group" style="margin-bottom: 20px;">
							<label>Contact Person Address </label>
							<input type="text" name="contact_person_address" value="{{ old('contact_person_address') }}" placeholder="Where is it located?" class="form-control" />
						</div>
				</div>
			</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-md-12 text-right">
    		<button class="btn btn-primary mr-1">
    			<i class="icon glyphicon glyphicon-floppy-saved"></i>
    			Save Company
    		</button>
    		<a href="{{ url('/admin/companies') }}" class="btn btn-default">
    			Go Back
    		</a>
    	</div>
    </div>
</form>
@stop

@section('script')
@stop
