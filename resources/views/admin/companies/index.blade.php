@extends('admin.layouts.master')

@section('style')
	@include('admin.imports.datatable-styles')

	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('title', 'Companies')

@section('content')
	<h2>Companies</h2>
    <p>Manage Companies</p>
    <div class="panel">
    	<header class="panel-heading">
    	  <div class="panel-actions">
    	  	<a href="{{ url('admin/companies/create') }}" class="btn btn-success" title="Add New User">
				<i class="icon glyphicon glyphicon-plus"></i> Add New Company
			</a>
    	  </div>
    	  <h3 class="panel-title">List of Companies</h3>
    	</header>

		<div class="panel-body">
			@if(session('success'))
				<div class="alert alert-success alert-icon alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="icon wb-check" aria-hidden="true"></i>
                    <h4>Nice!</h4>
                    <p>
                    	{{ session('success') }}
                    </p>
                </div>
			@endif

			<table class="table table-hover dataTable table-striped w-full" id="companiesTable">
			  <thead>
			    <tr>
			      <th>Company</th>
			      <th>Address</th>
			      <th>Mobile Number</th>
			      <th>Contact</th>
			      <th>Action</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@foreach($companies as $company)
			  	<tr>
			  		<td>
			  			{{ $company->name }}
			  		</td>
			  		<td>
			  			{{ $company->address }}
			  		</td>
			  		<td>
			  			{{ $company->mobile_number }}
			  		</td>
			  		<td>
			  			{{ $company->contact_person }}
			  		</td>
			  		<td>
			  			<a href="{{ url('/admin/companies/'.$company->id) }}" class="btn btn-info">
			  				<i class="icon wb-edit"></i>
			  			</a>
			  			<a href="javascript:void(0)" data-id="{{ $company->id }}" class="btn btn-danger delete-company">
			  				<i class="icon wb-close"></i>
			  			</a>
			  		</td>
			  	</tr>
			  	@endforeach
			  </tbody>
			  <tfoot>
			    <tr>
			      <th>Company</th>
			      <th>Address</th>
			      <th>Mobile Number</th>
			      <th>Contact</th>
			      <th>Action</th>
			    </tr>
			  </tfoot>
			  <tbody>
			  
			</tbody>
			</table>
		</div>
	</div>
@stop

@section('script')

<script type="text/javascript" src="{{ asset('admin_assets/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin_assets/global/js/Plugin/datatables.js') }}"></script>

@include('admin.imports.datatable-scripts')

<script src="{{ asset('admin_assets/global/vendor/alertify/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/notie/notie.js') }}"></script>

<script src="{{ asset('admin_assets/global/js/Plugin/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/js/Plugin/notie-js.js') }}"></script>

<!-- <script src="{{ asset('admin_assets/assets/examples/js/tables/datatable.js') }}"></script> -->
<script type="text/javascript">
	$("#companiesTable").DataTable();

	$(".delete-company").on('click', function() {
		var self = $(this);

		alertify.confirm("Delete this company?", function() {
			var company_id = self.data('id'),
				base_url = $("#baseurl").attr('content');
			window.location = base_url + "/admin/companies/" + company_id+"/delete";
		}, function() {
			alertify.error("Cancelled");
		});
	});
</script>
@stop