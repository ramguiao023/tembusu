@extends('admin.layouts.master')

@section('style')
	@include('admin.imports.datatable-styles')

	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('title', 'Permissions')

@section('content')
	<h2>Permissions</h2>
    <p>Manage Roles' Permission</p>

    <div class="panel">
    	<header class="panel-heading">
    	  <div class="panel-actions">
    	  	<a href="{{ url('admin/users/permissions/create') }}" class="btn btn-success" title="Add New User">
				<i class="icon glyphicon glyphicon-plus"></i> Add Permission
			</a>
    	  </div>
    	  <h3 class="panel-title">List of Permissions</h3>
    	</header>

		<div class="panel-body">
			@if(session('success'))
				<div class="alert alert-success alert-icon alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="icon wb-check" aria-hidden="true"></i>
                    <h4>Nice!</h4>
                    <p>
                    	{{ session('success') }}
                    </p>
                </div>
			@endif

			<table class="table table-hover dataTable table-striped w-full" id="userTable">
			  <thead>
			    <tr>
			      <th>Name</th>
			      <th>Display Name</th>
			      <th>Roles</th>
			      <th>Action</th>
			    </tr>
			  </thead>
			  <tfoot>
			    <tr>
			      <th>Name</th>
			      <th>Display Name</th>
			      <th>Roles</th>
			      <th>Action</th>
			    </tr>
			  </tfoot>
			  <tbody>
			  	@foreach($permissions as $permission)
			  		<tr>
			  			<td>{{ $permission->name }}</td>
			  			<td>{{ $permission->display_name }}</td>
			  			<td>
			  				{!! "<span class='badge badge-success'>".implode('</span> <br /> <span class="badge badge-success">', $permission->roles)."</span>" !!}
			  			</td>
			  			<td>
			  				<a href="{{ url('admin/users/permissions/'.$permission->id) }}" class="btn btn-primary">
			  					<i class="icon glyphicon glyphicon-edit"></i>
			  				</a>
			  			</td>
			  		</tr>
			  	@endforeach

			  </tbody>
			</table>
		</div>
	</div>
@stop

@section('script')

<script type="text/javascript" src="{{ asset('admin_assets/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin_assets/global/js/Plugin/datatables.js') }}"></script>

@include('admin.imports.datatable-scripts')

<script src="{{ asset('admin_assets/global/vendor/alertify/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/notie/notie.js') }}"></script>

<script src="{{ asset('admin_assets/global/js/Plugin/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/js/Plugin/notie-js.js') }}"></script>

<!-- <script src="{{ asset('admin_assets/assets/examples/js/tables/datatable.js') }}"></script> -->
<script type="text/javascript">
	$("#userTable").DataTable();
	$(".delete-user").on('click', function() {
		var self = $(this);

		alertify.confirm("Delete this user?", function() {
			var user_id = self.data('id'),
				base_url = $("#baseurl").attr('content');
			window.location = base_url + "/admin/users/delete/" + user_id;
		}, function() {
			alertify.error("Cancelled");
		});
	});
</script>
@stop