@extends('admin.layouts.master')

@section('style')
	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload.css') }}">
	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/dropify/dropify.css') }}">
@stop

@section('title', 'Users')

@section('content')
	<h2>My Profile</h2>
    <p>Manage Your System Profile</p>
	<div class="row">
		<div class="col-md-6">
		    <div class="panel">
				<div class="panel-body">
					<form method="POST" action="{{ url('admin/save-profile') }}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{ $user->id }}" />

						@if(count($errors) > 0)
							<div class="alert alert-danger alert-icon alert-dismissible" role="alert">
			                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                        <span aria-hidden="true">×</span>
			                    </button>
			                    <i class="icon wb-warning" aria-hidden="true"></i>
			                    <h4>Oops!</h4>
			                    <ul>
			                    	@foreach($errors->all() as $error)
					                    <li>{{ $error }}</li>
									@endforeach
			                    </ul>
			                </div>
						@endif

						@if(session('success'))
							<div class="alert alert-success alert-icon alert-dismissible" role="alert">
			                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                        <span aria-hidden="true">×</span>
			                    </button>
			                    <i class="icon wb-check" aria-hidden="true"></i>
			                    <h4>Nice!</h4>
			                    <p>
			                    	{{ session('success') }}
			                    </p>
			                </div>
						@endif
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group form-material" data-plugin="formMaterial">
									<label class="form-control-label" for="inputText">Profile Photo</label>
									<input type="file" id="input-file-now" name="profile_photo" data-plugin="dropify" data-default-file="{{ asset('uploads/profile-picture/'.Auth::user()->profile_photo) }}" />
								</div>
							</div>
							<div class="col-md-6">
							</div>
						</div>
						<br />
						<div class="row">
							<div class="col-md-6">
								<div class="form-group form-material" data-plugin="formMaterial">
									<label class="form-control-label" for="inputText">First Name</label>
									<input type="text" class="form-control" name="first_name" value="{{ old('first_name', $user->first_name) }}" placeholder="Enter Your First Name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group form-material" data-plugin="formMaterial">
									<label class="form-control-label" for="inputText">Last Name</label>
									<input type="text" class="form-control" value="{{ old('last_name', $user->last_name) }}" name="last_name" placeholder="Enter Your Last Name">
								</div>
							</div>
						</div>
						<div class="form-group form-material" data-plugin="formMaterial">
							<h4>Account Credentials</h4>
						</div>
						<div class="form-group form-material" data-plugin="formMaterial">
							<label class="form-control-label" for="inputText">Email Address</label>
							<input type="text" class="form-control" disabled="" value="{{ $user->email }}" name="email" placeholder="email@example.com">
						</div>
						<div class="form-group form-material" data-plugin="formMaterial">
							<label class="form-control-label" for="inputText">Change Password</label>
							<input type="password" class="form-control" name="password" placeholder="********">
						</div>
						<div class="form-group form-material" data-plugin="formMaterial">
							<button type="submit" name="save" class="btn-primary btn waves-effect waves-classic">Save Profile</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

@section('script')
<script src="{{ asset('admin_assets/global/vendor/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/blueimp-tmpl/tmpl.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/blueimp-load-image/load-image.all.min.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-process.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-image.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-audio.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-video.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-validate.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-ui.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/dropify/dropify.min.js') }}"></script>
@stop