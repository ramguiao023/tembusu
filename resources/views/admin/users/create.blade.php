@extends('admin.layouts.master')

@section('style')
@stop

@section('title', 'Add New User')

@section('content')
	<h2>Create New User</h2>
    <p>Fill-out all the Necessary Fields</p>
    <div class="row">
    	<div class="col-md-6">
    		    <div class="panel">
    		    	<header class="panel-heading">
    		    	  <div class="panel-actions">
    		    	  	<a href="{{ url('admin/users') }}" class="btn btn-default" title="Add New User">
    						<i class="icon glyphicon glyphicon-chevron-left"></i> Go Back
    					</a>
    		    	  </div>
    		    	  <h5 class="panel-title" style="visibility: hidden">Fill-out all the Necessary Fields</h5>
    		    	</header>

    				<div class="panel-body">
    					<form method="POST" action="{{ url('admin/users') }}">
							{{ csrf_field() }}

							@if(count($errors) > 0)
								<div class="alert alert-danger alert-icon alert-dismissible" role="alert">
				                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				                        <span aria-hidden="true">×</span>
				                    </button>
				                    <i class="icon wb-warning" aria-hidden="true"></i>
				                    <h4>Oops!</h4>
				                    <ul>
				                    	@foreach($errors->all() as $error)
						                    <li>{{ $error }}</li>
										@endforeach
				                    </ul>
				                </div>
							@endif

	    					<div class="row">
	    						<div class="col-md-6">
	    							<div class="form-group form-material" data-plugin="formMaterial">
	    								<label class="form-control-label" for="inputText">First Name</label>
	    								<input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="Enter Your First Name">
	    							</div>
	    						</div>
	    						<div class="col-md-6">
	    							<div class="form-group form-material" data-plugin="formMaterial">
	    								<label class="form-control-label" for="inputText">Last Name</label>
	    								<input type="text" class="form-control" value="{{ old('last_name') }}" name="last_name" placeholder="Enter Your Last Name">
	    							</div>
	    						</div>
	    					</div>
	    					<div class="form-group form-material" data-plugin="formMaterial">
	    						<h4>Account Credentials</h4>
	    					</div>

	    					<div class="form-group form-material" data-plugin="formMaterial">
	    						<label class="form-control-label" for="inputText">Role</label>
	    						<select name="role" class="form-control">
	    							@foreach($roles as $role)
	    								<option value="{{ $role->id }}">{{ $role->display_name }}</option>
	    							@endforeach
	    						</select>
	    					</div>
	    					<div class="form-group form-material" data-plugin="formMaterial">
	    						<label class="form-control-label" for="inputText">Email Address</label>
	    						<input type="text" class="form-control" value="{{ old('email') }}" name="email" placeholder="email@example.com">
	    					</div>
	    					<div class="form-group form-material" data-plugin="formMaterial">
	    						<label class="form-control-label" for="inputText">Password</label>
	    						<input type="password" class="form-control" name="password" placeholder="********">
	    					</div>
	    					<div class="form-group form-material" data-plugin="formMaterial">
	    						<button type="submit" name="save" class="btn-primary btn waves-effect waves-classic">Save Profile</button>
	    					</div>
	    				</form>
    				</div>
    			</div>
    	</div>
    </div>
@stop

@section('script')
@stop
