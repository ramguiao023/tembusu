@extends('admin.layouts.master')

@section('style')
	@include('admin.imports.datatable-styles')

	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('title', 'Add Permission')

@section('content')
	<h2>Permission</h2>
    <p>Add User Permission</p>

    <div class="row">
    	<div class="col-md-6">
    		    <div class="panel">
    		    	<header class="panel-heading">
    		    	  <div class="panel-actions">
                        <a href="{{ url('admin/users/permissions') }}" class="btn btn-default" title="Add New User">
                            <i class="icon glyphicon glyphicon-chevron-left"></i> Go Back
                        </a>
    		    	  </div>
                      <h3 class="panel-title" style="visibility: hidden;">Permissions</h3>
    		    	</header>

    				<div class="panel-body">
    					@if(session('success'))
    						<div class="alert alert-success alert-icon alert-dismissible" role="alert">
    		                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    		                        <span aria-hidden="true">×</span>
    		                    </button>
    		                    <i class="icon wb-check" aria-hidden="true"></i>
    		                    <h4>Nice!</h4>
    		                    <p>
    		                    	{{ session('success') }}
    		                    </p>
    		                </div>
    					@endif

    					@if(count($errors) > 0)
							<div class="alert alert-danger alert-icon alert-dismissible" role="alert">
			                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                        <span aria-hidden="true">×</span>
			                    </button>
			                    <i class="icon wb-warning" aria-hidden="true"></i>
			                    <h4>Oops!</h4>
			                    <ul>
			                    	@foreach($errors->all() as $error)
					                    <li>{{ $error }}</li>
									@endforeach
			                    </ul>
			                </div>
						@endif
						
    					<form method="POST" action="{{ url('/admin/users/permissions/') }}">
    						{{ csrf_field() }}

    						<div class="form-group form-material">
    							<label class="form-control-label">Permission Name</label>
    							<input type="text" name="name" value="{{ old('name') }}" class="form-control">
    						</div>

    						<div class="form-group form-material">
    							<label class="form-control-label">Permission Display Name</label>
    							<input type="text" name="display_name" value="{{ old('display_name') }}" class="form-control">
    						</div>

    						<div class="form-group form-material">
    							<label class="form-control-label">Permission Description</label>
    							<input type="text" name="description" value="{{ old('description') }}" class="form-control">
    						</div>
    						<div class="form-group form-material" data-plugin="formMaterial">
	    						<button type="submit" name="save" class="float-right btn-primary btn waves-effect waves-classic">Save Permission</button>

	    						<div class="clearfix"></div>
	    					</div>
    					</form>
    				</div>
    			</div>
    	</div>
    </div>

@stop

@section('script')

<script type="text/javascript" src="{{ asset('admin_assets/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin_assets/global/js/Plugin/datatables.js') }}"></script>

@include('admin.imports.datatable-scripts')

<script src="{{ asset('admin_assets/global/vendor/alertify/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/notie/notie.js') }}"></script>

<script src="{{ asset('admin_assets/global/js/Plugin/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/js/Plugin/notie-js.js') }}"></script>

<!-- <script src="{{ asset('admin_assets/assets/examples/js/tables/datatable.js') }}"></script> -->
<script type="text/javascript">
	$("#userTable").DataTable();
	$(".delete-user").on('click', function() {
		var self = $(this);

		alertify.confirm("Delete this user?", function() {
			var user_id = self.data('id'),
				base_url = $("#baseurl").attr('content');
			window.location = base_url + "/admin/users/delete/" + user_id;
		}, function() {
			alertify.error("Cancelled");
		});
	});
</script>
@stop