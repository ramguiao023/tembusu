@extends('admin.layouts.master')

@section('style')
	@include('admin.imports.datatable-styles')

	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('title', 'Add Permission to ' . $role->display_name)

@section('content')
	<h2>Add Permission to {{ $role->display_name }}</h2>
    <p>Manage Role Permissions</p>

    <div class="row">
    	<div class="col-md-8">
    		    <div class="panel">
    		    	<header class="panel-heading">
    		    	  <div class="panel-actions">
                        <a href="{{ url('admin/users/roles') }}" class="btn btn-default" title="Add New User">
                            <i class="icon glyphicon glyphicon-chevron-left"></i> Go Back
                        </a>
    		    	  </div>
                      <h3 class="panel-title">Set Permissions</h3>
    		    	</header>

    				<div class="panel-body">
                        <hr />
    					@if(session('success'))
    						<div class="alert alert-success alert-icon alert-dismissible" role="alert">
    		                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    		                        <span aria-hidden="true">×</span>
    		                    </button>
    		                    <i class="icon wb-check" aria-hidden="true"></i>
    		                    <h4>Nice!</h4>
    		                    <p>
    		                    	{{ session('success') }}
    		                    </p>
    		                </div>
    					@endif

    					@if(count($errors) > 0)
							<div class="alert alert-danger alert-icon alert-dismissible" role="alert">
			                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                        <span aria-hidden="true">×</span>
			                    </button>
			                    <i class="icon wb-warning" aria-hidden="true"></i>
			                    <h4>Oops!</h4>
			                    <ul>
			                    	@foreach($errors->all() as $error)
					                    <li>{{ $error }}</li>
									@endforeach
			                    </ul>
			                </div>
						@endif
						
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" name="role_id" id="role_id" value="{{ $role->id }}"  />
                                <h5>All Permissions</h5>
                                <br />
                                <ul id="all_permissions">
                                    @foreach($permissions as $permission)
                                        @if(!in_array($permission->id, $current_permissions_id))
                                        <li class="all-permission permissions assign-permission" data-name="{{ $permission->display_name }}" data-id="{{ $permission->id }}">
                                            <span class="badge badge-lg badge-primary">{{ $permission->display_name }}</span>
                                        </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <h5>Current Permissions</h5>
                                <br />
                                <ul id="current_permissions">
                                    @foreach($current_permissions as $permission)
                                        <li class="all-permission permissions revoke-permission" data-name="{{ $permission->display_name }}" data-id="{{ $permission->id }}">
                                            <span class="badge badge-lg badge-primary">{{ $permission->display_name }}</span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
    				</div>
    			</div>
    	</div>
    </div>

@stop

@section('script')

<script type="text/javascript" src="{{ asset('admin_assets/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin_assets/global/js/Plugin/datatables.js') }}"></script>

@include('admin.imports.datatable-scripts')

<script src="{{ asset('admin_assets/global/vendor/alertify/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/notie/notie.js') }}"></script>

<script src="{{ asset('admin_assets/global/js/Plugin/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/js/Plugin/notie-js.js') }}"></script>

<!-- <script src="{{ asset('admin_assets/assets/examples/js/tables/datatable.js') }}"></script> -->
<script type="text/javascript">

	$(document).on('click', ".assign-permission", function() {
		var self = $(this);

		alertify.confirm("Are you sure you want to assign "+self.data('name')+" permission to {{ $role->display_name }}?", function() {

            var permission_id = self.data('id'),
                role_id = $("#role_id").val(),
                permission_name = self.data('name'),
                base_url = $("#baseurl").attr('content');

            // alert(permission_id + " \n " + role_id);

            $.get(base_url + "/admin/users/assign/"+permission_id+"/"+role_id, function(data) {
                self.remove();

                $("#current_permissions").append("<li class='all-permission permissions revoke-permission' data-name='"+permission_name+"' data-id='"+permission_id+"'><span class='badge badge-lg badge-primary'>"+permission_name+"</span></li>");
            });
		}, function() {
			alertify.error("Cancelled");
		});
	});

    $(document).on('click', ".revoke-permission", function() {
        var self = $(this);

        alertify.confirm("Are you sure you want to revoke "+self.data('name')+" permission to {{ $role->display_name }}?", function() {
            var permission_id = self.data('id'),
                role_id = $("#role_id").val(),
                permission_name = self.data('name'),
                base_url = $("#baseurl").attr('content');

            $.get(base_url + "/admin/users/revoke/"+permission_id+"/"+role_id, function(data) {
                self.remove();

                $("#all_permissions").append("<li class='all-permission permissions assign-permission' data-name='"+permission_name+"' data-id='"+permission_id+"'><span class='badge badge-lg badge-primary'>"+permission_name+"</span></li>");
            });
        }, function() {
            alertify.error("Cancelled");
        });
    });
</script>
@stop