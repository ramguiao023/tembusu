@extends('admin.layouts.master')

@section('style')
	@include('admin.imports.datatable-styles')

	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('title', 'Winners of ' . $event->title)

@section('content')
	<div class="float-left">
		<h2>Winners of {{ $event->title }}</h2>
	    <p>Manage Lucky Draws</p>
	</div>
	<div class="float-right">
		<a href="{{ url('admin/events/'.$event->id.'/lucky-draw') }}" class="btn btn-default">
			<i class="icon fas fa-angle-left"></i>
			Go Back
		</a>
	</div>
	<div class="clearfix"></div>
    <div class="panel">
    	<header class="panel-heading">
    	  <div class="panel-actions">
    	  	<a href="{{ url('admin/events/'.$event->id.'/winners/'.$raffle_id.'/ppt') }}" class="btn btn-danger">
				<i class="fas fa-download"></i> Generate PPT
			</a>
    	  </div>
    	  <h3 class="panel-title">
    		Winners of {{ $event->title }}
    	  </h3>
    	</header>

		<div class="panel-body">
			@if( @isset($_GET['success']))
				<div class="alert alert-success alert-icon alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="icon wb-check" aria-hidden="true"></i>
                    <h4>Nice!</h4>
                    <p>
                    	Successfully created.
                    </p>
                </div>
			@endif

			<table class="table table-hover dataTable table-striped w-full" id="userTable">
				<thead>
					<tr>
						<th>Participant</th>
						<th>Type</th>
						<th>Prize</th>
						<th>Won Date</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Participant</th>
						<th>Type</th>
						<th>Prize</th>
						<th>Won Date</th>
					</tr>
				</tfoot>
				<tbody>
					@foreach($winners as $winner)
						<tr>
							<td>{{ $winner->events_participants->participant->name }}</td>
							<td>
								@if($winner->prize->type == 1)
									<span class="badge badge-success">Lucky Draw</span>
								@else
									<span class="badge badge-info">Pre-draw</span>
								@endif
							</td>
							<td>
								{{ $winner->prize->name }}
							</td>
							<td>
								{{ date('M d, Y h:i a', strtotime($winner->created_at)) }}
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@stop

@section('script')

<script type="text/javascript" src="{{ asset('admin_assets/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin_assets/global/js/Plugin/datatables.js') }}"></script>

@include('admin.imports.datatable-scripts')

<script src="{{ asset('admin_assets/global/vendor/alertify/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/notie/notie.js') }}"></script>

<script src="{{ asset('admin_assets/global/js/Plugin/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/js/Plugin/notie-js.js') }}"></script>

<!-- <script src="{{ asset('admin_assets/assets/examples/js/tables/datatable.js') }}"></script> -->
<script type="text/javascript">
	$("#userTable").DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    
	$(".delete-user").on('click', function() {
		var self = $(this);

		alertify.confirm("Delete this user?", function() {
			var user_id = self.data('id'),
				base_url = $("#baseurl").attr('content');
			window.location = base_url + "/admin/users/delete/" + user_id;
		}, function() {
			alertify.error("Cancelled");
		});
	});
</script>
@stop