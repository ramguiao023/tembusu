@extends('admin.layouts.master')

@section('style')
	@include('admin.imports.file-upload-styles')
	
@stop

@section('title', 'Edit Lucky Draw')

@section('content')
	<h2>Lucky Draw</h2>
    <p>Edit Lucky Draw</p>

    <div class="row">
    	<div class="col-md-8">
		    <div class="panel">
		    	<header class="panel-heading">
		    	  <div class="panel-actions">
		    	  	<br />
		    	  	<a href="{{ url('admin/events/'.$event->id.'/lucky-draw') }}" class="btn btn-default" title="Add New User">
						<i class="icon fas fa-angle-left"></i> Go Back
					</a>
		    	  </div>
		    	  <h3 class="panel-title">Lucky Draw Information</h3>
		    	</header>
				<div class="panel-body mt-5">
					<div class="alert alert-danger" id="error-wrap">
						<h4>Uh oh!</h4>
						<div id="errors"></div>
					</div>

					@if( @isset($_GET['success']))
						<div class="alert alert-success alert-icon alert-dismissible" role="alert">
		                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                        <span aria-hidden="true">×</span>
		                    </button>
		                    <i class="icon wb-check" aria-hidden="true"></i>
		                    <h4>Nice!</h4>
		                    <p>
		                    	Successfully created.
		                    </p>
		                </div>
					@endif
					
					<form action="{{ url('admin/events/'.$event->id.'/lucky-draw/create') }}" method="POST">
						<input type="hidden" id="event_id" value="{{ $event->id }}" />

						<div class="row">
							<div class="col-md-6">
								<div class="form-group form-material" data-plugin="formMaterial">
									<label class="form-control-label" for="inputText">Lucky Draw Name <span class="required">*</span></label>
									<input type="text" class="form-control" name="lucky_draw_name" id="lucky_draw_name" value="{{ $raffle->name }}" placeholder="Ex. Singapore Grand Lucky Draw 2019">
								</div>
							</div>
							<div class="col-md-6" style="display: none">
								<div class="form-group form-material" data-plugin="formMaterial">
									<label class="form-control-label" for="inputText">
										Shift Personnel
										<span class="required">*</span>
									</label>
									<select id="shift_personnel" class="form-control">
										@foreach($users as $user)
											<option {{ ($user->id == $raffle->shift_personnel) ? "selected='selected'" : "" }} value="{{ $user->id }}">{{ $user->first_name . " " . $user->last_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>

						<div class="form-group form-material" data-plugin="formMaterial">
						    <label class="form-control-label" for="inputText">Lucky Background Image</label>
						    <input type="file" id="lucky_draw_image" name="image" data-plugin="dropify" data-default-file="{{ ($raffle->image) ? $raffle->image : '' }}" accept="image/x-png,image/gif,image/jpeg" />
						</div>
						<textarea id="base64textarea" cols="30" rows="10" style="display: none;"></textarea>
						<hr />
						<h4 class="float-left">Prizes</h4>
						<a href="javascript:void(0)" id="add-prize" class="btn float-right btn-round btn-outline btn-default">
							<i class="icon fas fa-plus"></i> Add Prize
						</a>
						<div class="clearfix"></div>
						<br />
						<div id="prizes">
							@if(count($raffle->prizes)<=0)
								<div class="alert alert-info">Please add prizes by clicking the <strong><i>add prize</i></strong> button.</div>
							@endif

							@foreach($raffle->prizes as $prize)
								<div class="row">
									<input type="hidden" value="{{ $prize->id }}" class="form-control prizes_id" placeholder="What's the prize?" placeholder="" />
									<div class="col-md-4">
										<label>Prize <span class="required">*</span></label>
										<input type="text" value="{{ $prize->name }}" class="form-control prizes" placeholder="What's the prize?" placeholder="" />
									</div>
									<div class="col-md-4">
										<label>Number of Winners <span class="required">*</span></label>
										<input type="number" value="{{ $prize->number_of_winners }}" class="form-control number_of_winners" placeholder="How many winners can get this prize?" />
									</div>
									<div class="col-md-4">
										<label>Prize Type <span class="required">*</span></label>
										<select class="form-control type">
											<option {{ ($prize->type == 1) ? "selected='selected'" : "" }} value="1">Lucky Draw Prize</option>
											<option {{ ($prize->type == 2) ? "selected='selected'" : "" }} value="2">Pre-Draw Prize</option>
										</select>
									</div>
								</div>

								<hr />
							@endforeach
						</div>
						<br />
						<div class="form-group">
							<button type="button" id="update-lucky-draw" data-id='{{ $raffle->id }}' class="btn btn-primary btn-lg float-right">
								<i class="icon fas fa-save"></i> Update
							</button>
						</div>
					</form>
				</div>
			</div>
    	</div>
    </div>
@stop

@section('script')
	@include('admin.imports.file-upload-scripts')

	<script src="{{ asset('admin_assets/assets/js/lucky-draw.js') }}"></script>
@stop