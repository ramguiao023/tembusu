@extends('admin.layouts.lucky-draw')

@section('content')
	<div class="raffle-wrap">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-8 mx-auto">
					<div class="alert alert-warning">
						<h3>Note:</h3>
						<p>
							{{ $warning }}
						</p>
					</div>
					<div class="float-left">
						<h2>{{ $event->title }}</h2>
						<h4>{{ $event->caption }}</h4>
					</div>

					<div class="float-right">
						<a href="{{ url('admin/events/'.$event->id.'/lucky-draw') }}">
							<i class="fas fa-angle-left"></i>
							Go Back
						</a>
					</div>
					<div class="clearfix"></div>
					<hr />
					<br />
					<h5>Select prize to draw for {{ $raffle->name }}</h5>
					<br />
					@php
						$ctr = count($raffle->prizes);
					@endphp
					@foreach($raffle->prizes as $prize)
						<div class="row">
							<div class="col-md-4 prize-item">
								<a href="{{ url('admin/events/'.$event->id.'/lucky-draw/'.$raffle->id.'/draw/'.$prize->id) }}">
									<h4>Prize #{{ $ctr . ": " . $prize->name }}</h4>
									<span class="float-right"></span>
								</a>
							</div>
						</div>
						@php
							$ctr--;
						@endphp
					@endforeach
				</div>
			</div>
		</div>
	</div>
@stop

@section('script')
	<script src="{{ asset('admin_assets/assets/js/draw.js') }}"></script>
@stop