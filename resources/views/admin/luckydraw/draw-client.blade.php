@extends('admin.layouts.lucky-draw')

@if($raffle->image)
  @section('style', 'background: url('.$raffle->image.') !important; background-size: cover !important;')
@else 
  @section('style', '')
@endif

@section('content')
<div class="raffle-wrap">
    <audio id="successAudio">
      <source src="/sfx/drum-roll.mp3" type="audio/mpeg">
    </audio>

    <center>
      <h3 id="prize-name">
        {{-- Lucky Draw Prize # $prize_number :--}} {{ $prize->name }}
      </h3>
      <br />
      <div class="odometer"></div>
      <br />

      <div id="winner">
        <h2 id="winner-name"></h2>
        <h4 id="winner-employee-number"></h4>
      </div>
    </center>
  </div>
@stop

@section('script')
  <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
  <script>
    var el = document.querySelector('.odometer');

    od = new Odometer({
      el: el,
      value: 100000,
      duration: 10000,
      // Any option (other than auto and selector) can be passed in here
      format: '',
    });


  $(".odometer-value").text(0);

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('0047d5efb8a4f3221be3', {
      cluster: 'ap1',
      forceTLS: true
    });


    var channel = pusher.subscribe("prize<?= $prize->id ?>");

    channel.bind('my-event', function(data) {

      var audio = document.getElementById("successAudio");
      audio.play();
      var arr = JSON.parse(data.message);
      console.log(arr[0]);
      od.update(arr[0]);

      setTimeout(function() {
        $("#winner").show();
        $("#winner-name").html(arr[1]);
        $("#winner-employee-number").html(arr[2]);
      }, 4000);
    });
  </script>
@stop