@extends('admin.layouts.master')

@section('style')
	@include('admin.imports.datatable-styles')

	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('title', 'Lucky Draws of ' . $event->title)

@section('content')
	<div class="float-left">
		<h2>Lucky Draws of {{ $event->title }}</h2>
	    <p>Manage Lucky Draws</p>
	</div>
	<div class="float-right">
		<a href="{{ url('admin/events') }}" class="btn btn-default">
			<i class="icon fas fa-angle-left"></i>
			Go Back
		</a>
	</div>
	<div class="clearfix"></div>
    <div class="panel">
    	<header class="panel-heading">
    	  <div class="panel-actions">
    	  	<a href="{{ url('admin/events/'.$event->id.'/lucky-draw/create') }}" class="btn btn-success" title="Add New User">
				<i class="icon glyphicon glyphicon-plus"></i> Create New Lucky Draw
			</a>
    	  </div>
    	  <h3 class="panel-title">
    		Lucky Draws of {{ $event->title }}
    	  </h3>
    	</header>

		<div class="panel-body">
			@if( @isset($_GET['success']))
				<div class="alert alert-success alert-icon alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="icon wb-check" aria-hidden="true"></i>
                    <h4>Nice!</h4>
                    <p>
                    	Successfully created.
                    </p>
                </div>
			@endif

			<table class="table table-hover dataTable table-striped w-full" id="userTable">
				<thead>
					<tr>
						<th width="100">Image</th>
						<th>Name</th>
						<th>Number of Prizes</th>
						<th>Shift Personnel</th>
						<th>Action</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th width="100">Image</th>
						<th>Name</th>
						<th>Number of Draw</th>
						<th>Shift Personnel</th>
						<th>Action</th>
					</tr>
				</tfoot>
				<tbody>
					@foreach($raffles as $raffle)
					<tr>
						<td>
							<img src="{{ $raffle->image }}" alt="" class="img-fluid" />
						</td>
						<td>
							<strong>
								{{ $raffle->name }}
							</strong>
						</td>
						<td>
							{{ @count($raffle->prizes) }}
						</td>
						<td>
							{{ @$raffle->user->first_name . " " . @$raffle->user->last_name }}
						</td>
						<td>
							<a href="{{ url('admin/events/'.$event->id.'/lucky-draw/'.$raffle->id."/edit") }}" class="btn btn-primary">
								<i class="fas fa-edit"></i>
							</a>
							
							<a href="{{ url('admin/events/'.$event->id.'/winners/'.$raffle->id) }}" class="btn btn-danger">
								<i class="fas fa-trophy"></i>
							</a>
							
							<br />
							<br />
							<a href="{{ url('admin/events/'.$event->id.'/lucky-draw/'.$raffle->id."/prizes") }}" class="btn btn-success">
								<i class="fas fa-gift"></i>  Lucky Draw
							</a>
							<a href="{{ url('admin/pre-draw/'.$raffle->id) }}" class="btn btn-warning">
								<i class="fas fa-gift"></i>  Pre-draw
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@stop

@section('script')

<script type="text/javascript" src="{{ asset('admin_assets/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin_assets/global/js/Plugin/datatables.js') }}"></script>

@include('admin.imports.datatable-scripts')

<script src="{{ asset('admin_assets/global/vendor/alertify/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/notie/notie.js') }}"></script>

<script src="{{ asset('admin_assets/global/js/Plugin/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/js/Plugin/notie-js.js') }}"></script>

<!-- <script src="{{ asset('admin_assets/assets/examples/js/tables/datatable.js') }}"></script> -->
<script type="text/javascript">
	$("#userTable").DataTable();
	$(".delete-user").on('click', function() {
		var self = $(this);

		alertify.confirm("Delete this user?", function() {
			var user_id = self.data('id'),
				base_url = $("#baseurl").attr('content');
			window.location = base_url + "/admin/users/delete/" + user_id;
		}, function() {
			alertify.error("Cancelled");
		});
	});
</script>
@stop