@extends('admin.layouts.master')

@section('style')
	@include('admin.imports.datatable-styles')

	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('title', 'Lucky Draws')

@section('content')
	<div class="preloader-wrap">
		<div class="preloader">
			<h4>Please Wait...</h4>
			<h1>Selecting Winner</h1>
		</div>
	</div>
	<div class="float-left">
		<h2>Pre-draw</h2>
	    <p>Start pre-draw for {{ $raffle->name }}</p>
	</div>
	<div class="float-right">
		<a href="{{ url('admin/events') }}" class="btn btn-default">
			<i class="icon fas fa-angle-left"></i>
			Go Back
		</a>
	</div>
	<div class="clearfix"></div>
    <div class="panel">
    	<header class="panel-heading">
    	  <div class="panel-actions">
    	  
    	  </div>
    	  <h3 class="panel-title">
    	  </h3>
    	</header>
		
		<input type="hidden" id="event_id" value="{{ $raffle->event_id }}" />

		<div class="panel-body">
			<ol reversed="">
			@foreach($raffle->prizes as $prize)
				<li style="font-size: 2rem">
					{{ $prize->name }}
				</li>
				<table class="table">
					<thead>
						<tr>
							<th>Number of Winners</th>
							<th>Winners</th>
							<th>Action</th>
						</tr>
					</thead>
					<tr>
						<td style="font-size: 1.3rem;">
							
							{!! "<span id='winners_out_of".$prize->id."'>".count($prize->winners)."</span>" . " winners out of <span id='number_of_total_looking".$prize->id."'>" . $prize->number_of_winners . "</span>" !!}
						</td>
						<td id="winners{{ $prize->id }}">
							<ol>
							@foreach($prize->winners as $winner)
								<li id="winner{{ $winner->id }}" style="margin-bottom: 20px; font-size: 1.4rem;">
									{{ @$winner->events_participants->participant->name . " - " . @$winner->events_participants->participant->employee_number }}
									<a href="javascript:void(0)" data-raffleid='{{ $raffle->id }}' data-winnerid="{{ $winner->id }}" data-eventsparticipantsid="{{ $winner->events_participants_id }}" data-prizeid="{{ $prize->id }}" title="Redraw" class="redraw btn btn-icon btn-sm btn-success">
										<i class="icon fas fa-refresh"></i>
									</a>
								</li>
							@endforeach
							</ol>
						</td>
						<td>
							<input type="hidden" id="number_of_remaining{{ $prize->id }}" value="{{ $prize->number_of_winners - count($prize->winners) }}" />
							<input type="text" placeholder="Number of draw" id="number_of_draw{{ $prize->id }}" />

							<a href="javascript:void(0)" data-raffleid='{{ $raffle->id }}' data-prizeid='{{ $prize->id }}' class="start_pre_draw btn btn-success">
								Start Pre-draw
							</a>
						</td>
					</tr>
				</table>
				<br />
			@endforeach
			</ol>
		</div>
	</div>
@stop

@section('script')

<script type="text/javascript" src="{{ asset('admin_assets/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin_assets/global/js/Plugin/datatables.js') }}"></script>

@include('admin.imports.datatable-scripts')

<script src="{{ asset('admin_assets/global/vendor/alertify/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/notie/notie.js') }}"></script>

<script src="{{ asset('admin_assets/global/js/Plugin/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/js/Plugin/notie-js.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- <script src="{{ asset('admin_assets/assets/examples/js/tables/datatable.js') }}"></script> -->
<script type="text/javascript">
	$("#userTable").DataTable();
	$(".delete-user").on('click', function() {
		var self = $(this);

		alertify.confirm("Delete this user?", function() {
			var user_id = self.data('id'),
				base_url = $("#baseurl").attr('content');
			window.location = base_url + "/admin/users/delete/" + user_id;
		}, function() {
			alertify.error("Cancelled");
		});
	});

	$(document).on('click', ".redraw", function(data) {
		var button = $(this),
			conf = confirm("Are you sure you want to continue?");

		if(conf) {
			$(".preloader-wrap").show();

			var winner_id = button.data('winnerid'),
				prize_id = button.data('prizeid'),
				raffle_id = button.data('raffleid'),
				event_id = $("#event_id").val(),
				events_participants_id = button.data('eventsparticipantsid');

			var url = $("#baseurl").attr('content'),
				token = $("#token").attr('content');

			$.post(url + "/admin/re-draw", {
					_token: token,
					winner_id,
					prize_id,
					raffle_id,
					events_participants_id,
					event_id,
				},
				function(data) {
					var html = '<a href="javascript:void(0)" data-raffleid="'+data.raffle_id+'" data-winnerid="'+data.id+'" data-eventsparticipantsid="'+data.events_participants.id+'" data-prizeid="'+data.prize_id+'" title="Redraw" class="redraw btn btn-icon btn-sm btn-success">' +
										'<i class="icon fas fa-refresh"></i>' +
									'</a>';

					$("#winner"+winner_id).attr('id', 'winner'+data.id);

					$("#winner"+data.id).html(data.events_participants.participant.name + " - " + data.events_participants.participant.employee_number + " " + html);

					setTimeout(function() {
						console.log($("#winner"+data.id).html());
						$("#winner"+data.id).animate({
							backgroundColor: 'blue'
						});

						$("#winner"+data.id).animate({
							backgroundColor: '#FFF'
						});

						$(".preloader-wrap").hide();
					}, 300);
			})
			.catch(function(e) {
				alert("There are no remaining participants.");

				setTimeout(function() {
					$(".winnersitem").animate({
						backgroundColor: 'blue'
					});

					$(".winnersitem").animate({
						backgroundColor: '#FFF'
					});
					$(".preloader-wrap").hide();
				}, 300);
			});
		}
	});

	$(".start_pre_draw").click(function() {
		var draw_button = $(this),
			url = $("#baseurl").attr('content'),
			token = $("#token").attr('content');

		var raffle_id = draw_button.data('raffleid');
		var prize_id = draw_button.data('prizeid');
		var event_id = $("#event_id").val();
		var number_of_draws = $("#number_of_draw"+prize_id).val();
		var number_of_remaining = $("#number_of_remaining"+prize_id).val();

		if(parseInt(number_of_remaining) <= 0) {
			alert("All slots has been won.");
		} else if(number_of_draws == 0 || number_of_draws == "" || isNaN(number_of_draws)) {
			alert("Please indicate number of draws. It should be a number.");
		} else {
			if(parseInt(number_of_draws) > parseInt(number_of_remaining)) {
				alert("Number of winners should not exceed the remaning slots of winners. Currently looking for " + number_of_remaining + " more.");
			} else {
				$(".preloader-wrap").show();

				$.post(url + "/admin/pre-draw",
				{
					"_token": token,
					number_of_remaining,
					raffle_id,
					prize_id,
					event_id,
					number_of_draws
				},
				function(data) {
					if(data.length <= 0) {
						alert("No participants record yet.");
					} else {
						var html = "<ol>";

						for(var i=0; i<data.length; i++) {
							html = html + "<li id='winner"+data[i].id+"' class='winnersitem'  style='margin-bottom: 20px; font-size: 1.4rem;'>"+data[i].events_participants.participant.name+" - " + data[i].events_participants.participant.employee_number + 
								"<a href='javascript:void(0)' data-raffleid='"+data[i].raffle_id+"' data-winnerid='"+data[i].id+"' data-eventsparticipantsid='"+data[i].events_participants.id+"' data-prizeid='"+data[i].prize_id+"' title='Redraw' class='redraw btn btn-icon btn-sm btn-success'>" +
										"<i class='icon fas fa-refresh'></i>" +
									"</a>" +
								"</li>";
						}

						html = html + "</ol>";

						$("#winners"+prize_id).html(html);

						$("#winners_out_of"+prize_id).html(data.length);

						var total_looking = parseInt($("#number_of_total_looking"+prize_id).html());

						$("#number_of_remaining"+prize_id).val(total_looking - data.length);

						setTimeout(function() {
							$(".winnersitem").animate({
								backgroundColor: 'blue'
							});

							$(".winnersitem").animate({
								backgroundColor: '#FFF'
							});
							$(".preloader-wrap").hide();
						}, 300);
					}
				})
				.catch(function(e) {
					alert(e.responseJSON.message + " Please try again.");

					setTimeout(function() {
						$(".winnersitem").animate({
							backgroundColor: 'blue'
						});

						$(".winnersitem").animate({
							backgroundColor: '#FFF'
						});
						$(".preloader-wrap").hide();
					}, 300);
				});
			}
		}
	});
</script>
@stop