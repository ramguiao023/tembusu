@extends('admin.layouts.master')

@section('style')
	@include('admin.imports.file-upload-styles')
	
@stop

@section('title', 'Create New Lucky Draw')

@section('content')
	<h2>Lucky Draw</h2>
    <p>Create New Lucky Draw</p>

    <div class="row">
    	<div class="col-md-8">
		    <div class="panel">
		    	<header class="panel-heading">
		    	  <div class="panel-actions">
		    	  	<br />
		    	  	<a href="{{ url('admin/events/'.$event->id.'/lucky-draw') }}" class="btn btn-default" title="Add New User">
						<i class="icon fas fa-angle-left"></i> Go Back
					</a>
		    	  </div>
		    	  <h3 class="panel-title">Lucky Draw Information</h3>
		    	</header>
				<div class="panel-body mt-5">
					<div class="alert alert-danger" id="error-wrap">
						<h4>Uh oh!</h4>
						<div id="errors"></div>
					</div>
					<form id="raffle-form" action="{{ url('admin/events/'.$event->id.'/lucky-draw') }}" method="POST" enctype="multipart/form-data">
						<input type="hidden" id="event_id" name="event_id" value="{{ $event->id }}" />
						{{ csrf_field() }}
						<div class="row">
							<div class="col-md-6">
								<div class="form-group form-material" data-plugin="formMaterial">
									<label class="form-control-label" for="inputText">Lucky Draw Name <span class="required">*</span></label>
									<input type="text" class="form-control" name="lucky_draw_name" id="lucky_draw_name" value="" placeholder="Ex. Singapore Grand Lucky Draw 2019">
								</div>
							</div>
							<div class="col-md-6" style="display: none">
								<div class="form-group form-material" data-plugin="formMaterial">
									<label class="form-control-label" for="inputText">
										Shift Personnel
										<span class="required">*</span>
									</label>
									<select id="shift_personnel" name="shift_personnel" class="form-control">
										<option value=""></option>
										@foreach($users as $user)
											<option value="{{ $user->id }}">{{ $user->first_name . " " . $user->last_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>

						<div class="form-group form-material" data-plugin="formMaterial">
						    <label class="form-control-label" for="inputText">Lucky Background Image</label>
						    <input type="file" id="lucky_draw_image" name="lucky_draw_background_image" data-plugin="dropify" data-default-file="" accept="image/x-png,image/gif,image/jpeg" />
						</div>
						<textarea id="base64textarea" cols="30" rows="10" style="display: none;"></textarea>
						<hr />
						<div>
							<label>
								<input type="checkbox" checked="" id="upload_manually" name="upload_manually" value="1"> <strong>Upload Manually?
							</label></strong>
						</div>

						<div id="manual-upload">
							<h4 class="float-left">Prizes</h4>
							<a href="javascript:void(0)" id="add-prize" class="btn float-right btn-round btn-outline btn-default">
								<i class="icon fas fa-plus"></i> Add Prize
							</a>
							<div class="clearfix"></div>
							<br />
							<div id="prizes">
								<div class="alert alert-info">Please add prizes by clicking the <strong><i>add prize</i></strong> button.</div>
							</div>
						</div>

						<div id="auto-upload">
							<h4>Please Select an XLS file</h4>
							<br />
							<input type="file" name="xls" class="form-control" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
						</div>

						<br />
						<div class="form-group">
							<button type="button" id="create-lucky-draw" class="btn btn-primary btn-lg float-right">
								<i class="icon fas fa-save"></i> Create
							</button>
						</div>
					</form>
				</div>
			</div>
    	</div>
    </div>
@stop

@section('script')
	@include('admin.imports.file-upload-scripts')

	<script src="{{ asset('admin_assets/assets/js/lucky-draw.js') }}"></script>
@stop