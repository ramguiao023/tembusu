@extends('admin.layouts.lucky-draw')

@section('body_class', 'draw')

@section('content')
	{{-- <div class="bg-lucky-draw" style="background-image: url('http://tembusu.test/uploads/events/events-15617010505d15aaba69283.jpg')">
	</div> --}}
	<!-- Modal -->
	<!-- Button trigger modal -->
	

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Winners</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	@if(count($winners) >= $prize->number_of_winners)
				{{-- <div class="alert alert-success">
					<h4>Lucky draw for Prize #{{ $prize_number }} has been completed</h4>
					<br />
					<a href="{{ url('admin/events/'.$event->id.'/lucky-draw/'.$raffle->id.'/prizes') }}" class="btn btn-primary">
						Next Prize <i class="fas fa-angle-right"></i>
					</a>
				</div> --}}
	      	@endif
	      	<table class="table">
	      		<thead>
	      			<tr>
	      				<th>Name</th>
	      				<th>Employee Number</th>
	      				<th>Action</th>
	      			</tr>
	      		</thead>
	      		<tbody id="winners-html">
	      			
	      		</tbody>
	      	</table>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>


	<div class="raffle-wrap">
		<audio id="successAudio">
		  <source src="/sfx/drum-roll.mp3" type="audio/mpeg">
		</audio>

		<center>
			<h3>
				Lucky Draw Prize #{{ $prize_number }}: {{ $prize->name }}
			</h3>
			<h4 class="title">
				{{ $event->title }}
			</h4>
			<br />
			<a href="{{ url('/admin/events/'.$event->id.'/'.'lucky-draw'.'/'.$raffle->id.'/'.'draw'.'/'.$prize->id.'/client') }}" target="_blank" class="btn btn-primary"><i class="fas fa-window-restore"></i></a>
			<br />
			<div class="odometer"></div>
			<br />
			<input type="hidden" id="raffle_id" value="{{ $raffle->id }}" />
			<input type="hidden" id="event_id" value="{{ $event->id }}" />
			<input type="hidden" id="prize_id" value="{{ $prize->id }}" />
			<input type="hidden" id="winner_id" value="" />
			
			<div id="winner" data-toggle="modal" data-target="#exampleModal">
				<h2 id="winner-name"></h2>
				<h4 id="winner-employee-number"></h4>
			</div>

			@if(count($winners) < $prize->number_of_winners)
				<div class="mt-5">
					<button id="start-draw" class="btn btn-lg btn-primary">
						<i class="fas fa-star"></i> DRAW
					</button>

					<button id="re-draw" winnerid="123" class="btn btn-lg btn-default" style="margin-left: 10px;">
						<i class="fas fa-star"></i> RE-DRAW
					</button>	
				</div>
				<br />
				<br />
			@else
				<br />
				<div class="mt-5">
					<button id="re-draw" winnerid="123" class="btn btn-lg btn-default" style="margin-left: 10px;">
						<i class="fas fa-star"></i> RE-DRAW
					</button>	
				</div>
			@endif

			<div class="alert alert-success mt-5" id="success-draw">
				<h4>Lucky draw for Prize #{{ $prize_number }} has been completed</h4>
				<br />
				<a href="{{ url('admin/events/'.$event->id.'/lucky-draw/'.$raffle->id.'/prizes') }}" class="btn btn-primary">
					Next Prize <i class="fas fa-angle-right"></i>
				</a>
				<br />
				<br />
				<a href="{{ url('admin/events/'.$event->id.'/lucky-draw/'.$raffle->id.'/prizes') }}">
					<i class="fas fa-angle-left"></i> Go Back
				</a>
			</div>

			<div class="alert alert-danger" id="failed-draw">
				<h4>There are no participants record yet.</h4>
				<br />
				<a href="{{ url('admin/events/'.$event->id.'/lucky-draw/'.$raffle->id.'/prizes') }}">
					<i class="fas fa-angle-left"></i> Go Back
				</a>
			</div>

			<button type="button" class="winner-wrap" data-toggle="modal" data-target="#exampleModal">
			  <h4><span id="current-count">{{ count($winners) }}</span>/<span id="total-count">{{ $prize->number_of_winners }}</span></h4>
			</button>

			
		</center>
	</div>
	
	<div class="prize-drawer">
		{{-- <a href="javascript:void(0)" data-show='1' id="show-prizes" class="show-button btn btn-icon btn-danger">
			<i class="icon fas fa-angle-up"></i>
		</a> --}}
		<div class="prizes-wrap">
			<h4 style="margin-right: 10px; padding: 0; color: #FFF;">Prizes</h4>
			@foreach($raffle->prizes as $prize)
				<div class="prize">
					<h5>
						{{ $prize->name }}
					</h5>
				</div>
			@endforeach
		</div>
	</div>
	
@stop

@section('script')
	<script src="{{ asset('admin_assets/assets/js/draw.js') }}"></script>
@stop