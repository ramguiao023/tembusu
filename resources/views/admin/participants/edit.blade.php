@extends('admin.layouts.master')

@section('title', 'Participants')

@section('style')
	@include('admin.imports.datepicker-styles')
	@include('admin.imports.datatable-styles')

	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('content')
	<div class="float-left">
		<h3>Edit Participant: {{ $participant->name }}</h3>
		<p>Single Participant</p>
	</div>
	
	<div class="float-right">
		<a href="{{ url('admin/events/'.$event->id.'/participants') }}" class="btn btn-default" title="Add New User">
			<i class="icon glyphicon glyphicon-chevron-left"></i> Go Back
		</a>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel">
		    	<div class="panel-body">
		    		<form method="POST" action="{{ url('/admin/participants/'.$participant->id) }}" autocomplete="off">
		    			<input type="hidden" name="_method" value="PUT" />
		    			<input type="hidden" name="event_id" value="{{ $event->id }}" />
		    			
		    			@if(session('success'))
							<div class="alert alert-success alert-icon alert-dismissible" role="alert">
			                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                        <span aria-hidden="true">×</span>
			                    </button>
			                    <i class="icon wb-check" aria-hidden="true"></i>
			                    <h4>Nice!</h4>
			                    <p>
			                    	{{ session('success') }}
			                    </p>
			                </div>
						@endif

		    			@if(count($errors) > 0)
							<div class="alert alert-danger alert-icon alert-dismissible" role="alert">
			                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                        <span aria-hidden="true">×</span>
			                    </button>
			                    <i class="icon wb-warning" aria-hidden="true"></i>
			                    <h4>Oops!</h4>
			                    <ul>
			                    	@foreach($errors->all() as $error)
					                    <li>{{ $error }}</li>
									@endforeach
			                    </ul>
			                </div>
						@endif

		    			<h4>Basic Information</h4>
		    			<hr />
		    			{{ csrf_field() }}

    					<div class="form-group">
    						<img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')
    						                        ->size(240)->errorCorrection('H')
    						                        ->generate($participant->qrid)) !!} ">
    					</div>
		    			<div class="form-group">
		    				<label>Employee Number <span class="required">*</span></label>
		    				<input type="text" name="employee_number" value="{{ old('employee_number', $participant->employee_number) }}" class="form-control" />
		    			</div>

		    			
		    			<div class="form-group">
		    				<div class="row">
		    					<div class="col-md-6">
		    						<div class="form-group">
		    							<label>Salutation</label>
		    							<select name="salutation" class="form-control">
		    								<option value="">Salutation</option>
		    								<option {{ (old('salutation') == "Mr." || $participant->salutation == "Mr.") ? "selected='selected'" : "" }} value="Mr.">Mr.</option>
		    								<option {{ (old('salutation') == "Ms." || $participant->salutation == "Ms.") ? "selected='selected'" : "" }} value="Ms.">Ms.</option>
		    								<option {{ (old('salutation') == "Mrs." || $participant->salutation == "Mrs.") ? "selected='selected'" : "" }} value="Mrs.">Mrs.</option>
		    								<option {{ (old('salutation') == "Dr." || $participant->salutation == "Dr.") ? "selected='selected'" : "" }} value="Dr.">Dr.</option>
		    								<option {{ (old('salutation') == "Engr." || $participant->salutation == "Engr.") ? "selected='selected'" : "" }} value="Engr.">Engr.</option>
		    								<option {{ (old('salutation') == "Atty." || $participant->salutation == "Atty.") ? "selected='selected'" : "" }} value="Atty.">Atty.</option>
		    								<option {{ (old('salutation') == "Dean" || $participant->salutation == "Dean") ? "selected='selected'" : "" }} value="Dean">Dean</option>
		    								<option {{ (old('salutation') == "Prof." || $participant->salutation == "Prof.") ? "selected='selected'" : "" }} value="Prof.">Prof.</option>
		    							</select>	
		    						</div>
		    					</div>
		    					<div class="col-md-6">
		    						<label>Full Name <span class="required">*</span></label>
		    						<input type="text" name="name" value="{{ old('name', $participant->name) }}" class="form-control" />
		    					</div>
		    				</div>
		    			</div>
		    			<div class="form-group">
		    				<label>Company <span class="required">*</span></label>
		    				<select name="company_id" class="form-control">
		    					<option value="">Company</option>
		    					@foreach($companies as $company)
		    						<option value="{{ $company->id }}" {{ (old('company') == $company->id || $participant->company_id == $company->id) ? "selected='selected'" : "" }}>{{ $company->name }}</option>
		    					@endforeach
		    				</select>	
		    			</div>
		    			<div class="form-group">
		    				<label>Birthdate <span class="required">*</span></label>
		    				<input type="text" name="birthdate" value="{{ old('birthdate', $participant->birthdate) }}" class="date form-control" placeholder="YYYY-MM-DD">
		    			</div>
		    			<div class="form-group">
		    				<label>Email <span class="required">*</span></label>
		    				<input type="text" name="email" value="{{ old('email', $participant->email) }}" class="form-control" />
		    			</div>

		    			<div class="form-group">
                            <label>
                                Allow participants to choose diet preferences?
                            </label>
                            <br />
                            <label>
                                <input type="radio" name="show_diet_preferences" value="1" {{ (old('show_diet_preferences', $participant->show_diet_preferences) == 1) ? "checked='checked'" : "" }} /> Yes
                            </label>

                            <label>
                                <input type="radio" name="show_diet_preferences" value="0" {{ (old('show_diet_preferences', $participant->show_diet_preferences) == 0) ? "checked='checked'" : "" }} /> No
                            </label>
                        </div>

		    			<div class="form-group">
							<label>Department {{ $participant->department_id }}</label>
							<select name="department" class="form-control">
								<option value="">Choose Department</option>
								@foreach($departments as $department)
									<option {{ ($department->id == $participant->department_id) ? "selected='selected" : "" }} value="{{ $department->id }}">{{ $department->name }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label>Table Number</label>
							<input type="text" name="table_number" value="{{ old('table_number', $participant->event_participant->table_number) }}" class="form-control" />
						</div>

						<div class="form-group">
							<label>Lucky Draw Number <br /><small>Leave this blank so the system will generate a unique <strong>6 digit code</strong></small></label>
							<input type="text" name="raffle_code" value="{{ old('raffle_code', $participant->event_participant->raffle_code) }}" placeholder="xxxxxx" class="form-control" />
						</div>

		    			<h4>Access Credentials</h4>
		    			<hr />
		    			<div class="form-group">
		    				<label>Username <span class="required">*</span></label>
		    				<input type="text" name="username" value="{{ old('username', $participant->username) }}" class="form-control" />
		    			</div>
		    			<div class="form-group" style="display: none;">
		    				@if($participant->is_email_sent == 1)
		    					<label>
		    						<input type="checkbox" name="resent_email" value="1" {{ (old('resent_email')) ? "checked='checked'" : "" }} /> Resent registration email?
		    					</label>
		    				@else
		    					<label><input type="checkbox" name="is_email_sent" value="1" {{ (old('is_email_sent')) ? "checked='checked'" : "" }} /> Send email to participant to complete registration?</label>
		    				@endif
		    			</div>
		    			<hr />
		    			<div class="form-group">
		    				<a href="javascript:void(0)" class="btn btn-default float-left delete-participant" data-id="{{ $participant->id }}">
		    					<i class="icon glyphicon glyphicon-remove"></i>
		    					Delete this participant
		    				</a>
		    				<button type="submit" class="btn btn-primary float-right">
		    					<i class="icon glyphicon glyphicon-check"></i>
		    					Save
		    				</button>

		    				<div class="clearfix"></div>
		    			</div>
		    		</form>
		    	</div>
		    </div>
		</div>
	</div>
@stop


@section('script')
	@include('admin.imports.datepicker-scripts')
	<script src="{{ asset('admin_assets/global/vendor/alertify/alertify.js') }}"></script>
	<script src="{{ asset('admin_assets/global/vendor/notie/notie.js') }}"></script>

	<script src="{{ asset('admin_assets/global/js/Plugin/alertify.js') }}"></script>
	<script src="{{ asset('admin_assets/global/js/Plugin/notie-js.js') }}"></script>

	<script type="text/javascript">
		$('.date').datepicker({
            'format': 'yyyy-mm-dd',
            'autoclose': true
        });

		$(".delete-participant").on('click', function() {
		var self = $(this);

		alertify.confirm("Delete this participant?", function() {
				var id = self.data('id'),
					base_url = $("#baseurl").attr('content');
				window.location = base_url + "/admin/participants/" + id + "/delete";
			}, function() {
				alertify.error("Cancelled");
			});
		});
	</script>
@stop