@extends('admin.layouts.master')

@section('title', 'Participants')

@section('style')
	@include('admin.imports.datatable-styles')

	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
    
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload.css') }}">
	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/dropify/dropify.css') }}">
@stop

@section('content')
	<h2>Add Participants</h2>
	<p>Bulk Participant</p>

	<div class="row">
		<div class="col-md-6">
			<div class="panel">
		    	<header class="panel-heading">
		    	  <div class="panel-actions">
		    	  	<a href="{{ url('admin/events/'.$event_id.'/participants') }}" class="btn btn-default">
						<i class="icon glyphicon glyphicon-chevron-left"></i> Go Back
					</a>
		    	  </div>
		    	  <h5 class="panel-title" style="visibility: hidden">Fill-out all the Necessary Fields</h5>
		    	</header>

		    	<div class="panel-body">
		    		<form method="POST" action="{{ url('/admin/participants/import') }}" enctype="multipart/form-data">
		    			@if(count($errors) > 0)
							<div class="alert alert-danger alert-icon alert-dismissible" role="alert">
			                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                        <span aria-hidden="true">×</span>
			                    </button>
			                    <i class="icon wb-warning" aria-hidden="true"></i>
			                    <h4>Oops!</h4>
			                    <ul>
			                    	@foreach($errors->all() as $error)
					                    <li>{{ $error }}</li>
									@endforeach
			                    </ul>
			                </div>
						@endif

						@if(session('error'))
							<div class="alert alert-danger alert-icon alert-dismissible" role="alert">
			                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                        <span aria-hidden="true">×</span>
			                    </button>
			                    <i class="icon wb-warning" aria-hidden="true"></i>
			                    <h4>Oops!</h4>
			                    <p>
			                    	{{ session('error') }}
			                    </p>
			                </div>
						@endif

						@if(session('success'))
							<div class="alert alert-success alert-icon alert-dismissible" role="alert">
			                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                        <span aria-hidden="true">×</span>
			                    </button>
			                    <i class="icon wb-warning" aria-hidden="true"></i>
			                    <h4>Nice!</h4>
			                    <p>
			                    	{{ session('success') }}
			                    </p>
			                </div>
						@endif

						<input type="hidden" name="event_id" value="{{ $event_id }}">

		    			<div class="form-group" style="display: none;">
		    				<label>Company <span class="required">*</span></label>
		    				<select name="company_id" class="form-control" readonly='readonly'>
		    					<option value="">Company</option>
		    					@foreach($companies as $company)
		    						<option value="{{ $company->id }}" {{ ($event->company_id == $company->id) ? "selected='selected'" : "" }}>{{ $company->name }}</option>
		    					@endforeach
		    				</select>	
		    			</div>
		    			


		    			<label>
		    				Participants Files (XLS, XLSX, CSV)
		    				<span class="required">*</span>
		    			</label>
		    			{{ csrf_field() }}

		    			<div class="form-group form-material" data-plugin="formMaterial">
							<input type="file" id="input-file-now" name="participants" data-plugin="dropify" data-default-file="" />
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">
								<i class="icon glyphicon glyphicon-upload"></i> Upload
							</button>
						</div>
		    		</form>
		    	</div>
		    </div>
		</div>
	</div>
@stop


@section('script')
	<script src="{{ asset('admin_assets/global/vendor/jquery-ui/jquery-ui.js') }}"></script>
	<script src="{{ asset('admin_assets/global/vendor/blueimp-tmpl/tmpl.js') }}"></script>
	<script src="{{ asset('admin_assets/global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js') }}"></script>
	<script src="{{ asset('admin_assets/global/vendor/blueimp-load-image/load-image.all.min.js') }}"></script>
	<script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload.js') }}"></script>
	<script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-process.js') }}"></script>
	<script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-image.js') }}"></script>
	<script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-audio.js') }}"></script>
	<script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-video.js') }}"></script>
	<script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-validate.js') }}"></script>
	<script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-ui.js') }}"></script>
	<script src="{{ asset('admin_assets/global/vendor/dropify/dropify.min.js') }}"></script>
@stop