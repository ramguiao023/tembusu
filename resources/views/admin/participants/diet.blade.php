@extends('admin.layouts.master')

@section('title', 'Participants')

@section('style')
	@include('admin.imports.datepicker-styles')
	@include('admin.imports.datatable-styles')

	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('content')
	<div class="float-left">
		<h3>Edit Participant: {{ $participant->first_name . " " . $participant->last_name }}</h3>
		<p>Single Participant</p>
	</div>
	
	<div class="float-right">
		<a href="{{ url('admin/participants') }}" class="btn btn-default" title="Add New User">
			<i class="icon glyphicon glyphicon-chevron-left"></i> Go Back
		</a>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-6">
	<div class="panel">
    	<div class="panel-body">
    		<h4>Complete Profile</h4>
    		<hr />
    		@if(session('complete_success'))
				<div class="alert alert-success alert-icon alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="icon wb-check" aria-hidden="true"></i>
                    <h4>Nice!</h4>
                    <p>
                    	{{ session('complete_success') }}
                    </p>
                </div>
			@endif
    		<form action="{{ url('/admin/participants/update-profile') }}" method="POST">
    			{{ csrf_field() }}
    			<input type="hidden" name="participant_id" value="{{ $participant->id }}" />
				@if(count($errors) > 0)
					<div class="alert alert-danger alert-icon alert-dismissible" role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                        <span aria-hidden="true">×</span>
	                    </button>
	                    <i class="icon wb-warning" aria-hidden="true"></i>
	                    <h4>Oops!</h4>
	                    <ul>
	                    	@foreach($errors->all() as $error)
			                    <li>{{ $error }}</li>
							@endforeach
	                    </ul>
	                </div>
				@endif

				<div class="form-group">
					<label>Diet Preferences</label>
					<div class="row">
						<div class="col-md-4">
							<label>
								<input type="checkbox" name="diet_preferences[]" {{ (in_array('Vegetarian', $diet_preferences)) ? "checked='checked'" : "" }} value='Vegetarian' />
								Vegetarian
							</label> <br />

							<label>
								<input type="checkbox" name="diet_preferences[]" {{ (in_array('Lactose Intolerance', $diet_preferences)) ? "checked='checked'" : "" }} value='Lactose Intolerance' />
								Lactose Intolerance
							</label> <br />
							<label>
								<input type="checkbox" name="diet_preferences[]" {{ (in_array('Diabetic', $diet_preferences)) ? "checked='checked'" : "" }} value='Diabetic' />
								Diabetic
							</label> <br />
						</div>
						<div class="col-md-4">
							<label>
								<input type="checkbox" name="diet_preferences[]" {{ (in_array('Peanut Allergies', $diet_preferences)) ? "checked='checked'" : "" }} value='Peanut Allergies' />
								Peanut Allergies
							</label> <br />
							<label>
								<input type="checkbox" name="diet_preferences[]" {{ (in_array('Halal', $diet_preferences)) ? "checked='checked'" : "" }} value='Halal' />
								Halal
							</label> <br />
						</div>
						<div class="col-md-4">
							
							<label>
								<input type="checkbox" name="diet_preferences[]" {{ (in_array('Gluten Free', $diet_preferences)) ? "checked='checked'" : "" }} value='Gluten Free' />
								Gluten Free
							</label> <br />

							<label>
								<input type="checkbox" name="diet_preferences[]" {{ (in_array('Kosher', $diet_preferences)) ? "checked='checked'" : "" }} value='Kosher' />
								Kosher
							</label> <br />
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Diet Notes</label>
					<textarea name="diet_notes" class="form-control">{{ $participant->diet_notes }}</textarea>	
				</div>
    			<div class="form-group">
    				<button type="submit" class="btn btn-primary float-right">
    					<i class="icon glyphicon glyphicon-check"></i>
    					Save
    				</button>
    			</div>
    		</form>
    	</div>
    </div>
</div>
	</div>
@stop


@section('script')
	@include('admin.imports.datepicker-scripts')
	<script src="{{ asset('admin_assets/global/vendor/alertify/alertify.js') }}"></script>
	<script src="{{ asset('admin_assets/global/vendor/notie/notie.js') }}"></script>

	<script src="{{ asset('admin_assets/global/js/Plugin/alertify.js') }}"></script>
	<script src="{{ asset('admin_assets/global/js/Plugin/notie-js.js') }}"></script>

	<script type="text/javascript">
		$('.date').datepicker({
            'format': 'yyyy-mm-dd',
            'autoclose': true
        });

		$(".delete-participant").on('click', function() {
		var self = $(this);

		alertify.confirm("Delete this participant?", function() {
				var id = self.data('id'),
					base_url = $("#baseurl").attr('content');
				window.location = base_url + "/admin/participants/" + id + "/delete";
			}, function() {
				alertify.error("Cancelled");
			});
		});
	</script>
@stop