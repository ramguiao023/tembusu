@extends('admin.layouts.master')

@section('title', 'Participants')

@section('style')
	@include('admin.imports.datatable-styles')

	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('content')
	<div class="float-left">
		<h2>Participants</h2>
	    <p>Manage Participants</p>
	</div>

    <a href="{{ url('admin/participants/create') }}" class="btn btn-success float-right" title="Add New User">
		<i class="icon glyphicon glyphicon-plus"></i> Add New Participant (Single Upload)
	</a>

	<a href="{{ url('admin/participants/bulk-upload') }}" class="btn btn-success float-right" style="margin-right: 10px;" title="Add New User">
		<i class="icon glyphicon glyphicon-plus"></i> Add New Participant (Bulk Upload)
	</a>

	<div class="clearfix"></div>

    <div class="panel">
    	<header class="panel-heading">
    	  <div class="panel-actions">
    	  	
    	  </div>
    	  <h3 class="panel-title"></h3>
    	</header>

    	<div class="panel-body">
			@if(session('success'))
				<div class="alert alert-success alert-icon alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="icon wb-check" aria-hidden="true"></i>
                    <h4>Nice!</h4>
                    <p>
                    	{{ session('success') }}
                    </p>
                </div>
			@endif

			@if(isset($_GET['delete']))
				<div class="alert alert-success alert-icon alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="icon wb-check" aria-hidden="true"></i>
                    <h4>Nice!</h4>
                    <p>
                    	Deleted successfully.
                    </p>
                </div>
			@endif

			@if(isset($_GET['sent']))
				<div class="alert alert-success alert-icon alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="icon wb-check" aria-hidden="true"></i>
                    <h4>Nice!</h4>
                    <p>
                    	Registration email was sent successfully.
                    </p>
                </div>
			@endif

			<form>
				
			<a href="javascript:void(0)" class="btn btn-danger delete" style="margin-right: 10px;" title="Remove Participant">
				<i class="icon glyphicon glyphicon-remove"></i>
			</a>
			<a href="javascript:void(0)" class="btn btn-primary send" title="Send Invitation">
				<i class="icon glyphicon glyphicon-send"></i>
			</a>

			<br />
			<br />

			<table class="table table-hover dataTable table-striped w-full" id="userTable">
			  <thead>
			    <tr>
					<th>#</th>
					<th>Employee Number</th>
					<th>Name</th>
					<th>Email</th>
					<th>Company</th>
					{{-- <th>Registration Status</th> --}}
					{{-- <th>Is Email Sent?</th> --}}
					<th>Action</th>
			    </tr>
			  </thead>
			  <tfoot>
			    <tr>
					<th>#</th>
					<th>Employee Number</th>
					<th>Name</th>
					<th>Email</th>
					<th>Company</th>
					{{-- <th>Registration Status</th> --}}
					{{-- <th>Is Email Sent?</th> --}}
					<th>Action</th>
			  	</tr>
			  </tfoot>
			  <tbody>
			  	@foreach($participants as $participant)
			  	<tr>
			  		<td>
			  			<input type="checkbox" name="participant_id" value="{{ $participant->id }}" />
			  		</td>
			  		<td>{{ $participant->employee_number }}</td>
			  		<td>{{ $participant->salutation . " " . @$participant->name }}</td>
			  		<td>{{ $participant->email }}</td>
			  		<td>
			  			{{ @$participant->company->name }} 
			  			{{ (@$participant->department) ? " - ".@$participant->department : "" }} <br />
			  		</td>
			  		{{-- <td>
			  			{!!
			  				($participant->status == 1)
			  				? "<span class='badge badge-success'>Completed</span>"
			  				: "<span class='badge badge-danger'>Not yet completed</span>"
			  			!!}
			  		</td>
			  		<td>
			  			{!!
			  				($participant->is_email_sent == 1)
			  				? "<span class='badge badge-success'>Sent</span>"
			  				: "<span class='badge badge-danger'>Not Sent</span>"
			  			!!}
			  		</td> --}}
			  		<td>
			  			<a href="{{ url('admin/participants/'.$participant->id.'/edit') }}" class="btn btn-primary">
			  				<i class="icon glyphicon glyphicon-edit"></i>
			  			</a>
			  			<a href="{{ url('admin/participants/'.$participant->id.'/diet') }}" class="btn btn-success">
			  				<i class="glyphicon glyphicon-cutlery"></i>
			  			</a>
			  		</td>
			  	</tr>
			  	@endforeach
			  </tbody>
			</table>

			<a href="javascript:void(0)" class="btn btn-danger delete" style="margin-right: 10px;" title="Remove Participant">
				<i class="icon glyphicon glyphicon-remove"></i>
			</a>
			<a href="javascript:void(0)" class="btn btn-primary send" title="Send Invitation">
				<i class="icon glyphicon glyphicon-send"></i>
			</a>

		</div>
    </div>
@stop

@section('script')

<script type="text/javascript" src="{{ asset('admin_assets/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin_assets/global/js/Plugin/datatables.js') }}"></script>

@include('admin.imports.datatable-scripts')

<script src="{{ asset('admin_assets/global/vendor/alertify/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/notie/notie.js') }}"></script>

<script src="{{ asset('admin_assets/global/js/Plugin/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/js/Plugin/notie-js.js') }}"></script>

<!-- <script src="{{ asset('admin_assets/assets/examples/js/tables/datatable.js') }}"></script> -->
<script type="text/javascript">
	const base_url = $("#baseurl").attr('content'),
		  token	   = $("#token").attr('content');

	$("#userTable").DataTable();
	
	$(".delete").on('click', function() {
		var self = $(this);

		alertify.confirm("Delete the selected participant/s?", function() {
			const participant_id = [];

			$.each($("input[name='participant_id']:checked"), function(){            
                var this_checkbox = $(this);

                participant_id.push(this_checkbox.val());
            });

            $.post(base_url + '/admin/participants/delete-many', {
			            	"_token" : token,
			            	"participant_id": participant_id
			            }, function(data) {
			            	window.location = base_url + "/admin/participants?delete=true"
			            });

		}, function() {
			alertify.error("Cancelled");
		});
	});

	$(".send").on('click', function() {
		var self = $(this);

		alertify.confirm("Send registration email to the selected participant/s?", function() {
			const participant_id = [];

			$.each($("input[name='participant_id']:checked"), function(){            
                var this_checkbox = $(this);

                participant_id.push(this_checkbox.val());
            });

            $.post(base_url + '/admin/participants/send-to-many', {
			            	"_token" : token,
			            	"participant_id": participant_id
			            }, function(data) {
			            	window.location = base_url + "/admin/participants?sent=true"
			            });

		}, function() {
			alertify.error("Cancelled");
		});
	});
</script>
@stop