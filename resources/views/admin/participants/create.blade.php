@extends('admin.layouts.master')

@section('title', 'Participants')

@section('style')
	@include('admin.imports.datepicker-styles')

	@include('admin.imports.datatable-styles')

	<link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">
@stop

@section('content')
	<h2>Add Participant for {{ $event->title }}</h2>
	<p>Single Participant</p>

	<div class="row">
		<div class="col-md-6">
			<div class="panel">
		    	<header class="panel-heading">
		    	  <div class="panel-actions">
		    	  	<a href="{{ url('admin/participants') }}" class="btn btn-default" title="Add New User">
						<i class="icon glyphicon glyphicon-chevron-left"></i> Go Back
					</a>
		    	  </div>
		    	  <h5 class="panel-title" style="visibility: hidden">Fill-out all the Necessary Fields</h5>
		    	</header>

		    	<div class="panel-body">
		    		<form method="POST" action="{{ url('/admin/participants') }}" autocomplete="off">
		    			<input type="hidden" name="event_id" value="{{ $event->id }}" />
		    			@if(count($errors) > 0)
							<div class="alert alert-danger alert-icon alert-dismissible" role="alert">
			                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                        <span aria-hidden="true">×</span>
			                    </button>
			                    <i class="icon wb-warning" aria-hidden="true"></i>
			                    <h4>Oops!</h4>
			                    <ul>
			                    	@foreach($errors->all() as $error)
					                    <li>{{ $error }}</li>
									@endforeach
			                    </ul>
			                </div>
						@endif

		    			<h4>Basic Information</h4>
		    			<hr />
		    			{{ csrf_field() }}


		    			<div class="form-group">
		    				<label>Employee Number <span class="required">*</span></label>
		    				<input type="text" name="employee_number" value="{{ old('employee_number') }}" class="form-control" />
		    			</div>
		    			
		    			<div class="form-group">
		    				<div class="row">
		    					<div class="col-md-6">
		    						<div class="form-group">
					    				<label>Salutation</label>
					    				<select name="salutation" class="form-control">
					    					<option value="">Salutation</option>
					    					<option {{ (old('salutation') == "Mr.") ? "selected='selected'" : "" }} value="Mr.">Mr.</option>
					    					<option {{ (old('salutation') == "Ms.") ? "selected='selected'" : "" }} value="Ms.">Ms.</option>
					    					<option {{ (old('salutation') == "Mrs.") ? "selected='selected'" : "" }} value="Mrs.">Mrs.</option>
					    					<option {{ (old('salutation') == "Dr.") ? "selected='selected'" : "" }} value="Dr.">Dr.</option>
					    					<option {{ (old('salutation') == "Engr.") ? "selected='selected'" : "" }} value="Engr.">Engr.</option>
					    					<option {{ (old('salutation') == "Atty.") ? "selected='selected'" : "" }} value="Atty.">Atty.</option>
					    					<option {{ (old('salutation') == "Dean") ? "selected='selected'" : "" }} value="Dean">Dean</option>
					    					<option {{ (old('salutation') == "Prof.") ? "selected='selected'" : "" }} value="Prof.">Prof.</option>
					    				</select>
					    			</div>
		    					</div>
		    					<div class="col-md-6">
		    						<label>Full Name <span class="required">*</span></label>
		    						<input type="text" name="name" value="{{ old('name') }}" class="form-control" />
		    					</div>
		    				</div>
		    			</div>
		    			<div class="form-group" style="display: none;">
		    				<label>Company <span class="required">*</span></label>
		    				<select disabled name="company_id" class="form-control">
		    					<option value="">Company</option>
		    					@foreach($companies as $company)
		    						<option value="{{ $company->id }}" {{ ($event->company_id == $company->id) ? "selected='selected'" : "" }}>{{ $company->name }}</option>
		    					@endforeach
		    				</select>
		    				<input type="hidden" name="company_id" value="{{ $event->company_id }}" />
		    			</div>
		    			<div class="form-group">
		    				<label>Birthdate <span class="required">*</span></label>
		    				<input type="text" name="birthdate" value="{{ old('birthdate') }}" class="date form-control" placeholder="YYYY-MM-DD">
		    			</div>
		    			<div class="form-group">
		    				<label>Email <span class="required">*</span></label>
		    				<input type="text" name="email" value="{{ old('email') }}" class="form-control" />
		    			</div>
		    			
		    			<div class="form-group">
                            <label>
                                Allow participants to choose diet preferences?
                            </label>
                            <br />
                            <label>
                                <input type="radio" name="show_diet_preferences" value="1" {{ (old('show_diet_preferences') == 1) ? "checked='checked'" : "" }} /> Yes
                            </label>

                            <label>
                                <input type="radio" name="show_diet_preferences" value="0" {{ (old('show_diet_preferences') == 0) ? "checked='checked'" : "" }} /> No
                            </label>
                        </div>
                       
                       	<div class="form-group">
                       		<label>Table Number</label>
                       		<input type="text" name="table_number" value="{{ old('table_number') }}" class="form-control" />
                       	</div>

                       	<div class="form-group">
                       		<label>Lucky Draw Number <br /><small>Leave this blank so the system will generate a unique <strong>6 digit code</strong></small></label>
                       		<input type="text" name="raffle_code" value="{{ old('raffle_code') }}" placeholder="xxxxxx" class="form-control" />
                       	</div>

		    			<div class="form-group" style="display: none;">
		    				<label><input type="checkbox" name="is_email_sent" value="1" {{ (old('is_email_sent')) ? "checked='checked'" : "" }} /> Send email to participant to complete registration?</label>
		    			</div>
		    			
		    			<hr />

		    			<div class="form-group">
		    				<button type="submit" class="btn btn-primary">
		    					<i class="icon glyphicon glyphicon-check"></i>
		    					Save
		    				</button>
		    			</div>
		    		</form>
		    	</div>
		    </div>
		</div>
	</div>
@stop


@section('script')
	@include('admin.imports.datepicker-scripts')
	<script>
		$('.date').datepicker({
            'format': 'yyyy-mm-dd',
            'autoclose': true
        });
	</script>
@stop