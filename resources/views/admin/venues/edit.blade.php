@extends('admin.layouts.master')

@section('title', 'Edit Venue')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/alertify/alertify.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/notie/notie.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/examples/css/advanced/alertify.css') }}">

    <style type="text/css">
        /* Always set the map height explicitly to define the size of the div
        * element that contains the map. */
        #map {
            height: 300px;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #description {
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
        }

        #infowindow-content .title {
            font-weight: bold;
        }

        #infowindow-content {
            display: none;
        }

        #map #infowindow-content {
            display: inline;
        }

        .pac-card {
            margin: 10px 10px 0 0;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            background-color: #fff;
            font-family: Roboto;
        }

        #pac-container {
            padding-bottom: 12px;
            margin-right: 12px;
        }

        .pac-controls {
            display: inline-block;
            padding: 5px 11px;
        }

        .pac-controls label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 400px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        #title {
            color: #fff;
            background-color: #4d90fe;
            font-size: 15px;
            font-weight: 500;
            padding: 6px 12px;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/vendor/dropify/dropify.css') }}">s
@stop

@section('content')
    <div class="float-left">
        <h2>Edit Venue</h2>
        <p>Update Venue Information</p>
    </div>

    <a href="{{ url('admin/venues') }}" class="btn btn-default float-right" title="Add New User">
        <i class="icon glyphicon glyphicon-chevron-left"></i>
        Go Back
    </a>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-10">
            <div class="panel">
                <div class="panel-body">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger alert-icon alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <i class="icon wb-warning" aria-hidden="true"></i>
                            <h4>Uh oh!</h4>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form id='form' action="{{ url('admin/venues/'.$venue->id) }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @method("PUT")
                        <div class="row">
                            <div class="col-md-6">
                                <h3>{{ $venue->name }}</h3>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group text-right">
                                    <button type="submit" name="create" class="btn btn-primary">
                                        <i class="icon glyphicon glyphicon-floppy-save"></i>
                                        Update Venue
                                    </button>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Venue <span class="required">*</span></label>
                                    <input type="text" required="" name="name" value="{{ old('name', $venue->name) }}" placeholder="What is the name of the venue?" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Address <span class="required">*</span></label>
                            <input type="text" required="" name="address" placeholder="Where is the venue located? (Please indicate full address)" value="{{ old('address', $venue->address) }}" class="form-control">
                            <input type="hidden" name="lng" value="{{ old('lng', $venue->lng) }}" id="long" />
                            <input type="hidden" name="lat" value="{{ old('lat', $venue->lat) }}" id="lat">
                        </div>
                        <div class="form-group">
                             <div class="pac-card" id="pac-card">
                                   <div>
                                     <div id="title">
                                       Search a Location
                                     </div>
                                     <div id="type-selector" class="pac-controls">
                                       <input type="radio" name="type" id="changetype-all" checked="checked">
                                       <label for="changetype-all">All</label>

                                       <input type="radio" name="type" id="changetype-establishment">
                                       <label for="changetype-establishment">Establishments</label>

                                       <input type="radio" name="type" id="changetype-address">
                                       <label for="changetype-address">Addresses</label>

                                       <input type="radio" name="type" id="changetype-geocode">
                                       <label for="changetype-geocode">Geocodes</label>
                                     </div>
                                     <div id="strict-bounds-selector" class="pac-controls">
                                       <input type="checkbox" id="use-strict-bounds" value="">
                                       <label for="use-strict-bounds">Strict Bounds</label>
                                     </div>
                                   </div>
                                   <div id="pac-container">
                                    <label class="block pl-10">Long & Lat <span class="required">*</span></label>

                                    <input id="pac-input" type="text" 
                                         placeholder="Enter a location" value="">
                                   </div>
                            </div>
                            <div id="map"></div>
                            <div id="infowindow-content">
                                <img src="" width="16" height="16" id="place-icon">
                                <span id="place-name"  class="title"></span><br>
                                <span id="place-address"></span>
                            </div>
                        </div>
                         
                        <h4>Other Information</h4>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Traffic Guide</label>
                                        <textarea class="form-control" name="traffic_guide" placeholder="How's the traffic in the area?">{{ old('traffic_guide', $venue->traffic_guide) }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label>Carpark Guide</label>
                                        <textarea class="form-control" name="carpark_guide" placeholder="Where participants can park in the venue?">{{ old('carpark_guide', $venue->carpark_guide) }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Hall/Ballroom/Name/Number</label>
                                        <input type="text" class="form-control" value="{{ old('building_info', $venue->building_info) }}" placeholder="Specify more information about the venue Ex. Hall Name, Room Number, etc..." name="building_info" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4>Images</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label>Primary Image <span class="required">*</span></label>
                                        <input type="file" data-show-remove="false" id="input-file-now" name="primary_image" data-plugin="dropify" data-default-file="{{ asset('uploads/venues/'.$venue->primary_image) }}" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label>Street Map Image</label>
                                        <input type="file" data-show-remove="false" id="input-file-now" name="street_map_image" data-plugin="dropify" data-default-file="{{ ($venue->street_map_image) ? asset('uploads/venue_street/'.$venue->street_map_image) : '' }}" />

                                        @if($venue->indoor_map_image)
                                            Remove Street Map Image?
                                            <input type="checkbox" name="remove_street_image" /> Yes
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label>Indoor Map Image</label>
                                        <input type="file" data-show-remove="false" id="input-file-now" name="indoor_map_image" data-plugin="dropify" data-default-file="{{ ($venue->indoor_map_image) ? asset('uploads/venue_indoor/'.$venue->indoor_map_image) : '' }}" />

                                        @if($venue->indoor_map_image)
                                            Remove Indoor Map Image?
                                            <input type="checkbox" name="remove_indoor_image" /> Yes
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <a href="javascript:void(0)" data-id="{{ $venue->id }}" class="btn btn-default float-left delete-venue">
                                <i class="icon glyphicon glyphicon-remove"></i>
                                Delete Venue
                            </a>
                            <button type="submit" name="create" class="btn btn-primary float-right">
                                <i class="icon glyphicon glyphicon-floppy-save"></i>
                                Update Venue
                            </button>

                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
<script src="{{ asset('admin_assets/global/vendor/alertify/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/vendor/notie/notie.js') }}"></script>

<script src="{{ asset('admin_assets/global/js/Plugin/alertify.js') }}"></script>
<script src="{{ asset('admin_assets/global/js/Plugin/notie-js.js') }}"></script>

<script>
    $(".delete-venue").on('click', function() {
        var self = $(this);

        alertify.confirm("Delete this venue?", function() {
            var venue_id = self.data('id'),
                base_url = $("#baseurl").attr('content');
            window.location = base_url + "/admin/venues/" + venue_id + "/delete";
        }, function() {
            alertify.error("Cancelled");
        });
    });
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
      // var longVal = parseFloat($("#long").val()),
      //     latVal = parseFloat($("#lat").val());

      // var longVal = parseFloat($("#long").val()),
      //     latVal = parseFloat($("#lat").val());

      var longVal = <?= ($venue->lng) ? $venue->lng : 14.5777691 ?>,
          latVal = <?= ($venue->lat) ? $venue->lat : 121.0725455 ?>;


      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: latVal, lng: longVal},
            zoom: 13
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);

          $("#long").val(autocomplete.getPlace().geometry.location.lng());
          $("#lat").val(autocomplete.getPlace().geometry.location.lat());
            
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
          });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);

        document.getElementById('use-strict-bounds')
            .addEventListener('click', function() {
              console.log('Checkbox clicked! New state=' + this.checked);
              autocomplete.setOptions({strictBounds: this.checked});
            });
      }

    $("#form").on('keypress', function (e) {
        if (e.which == 13) {
            e.preventDefault();
        }
    });

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAANZ4AD4WX1TyAE8RKKOFQ8CD69eQ0PMY&libraries=places&callback=initMap"
        async defer></script>

    <!-- FILE UPLOAD PLUGIN -->
    <script src="{{ asset('admin_assets/global/vendor/jquery-ui/jquery-ui.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-tmpl/tmpl.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-load-image/load-image.all.min.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-process.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-image.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-audio.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-video.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-validate.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/blueimp-file-upload/jquery.fileupload-ui.js') }}"></script>
    <script src="{{ asset('admin_assets/global/vendor/dropify/dropify.min.js') }}"></script>
@stop