@extends('admin.layouts.master')

@section('title', 'Venues')

@section('content')
    <div class="float-left">
        <h2>Venues</h2>
        <p>Manage event venues</p>
    </div>

    <a href="{{ url('admin/venues/create') }}" class="btn btn-success float-right" title="Add New User">
        <i class="icon glyphicon glyphicon-plus"></i> Add New Venue
    </a>

    <div class="clearfix"></div>

    <div class="panel">
    	<header class="panel-heading">
    	  <div class="panel-actions">
    	  	
    	  </div>   
    	  <h3 class="panel-title"></h3>
    	</header>

        <div class="panel-body">
            @if(session('success'))
                <div class="alert alert-success alert-icon alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="icon wb-check" aria-hidden="true"></i>
                    <h4>Nice!</h4>
                    <p>
                        {{ session('success') }}
                    </p>
                </div>
            @endif

            <table class="table table-hover dataTable table-striped w-full" id="userTable">
                <thead>
                    <tr>
                        <th>Venue</th>
                        <th>Address</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Venue</th>
                        <th>Address</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($venues as $venue)
                    <tr>
                        <td>{{ $venue->name }}</td>
                        <td>{{ $venue->address }}</td>
                        <td>
                            <a href="{{ url('admin/venues/'.$venue->id) }}" class="btn btn-primary">
                                <i class="glyphicon glyphicon-edit"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
          </table>
        </div>
    </div>
@stop