<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Lucky Draw</title>

	<link rel="stylesheet" href="{{ asset('admin_assets/assets/js/odometer/themes/odometer-theme-default.css') }}" />
	<link rel="stylesheet" href="{{ asset('admin_assets/global/fonts/font-awesome/font-awesome.css?v4.0.2') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/global/css/bootstrap.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,600&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('admin_assets/favicon.png') }}">
	<link rel="stylesheet" href="{{ asset('admin_assets/assets/css/lucky-draw.css') }}" />
	<meta id="baseurl" content="{{ url('/') }}">
	<meta id="token" content="{{ csrf_token() }}">
</head>
<body style="@yield('style')">
	
	<div class="container-fluid" style="padding: 0 !important;">
		@yield('content')
	</div>
	
	<script src="{{ asset('admin_assets/assets/js/odometer/odometer.min.js') }}"></script>

	<script
	  src="https://code.jquery.com/jquery-2.2.4.min.js"
	  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
	  crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	@yield('script')
</body>
</html>