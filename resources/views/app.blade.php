<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Tembusu</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel='icon' href='/admin_assets/favicon.png' type='image/x-icon' />   

        <link rel="stylesheet" type="text/css" href="/css/app.css">
        <link rel="stylesheet" type="text/css" href="/css/custom.css">
        <style lang="scss">
          @import "../node_modules/ag-grid-community/dist/styles/ag-grid.css";
          @import "../node_modules/ag-grid-community/dist/styles/ag-theme-balham.css";
        </style>
    </head>
    <body class="bg-teal">
        <div id="app">
            <router-view></router-view>
        </div>

        <script type="text/javascript" src="/js/app.js"></script>
    </body>
</html>
