import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
import store from './store/index';

Vue.use(VueRouter);

// Vue.mixin({
// 	beforeCreate() {
// 		var self = this;
		
// 		if (localStorage.getItem("user") === null || localStorage.getItem("token") === null) {
// 			self.$router.push('/');
// 		}
// 	}
// });


let app = new Vue({
    el: '#app',
    store,
    router: new VueRouter(routes)
});
