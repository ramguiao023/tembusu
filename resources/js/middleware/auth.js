export default function auth({ next, router }) {
	if (localStorage.getItem("user") === null || localStorage.getItem("token") === null) {
		return router.push('/');
	}

	return next;
}