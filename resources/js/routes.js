import Login from './components/login/Login';
import FirstLogin from './components/login/FirstLogin';
import Verification from './components/login/Verification';

// EMPLOYEE ROUTES
import EmployeeProfile from './components/employee/EmployeeProfile';
// EOF EMPLOYEE ROUTES

// EVENTS ROUTES
import Events from './components/events/Events';
import InvitedEvents from './components/events/InvitedEvents';
import LoginViaId from './components/events/LoginViaId';
import SingleInvitedEvent from './components/events/SingleInvitedEvent';
// EOF EVENTS ROUTES

import Company404 from './components/company/Company404';

// import NotFound from './components/NotFound';

const authGuard = (to, from, next) => {
	var self = this;
	if (localStorage.getItem("user") === null || localStorage.getItem("token") === null) {
		window.location = "/";
	}

	next();
}

export default {
	mode: 'history',

	routes: [
		// {
		// 	path: '*',
		// 	component: NotFound
		// },
		
		/*
			LOGIN ROUTES
		*/
		{
			path: '/corp/:eventid/:company/',
			component: Login
		},
		{
			path: '/corp/:company/:employee/first-login',
			component: FirstLogin,
			name: 'firstlogin',
			beforeEnter: authGuard
		},
		{
			path: '/corp/:company/:event/verification',
			component: Verification,
			name: 'verification',
		},
		{
			path: '/corp/:company/:participantid/:eventid/login-via-id',
			component: LoginViaId,
			name: 'loginviaid',
		},

		/*
			
			EVENTS ROUTES

		 */
		{
			path: '/corp/:company/:employee/events',
			component: Events,
			name: 'events',
			beforeEnter: authGuard
		},
		{
			path: '/corp/:company/:employee/invited',
			component: InvitedEvents,
			name: 'invitedevents',
			beforeEnter: authGuard
		},
		{
			path: '/corp/:company/event/:id/:participantid',
			component: SingleInvitedEvent,
			name: 'singleinvitedevent'
		},
		/*
			
			EOF EVENTS ROUTES

		 */

		
		/*
			EOF LOGIN ROUTES
		*/
		{
			path: '/corp/:company/:employee',
			component: EmployeeProfile,
			name: "index",
			beforeEnter: authGuard
		},
		

		{
			path: '/company404/',
			component: Company404
		}
	]
}



