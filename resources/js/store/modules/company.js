import axios from 'axios';

const state = {
	user: {},
	url: process.env.MIX_APP_URL,
	api_url: process.env.MIX_API_URL,
};

const getters = {
	
};

const actions = {
	async getCompanyViaSlug({commit}, slug)
	{
		const response = await axios.get(state.api_url + "/companies/" + slug, 
						{
							headers: {
								"Accept": "application/json",
								"Authorization": localStorage.getItem('token')
							}
						}
	 				);

		return response;
	},

	async getCompanyById({commit}, id)
	{
		const response = await axios.get(state.api_url + "/companies/by-id/" + id);

		return response;
	}
};

const mutations = {
	
};

export default {
	state,
	getters,
	actions,
	mutations
}