import axios from 'axios';

const state = {
	event: {},
	url: process.env.MIX_APP_URL,
	api_url: process.env.MIX_API_URL,
};

const getters = {
	
};

const actions = {
	async getEventById({commit}, id)
	{
		const response = await axios.get(state.api_url + "/events/" + id);

		return response;
	},
	async joinEvent({commit}, request)
	{
		const response = await axios.post(state.api_url + "/events",
									request
							   );

		return response;
	},
	async declineEvent({commit}, request)
	{
		const response = await axios.post(state.api_url + "/events/decline-event",
									request
							   );

		return response;
	},
	async answerQuestion({commit}, request)
	{
		const response = await axios.post(state.api_url + "/events/answer", request);

		return response;
	}
};

const mutations = {
	
};

export default {
	state,
	getters,
	actions,
	mutations
}