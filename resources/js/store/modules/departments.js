import axios from 'axios';

const state = {
	department: {},
	url: process.env.MIX_APP_URL,
	api_url: process.env.MIX_API_URL,
	api_key: process.env.MIX_API_KEY,
	token: localStorage.getItem("token")
};

const getters = {
	
};

const actions = {
	async getDepartmentsOfACompany({commit}, company_id)
	{
		const response = await axios.get(state.api_url + "/departments/get-all-departments-of-a-company/" + company_id, {
									headers: {
										"Accept": "application/json",
										"Authorization": localStorage.getItem("token")
									}
								});

		return response;
	}
};

const mutations = {
	
};

export default {
	state,
	getters,
	actions,
	mutations
}