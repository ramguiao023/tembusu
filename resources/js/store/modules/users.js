import axios from 'axios';

const state = {
	user: {},
	url: process.env.MIX_APP_URL,
	api_url: process.env.MIX_API_URL,
	api_key: process.env.MIX_API_KEY,
	token: localStorage.getItem("token")
};

const getters = {
	
};

const actions = {
	async getLoggedInUser()
	{
		const response = await axios.get(state.api_url + "/participants/get-loggedin-participant",
						{
							headers: {
								"Accept": "application/json",
								"Authorization": localStorage.getItem("token")
							}
		 				});

	 	return response;
	},

	async changePassword({commit}, request)
	{
		const response = await axios.post(state.api_url + "/participants/change-password",
									request,
									{
										headers: {
											"Accept": "application/json",
											"Authorization": localStorage.getItem("token")
										}
					 				});

		return response;
	},
	async logout({ commit }) {
		const response = await axios.post(state.api_url + '/participants/logout',{},{
								headers: {
											"Accept": "application/json",
											"Authorization": localStorage.getItem("token")
										}
								});

		return response;
	},
	async updateParticipant({commit}, request)
	{
		const response = await axios.put(state.api_url + "/participants/" + request.id, request,
									{
										headers: {
											"Accept": "application/json",
											"Authorization": localStorage.getItem("token")
										}
									});

		return response;
	},
	async updateProfilePhoto({commit}, request)
	{
		const response = await axios.post(state.api_url + "/participants/update-profile-photo", request,
									{
										headers: {
											"Accept": "application/json",
											"Authorization": localStorage.getItem("token")
										}
									});

		return response;
	},
	async getParticipantByEmployeeNumber({commit}, employee_number)
	{
		const response = await axios.get(state.api_url + "/participants/via-employee-number/" + employee_number);
	
		return response;
	},
	async getParticipantById({commit}, data)
	{
		const response = await axios.get(state.api_url + "/participants/via-id/" + data[0] + "/" + data[1]);
	
		return response;
	}
};

const mutations = {
	
};

export default {
	state,
	getters,
	actions,
	mutations
}