import axios from 'axios';

const state = {
	user: {},
	url: process.env.MIX_APP_URL,
	api_url: process.env.MIX_API_URL,
	api_key: process.env.MIX_API_KEY,
	token: ""
};

const getters = {
	
};

const actions = {
	async auth({commit}, request)
	{
		const data = {
			grant_type: "password",
			client_secret: state.api_key,
			client_id: 2,
			...request
		}

		const response = await axios.post(state.url + "/oauth/token", data)
			 .then(function(response_data) {
			 	localStorage.setItem("token", "Bearer "+response_data.data.access_token);

			 	axios.get(state.api_url + "/participants/get-loggedin-participant",
					{
						headers: {
							"Accept": "application/json",
							"Authorization": localStorage.getItem("token")
						}
	 				})
	 				.then(function(loggedin_data) {
		 				axios.get(state.api_url + "/participants/get-current-company/"+loggedin_data.data.data.id,
							{
								headers: {
									"Accept": "application/json",
									"Authorization": localStorage.getItem("token")
								}
			 				})
		 					.then(function(current_company_data) {
		 						let locationLastPart = window.location.pathname
		 						if (locationLastPart.substring(locationLastPart.length-1) == "/") {
		 						  locationLastPart = locationLastPart.substring(0, locationLastPart.length-1);
		 						}
		 						locationLastPart = locationLastPart.substr(locationLastPart.lastIndexOf('/') + 1);

		 						if(typeof current_company_data.data.data.company.slug != 'undefined') {
			 						if(current_company_data.data.data.company.slug == locationLastPart) {
										localStorage.setItem('user', JSON.stringify(loggedin_data.data.data))
			 						} else {
										localStorage.setItem('user', JSON.stringify([]))
			 						}
		 						} else {
									localStorage.setItem('user', JSON.stringify([]));
		 						}
		 					});
	 				});
			 });

		// console.log(response.data.data);
		return response;
	},

	async verify({commit}, request)
	{
		const response = await axios.post(state.api_url + "/participants/verify", request);

		return response;
	}
};

const mutations = {
	
};

export default {
	state,
	getters,
	actions,
	mutations
}