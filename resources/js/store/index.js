import Vuex from 'vuex';
import Vue from 'vue';
import login from './modules/login';
import users from './modules/users';
import company from './modules/company';
import departments from './modules/departments';
import event from './modules/event';

// load Vuex

Vue.use(Vuex);

// Create store
export default new Vuex.Store({
	modules: {
		login,
		users,
		company,
		departments,
		event,
	}
});