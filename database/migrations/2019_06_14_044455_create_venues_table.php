<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')
                  ->nullable();
            $table->string('address')
                  ->nullable();
            $table->string('lng')
                  ->nullable();
            $table->string('lat')
                  ->nullable();
            $table->string('building_info')
                  ->nullable();
            $table->string('traffic_guide')
                  ->nullable();
            $table->string('carpark_guide')
                  ->nullable();
            $table->string('primary_image')
                  ->nullable();
            $table->string('street_map_image')
                  ->nullable();
            $table->string('indoor_map_image')
                  ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venues');
    }
}
