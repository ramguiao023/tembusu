<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('caption')
                  ->nullable();
            $table->string('write_up')
                  ->nullable();
            $table->string('banner_image')
                  ->nullable();
            $table->string('wallpaper')
                  ->nullable();
            $table->bigInteger('company_id')
                  ->nullable();
            $table->bigInteger('created_by')
                  ->nullable();
            $table->date('start_date')
                  ->nullable();
            $table->date('end_date')
                  ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
