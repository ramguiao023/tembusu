<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('salutation')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->bigInteger('company_id')->nullable();
            $table->string('employee_number')
                  ->unique()
                  ->nullable();
            $table->bigInteger('department_id')->nullable();
            $table->string('email')
                  ->unique()
                  ->nullable();
            $table->string('contact_number')->nullable();
            $table->string('username')
                  ->unique()
                  ->nullable();
            $table->string('password')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('diet_preferences')->nullable();
            $table->string('qrid')->nullable();
            $table->tinyInteger('status')
                  ->default(0)
                  ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
