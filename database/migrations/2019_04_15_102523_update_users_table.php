<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('users', function(Blueprint $table) {
        //     $table->renameColumn('name', 'first_name');
        // });

        Schema::table('users', function(Blueprint $table) {
            $table->string('last_name')->after('first_name');

        });

        Schema::table('users', function(Blueprint $table) {
            $table->string('profile_photo')
                            ->nullable()
                            ->after('last_name');
        });

        Schema::table('users', function(Blueprint $table) {
            $table->tinyInteger('status')
                            ->default(1)
                            ->after('email');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
