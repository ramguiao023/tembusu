<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateParticipantsTableSoCanAdjustForBulkUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('participants', function (Blueprint $table) {
            $table->string('is_vip')
                  ->after('show_diet_preferences')
                  ->nullable();

            $table->string('department_id')->change();
        });

        Schema::table('participants', function (Blueprint $table) {
            $table->renameColumn('department_id', 'department');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('participants', function (Blueprint $table) {
            $table->dropColumn('is_vip');

            $table->bigInteger('department')->change();

            $table->renameColumn('department', 'department_id');
        });
    }
}
