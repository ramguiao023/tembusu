<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveContactDetailsFieldInEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('contact_person');
            $table->dropColumn('contact_person_email');
            $table->dropColumn('contact_person_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->string('contact_person')
                  ->nullable()
                  ->after('event_type');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->string('contact_person_email')
                  ->nullable()
                  ->after('contact_person');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->string('contact_person_number')
                  ->nullable()
                  ->after('contact_person_email');
        });
    }
}
