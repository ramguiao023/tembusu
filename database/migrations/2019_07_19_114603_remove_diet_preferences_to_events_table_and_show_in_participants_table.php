<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveDietPreferencesToEventsTableAndShowInParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('show_diet_preferences');
        });

        Schema::table('participants', function (Blueprint $table) {
            $table->tinyInteger('show_diet_preferences')
                  ->after('qrid')
                  ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->tinyInteger('show_diet_preferences')
                  ->after('end_time')
                  ->nullable();
        });

        Schema::table('participants', function (Blueprint $table) {
            $table->dropColumn('show_diet_preferences');
        });
    }
}
