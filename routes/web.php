<?php

Route::get("a", function() {
	echo bcrypt("12312312");
});

Route::get('event-checker/{event_id}/{participant_id}', 'Admin\EventsController@eventChecker');

Route::prefix('admin')
    ->group(base_path('routes/admin.php'));

Route::prefix('/')
    ->group(base_path('routes/client.php'));

