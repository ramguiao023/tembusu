<?php
Route::get('companies/by-id/{id}', 'Api\CompaniesController@showById');
Route::get('companies/{slug}', 'Api\CompaniesController@show');
Route::post('events/decline-event', 'Api\EventsController@declineEvent');
Route::post('participants/verify', 'Api\ParticipantsController@verify');

Route::post('events/answer', 'Api\EventsController@answerEvent');
Route::resource('events', 'Api\EventsController');

Route::get('participants/via-employee-number/{employee_number}', 'Api\ParticipantsController@showViaEmployeeNumber');

Route::get('participants/via-id/{id}/{event_id}', 'Api\ParticipantsController@show');

Route::get('login-via-id/{id}', 'Api\LoginController@login');

Route::middleware('auth:api')->group(function() {
	// USERS
	Route::get('users/get-loggedin-user', 'UsersController@getLoggedInUser');
	Route::post('users/update-profile-picture', 'UsersController@updateProfilePicture');
	// EOF USERS



	// PARTICIPANTS
	Route::get('participants/get-current-company/{id}', 'Api\ParticipantsController@getCurrentCompany');
	Route::get('participants/get-loggedin-participant', 'Api\ParticipantsController@getLoggedInParticipantData');
	Route::post('participants/logout', 'Api\ParticipantsController@logout');
	Route::post('participants/change-password', 'Api\ParticipantsController@changePassword');
	Route::post('participants/update-profile-photo', 'Api\ParticipantsController@updateProfilePhoto');
	Route::resource('participants', 'Api\ParticipantsController');
	// EOF PARTICIPANTS



	// COMPANIES
	Route::resource('companies', 'Api\CompaniesController');
	// EOF COMPANIES

	// COMPANIES
	Route::get('departments/get-all-departments-of-a-company/{company_id}', 'Api\DepartmentsController@getDepartmentsOfACompany');
	Route::resource('departments', 'Api\DepartmentsController');
	// EOF COMPANIES
});

// use App\User;
// Route::get('test', function() {
// 	$user = new User;

// 	$user->first_name = "Ram";
// 	$user->last_name = "Guiao";
// 	$user->profile_photo = "photo.png";
// 	$user->role = 1;
// 	$user->status = 1;
// 	$user->email = "ram@guiao.com";
// 	$user->password = bcrypt('12312312');

// 	$user->save();
// });