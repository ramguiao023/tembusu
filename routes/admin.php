<?php

Route::get('/', 'Admin\LoginController@index')->name('login');
Route::get('/qr-code/{id}', 'Admin\ParticipantsController@getQR');
Route::post('/login', 'Admin\LoginController@login');

// use App\Mail\SendReminder;
// Route::get('try', function() {
//         Mail::to("ramheelpguiao@gmail.com")->send(new SendReminder());
	
// });

Route::get('try', function() {
	echo bcrypt("12312312");
});

Route::middleware(['auth'])->group(function () {
	/*
	 * LOGIN
	 */
	Route::get('/logout', 'Admin\LoginController@logout');
	/*
	 * EOF LOGIN
	 */

	/*
	 * DASHBOARD
	 */
	Route::get('/dashboard', 'Admin\DashboardController@index');

	/*
	 * EOF DASHBOARD
	 */

	/*
	 * PARTICIPANTS
	 */
	
	Route::get('participants/{qrid}/{event_id}/qrid', 'Admin\ParticipantsController@getParticipantViaQRID');
	Route::post('participants/delete-many', 'Admin\ParticipantsController@deleteMany');
	Route::post('participants/send-to-many', 'Admin\ParticipantsController@sendToMany');
	Route::get('participants/{id}/delete', 'Admin\ParticipantsController@destroy');
	Route::get('participants/bulk-upload/{event_id}', 'Admin\ParticipantsController@showBulkUploadForm');
	Route::post('participants/update-profile', 'Admin\ParticipantsController@updateProfile');
	Route::post('participants/import', 'Admin\ParticipantsController@import');
	Route::post('participants/update-import', 'Admin\ParticipantsController@updateImport');
	Route::get('participants/{id}/diet', 'Admin\ParticipantsController@getDietPage');
	Route::get('participants/create/{id}', 'Admin\ParticipantsController@create');
	Route::get('participants/{id}/edit/{event_id}', 'Admin\ParticipantsController@edit');
	Route::resource('participants', 'Admin\ParticipantsController');

	/*
	 * EOF PARTICIPANTS
	 */


	/*
	 * EVENTS
	 */
	Route::get('events/{id}/mails/{mail_id}/delete', 'Admin\MailsController@destroy');
	Route::resource('events/{id}/mails', 'Admin\MailsController');

	Route::post('events/invite', 'Admin\EventsController@invite');
	Route::post('events/send-reminder', 'Admin\EventsController@sendReminder');
	
	Route::get('events/{event_id}/bulk', 'Admin\ParticipantsController@getBulkEventPage');
	Route::get('events/{event_id}/participant-download', 'Admin\EventsController@participantDownload');
	Route::get('events/{event_id}/{participant_id}/checkin', 'Admin\EventsController@checkIn');

	Route::get('events/{event_id}/kiosk', 'Admin\EventsController@getKioskPage');
	Route::get('events/{id}/kiosk', 'Admin\EventsController@getKioskPage');
	Route::get('events/{id}/participants', 'Admin\EventsController@getParticipants');
	Route::get('events/{id}/invite', 'Admin\EventsController@getInvitePage');
	Route::get('events/{id}/invite/{company_id}', 'Admin\EventsController@getInviteCompanyPage');
	Route::get('events/{id}/remind/{company_id}', 'Admin\EventsController@getRemindEmailPage');
	Route::get('events/{id}/delete', 'Admin\EventsController@destroy');
	Route::get('events/{id}/activate', 'Admin\EventsController@activate');
	Route::get('events/{id}/disable', 'Admin\EventsController@disable');
	Route::get('events/{id}/venue', 'Admin\EventsController@getEventVenuePage');
	Route::get('events/{id}/images', 'Admin\EventsController@getEventImagesPage');
	Route::get('events/{id}/contacts', 'Admin\EventsController@getEventContactsPage');
	Route::post('events/{id}/update-contacts', 'Admin\EventsController@updateContacts');

	Route::put('events/{id}/images', 'Admin\EventsController@updateImages');

	Route::resource('events', 'Admin\EventsController');
	/*
	 * EOF EVENTS
	 */


	/*
	 * VENUES
	 */
	Route::get('venues/{id}/delete', 'Admin\VenuesController@destroy');
	Route::resource('venues', 'Admin\VenuesController');
	/*
	 * EOF VENUES
	 */

	/*
	 * COMPANIES
	 */
	Route::get('companies/{id}/delete', 'Admin\CompaniesController@destroy');
	Route::resource('companies', 'Admin\CompaniesController');
	/*
	 * EOF COMPANIES
	 */

	/*
	 * LUCKY DRAW
	 */
	Route::get('lucky-draw/claim-prize/{winner_id}', 'Admin\LuckyDrawController@claimPrize');

	Route::post('re-draw', 'Admin\EventsController@reDraw');
	Route::post('pre-draw', 'Admin\EventsController@preDraw');
	Route::get('/pre-draw/{draw_id}', 'Admin\LuckyDrawController@getPreDraw');

	Route::get('lucky-draw/delete-winner/{winner_id}', 'Admin\LuckyDrawController@deleteWinner');
	Route::get('lucky-draw/get-winners/{raffle_id}/{prize_id}', 'Admin\LuckyDrawController@getWinners');


	Route::get('lucky-draw/get-winners-claimed/{raffle_id}/{prize_id}', 'Admin\LuckyDrawController@getWinnersClaimed');

	Route::post('lucky-draw/save-winner', 'Admin\LuckyDrawController@saveWinner');

	Route::get('lucky-draw/event/{event_id}', 'Admin\LuckyDrawController@getWinner');

	Route::get('events/{event_id}/lucky-draw/{raffle_id}/prizes', 'Admin\LuckyDrawController@getPrize');

	Route::get('events/{event_id}/winners/{raffle_id}/ppt', 'Admin\LuckyDrawController@generatePPT');

	Route::get('events/{event_id}/winners/{raffle_id}', 'Admin\LuckyDrawController@getWinnersPage');

	Route::get('events/{event_id}/lucky-draw/{raffle_id}/draw/{prize_id}/client', 'Admin\LuckyDrawController@getClientDrawPage');

	Route::get('events/{event_id}/lucky-draw/{raffle_id}/draw/{prize_id}/client', 'Admin\LuckyDrawController@getClientDrawPage');

	Route::get('trigger-draw/{event_id}/{raffle_id}/{prize_id}/{lucky_number}/{name}/{employee_number}', 'Admin\LuckyDrawController@triggerDraw');

	Route::get('events/{event_id}/lucky-draw/{raffle_id}/draw/{prize_id}', 'Admin\LuckyDrawController@getDraw');
	
	Route::resource('events/{id}/lucky-draw', 'Admin\LuckyDrawController');
	Route::resource('events/{id}/questions', 'Admin\QuestionsController');
	/*
	 * EOF LUCKY DRAW
	 */

	/*
	 * USERS
	 */

	// profile
	Route::get('my-profile', 'Admin\UsersController@getMyProfile');
	Route::post('save-profile', 'Admin\UsersController@saveProfile');

	// roles
	Route::get('users/roles', 'Admin\UsersController@getRoles');
	Route::get('users/roles/create', 'Admin\UsersController@getCreateRole');
	Route::get('users/roles/{id}', 'Admin\UsersController@getRole');
	Route::get('users/roles/{id}/delete', 'Admin\UsersController@deleteRole');
	Route::put('users/roles/{id}', 'Admin\UsersController@updateRole');
	Route::post('users/roles', 'Admin\UsersController@saveRole');
	Route::get('users/roles/{id}/permissions', 'Admin\UsersController@addPermissionToRole');

	Route::get('users/assign/{permission_id}/{role_id}', 'Admin\UsersController@assignPermissionToRole');
	Route::get('users/revoke/{permission_id}/{role_id}', 'Admin\UsersController@revokePermissionToRole');
	Route::get('users/permissions', 'Admin\UsersController@getPermissions');
	Route::get('users/permissions/create', 'Admin\UsersController@getCreatePermission');
	Route::get('users/permissions/{id}', 'Admin\UsersController@getPermission');
	Route::get('users/permissions/{id}/delete', 'Admin\UsersController@deletePermission');
	Route::post('users/permissions', 'Admin\UsersController@savePermission');
	Route::put('users/permissions/{id}', 'Admin\UsersController@updatePermission');

	Route::get('users/delete/{id}', 'Admin\UsersController@destroy');
	Route::resource('users', 'Admin\UsersController');
	/*
	 * EOF USERS
	 */
});