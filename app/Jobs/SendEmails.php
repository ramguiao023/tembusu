<?php

namespace App\Jobs;

use App\Participant;
use App\EventParticipant;
use App\InvitedParticipant;
use App\Event;
use App\Mail as Mailer;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Mail\EventInvitation;
use Mail;

class SendEmails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $participants = [];

        foreach($this->data as $participant) {
            $invited_participants = InvitedParticipant::firstOrCreate($participant);

            $event_participant = EventParticipant::where('participant_id', $invited_participants->participant_id)->first();

            if(@$event_participant->status == 3) {
                $event_participant->status = 4;
                $event_participant->save();
            }

            $participants[] = Participant::find($invited_participants->participant_id);
        }

        $event = Event::with('venue')
                      ->with('company')
                      ->find($this->data[0]['event_id']);

        // GET INVITATION EMAIL CONTENT
        $mail_content = Mailer::where('event_id', $this->data[0]['event_id'])
                                ->where('name', 'Invitation')
                                ->first();

        foreach($participants as $participant) {
            Mail::to($participant->email)->queue(new EventInvitation($participant, $event, $mail_content));
        }
    }
}






