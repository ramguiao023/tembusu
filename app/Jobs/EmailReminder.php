<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Participant;
use App\EventParticipant;
use App\InvitedParticipant;
use App\Event;
use App\Mail as Mailer;
use App\Mail\SendReminder;
use Mail;

class EmailReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()    
    {
        $participants = [];

        foreach($this->data as $participant) {
            $event_participant = EventParticipant::find($participant['participant_id']);
            $participants[] = Participant::find($event_participant->participant_id);
        }

        $event = Event::with('venue')
                      ->with('company')
                      ->find($this->data[0]['event_id']);

        // GET INVITATION EMAIL CONTENT
        $mail_content = Mailer::where('event_id', $this->data[0]['event_id'])
                                ->where('name', 'Reminder')
                                ->first();

        foreach($participants as $participant) {
            Mail::to($participant->email)->queue(new SendReminder($participant, $event, $mail_content));
        }
    }
}










