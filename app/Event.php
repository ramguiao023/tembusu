<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public $guarded = [];

    protected $dates = ['start_date', 'end_date'];

    public function company()
    {
    	return $this->hasOne('App\Company', 'id', 'company_id');
    }

    public function user()
    {
    	return $this->hasOne('App\User', 'id', 'created_by');
    }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function venue()
    {
        return $this->hasOne('App\Venue', 'id', 'venue_id');
    }

    public function contacts()
    {
        return $this->hasMany('App\Contact');
    }

    public function getStartDateAttribute($value)
    {
        return date("F d, Y", strtotime($value));
    }

    public function getEndDateAttribute($value)
    {
        return date("F d, Y", strtotime($value));
    }

    public function getWallpaperAttribute($value)
    {
        if($value) {
            return asset('uploads/events/'.$value);
        } else {
            return asset('event-wallpaper.png');
        }
    }

    public function getBannerImageAttribute($value)
    {
        if($value) {
            return asset('uploads/events/'.$value);
        } else {
            return asset('event-cover-photo.png');
        }
    }
}
