<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use ImageManagerStatic;

class IsImage implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $split = explode( '/', $value );
        
        $type = $split[1];

        // dd($type);
        return ($type == "png;base64," || $type == "jpg;base64," || $type == "jpeg;base64," || $type == "gif;base64,");
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute should be an image and has these formats (.jpg, .png, .gif, .jpeg).';
    }
}
