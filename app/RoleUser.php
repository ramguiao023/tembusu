<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $table = "role_user";
    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = "user_id";
}
