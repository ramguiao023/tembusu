<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    public $guarded = [];

    public function events_participants()
    {
    	return $this->hasOne('App\EventParticipant', 'id', 'events_participants_id');
    }

    public function prize()
    {
    	return $this->hasOne('App\Prize', 'id', 'prize_id');
    }
}
