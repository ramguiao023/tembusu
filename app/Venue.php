<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
	public $guarded = [];
	
    public function company()
    {
    	return $this->hasOne('App\Company', 'id', 'company_id');
    }
}
