<?php

namespace App\Mail;

use App\Participant;
use App\Event;
use App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReminder extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $participant;
    public $event;
    public $mail_content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Participant $participant, Event $event, Mail $mail_content)
    {
        $this->participant = $participant;
        $this->event = $event;
        $this->mail_content = $mail_content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail_content = $this->mail_content->content;
        
        return $this->view('emails.reminder')
                    ->with('participant', $this->participant)
                    ->with('event', $this->event)
                    ->with('mail_content', $mail_content)
                    ->subject(str_replace("[event-name]", $this->event->title, $this->mail_content->subject ));
    }
}
