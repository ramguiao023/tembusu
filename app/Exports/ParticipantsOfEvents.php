<?php

namespace App\Exports;

use App\Participant;
use App\Question;
use App\Answer;
use App\EventParticipant;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ParticipantsOfEvents implements FromView
{

	protected $event_id = null;

	public function __construct($event_id)
	{
		$this->event_id = $event_id;
	}

    /**
    * @return \Illuminate\Support\Collection
    */

    public function view(): View
    {
        $participants = EventParticipant::with('participant')
                             ->where('event_id', $this->event_id)
                             ->get();

        foreach($participants as $participant) {
            $questions = Question::where('event_id', $this->event_id)
                                    ->get();

            foreach($questions as $question) {
                $answer = Answer::where('question_id', $question->id)
                               ->where('participant_id', $participant->participant_id)
                               ->first();

                $question->answer = $answer;
            }

            $participant->questions = $questions;
        }
        
        return view('admin.exports.participants', [
            'participants' => $participants
        ]);
    }
    // public function collection()
    // {
        

    //     return $;
    // }
}
