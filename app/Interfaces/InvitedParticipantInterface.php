<?php

namespace App\Interfaces;

interface InvitedParticipantInterface
{
	public function inviteParticipants($data);
}