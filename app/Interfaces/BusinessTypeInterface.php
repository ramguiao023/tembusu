<?php

namespace App\Interfaces;

interface BusinessTypeInterface
{
	public function fetchBusinessTypes();
}