<?php

namespace App\Interfaces;

interface UserInterface
{
	public function getAllUsers();
	public function getAUser($id);
	public function getAllRoles();
	public function getRole($id);
	public function saveNewRole($request);
	public function deleteRole($id);
	public function deletePermission($id);
	public function updateRole($request, $id);
	public function createUser($request);
	public function updateProfile($request, $id);
	public function updateMyProfile($request);
	public function updateProfilePicture($id, $path);
	public function deleteUser($id);
	public function blockUser($user);
	public function getAllPermissions();
	public function updatePermission($request, $id);
	public function getPermissionsPerRole($role_id);
	public function assignPermissionToRole($permission_id, $role_id);
	public function revokePermissionToRole($permission_id, $role_id);
	public function getRolesPerPermission($permission_id);
}
