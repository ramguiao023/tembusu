<?php

namespace App\Interfaces;

interface CompanyInterface
{
	public function findCompany($id);
	public function getCompanyViaSlug($slug);
	public function getAllCompanies();
	public function createCompany($request);
}
