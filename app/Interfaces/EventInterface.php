<?php

namespace App\Interfaces;

interface EventInterface
{
	public function getEvents();
	public function getEvent($id);
	public function getParticipantsOfAnEvent($event_id);
	public function createEvent($data);
	public function updateEvent($data, $id);
	public function setStatus($id, $status);
}