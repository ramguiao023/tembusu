<?php
namespace App\Interfaces;


interface ParticipantInterface
{
	public function getParticipants();
	public function addParticipant($request);
	public function getParticipant($id);
	public function getParticipantViaEmployeeNumber($employee_number);
	public function getParticipantsPerCompanyId($id);
	public function updateParticipantSentStatus($id);
	public function getLoggedInParticipant();
	public function updateParticipant($request, $id);
	public function deleteParticipant($id);
	public function changePassword($request);
	public function verifyParticipant($request);
}