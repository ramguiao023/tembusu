<?php

namespace App\Interfaces;

interface VenueInterface
{
	public function getVenues();
	public function addVenue($data);
	public function getVenue($id);
	public function editVenue($data, $id); 
	public function deleteVenue($id);
}