<?php

namespace App\Interfaces;

interface DepartmentInterface
{
	public function getAllDepartmentsOfACompany($company_id);
}
