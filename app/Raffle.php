<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Raffle extends Model
{
    public $guarded = [];

    public function prizes()
    {
    	return $this->hasMany('App\Prize');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'shift_personnel');
    }

    public function getImageAttribute($value)
    {
    	if($value) {
    		return asset('uploads/raffles/'.$value);
    	} else {
    		return "";
    	}
    }
    public function event()
    {
        return $this->hasOne('App\Event', 'id', 'event_id');
    }
}
