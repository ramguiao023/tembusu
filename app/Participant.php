<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Participant extends Authenticatable
{
    use HasApiTokens, Notifiable;

	public $guarded = [];
    public $ignoreMutator = false;
    protected $hidden = [];
	
    public function findForPassport($username) {
        return $this->where('username', $username)
                    ->orWhere('email', $username)
                    ->first();
    }

    public function getProfileImageAttribute($value)
    {
        if($value) {
            return asset('uploads/profile-picture/' . $value);
        } else {
            return asset('uploads/profile-picture/profile-image.png');
        }
    }

    public function company()
    {
    	return $this->hasOne('App\Company', 'id', 'company_id');
    }

    public function department()
    {
    	return $this->hasOne('App\Department', 'id', 'department_id');
    }

    public function setPasswordAttribute($value)
    {
        if($this->ignoreMutator === false) {
            $this->attributes['password'] = bcrypt($value);
        } else {
            $this->attributes['password'] = $value;
        }
    }

    public function event_participant()
    {
        return $this->hasOne('App\EventParticipant', 'participant_id', 'id');
    }
}
