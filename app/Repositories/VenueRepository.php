<?php

namespace App\Repositories;

use App\Interfaces\VenueInterface;

use App\Venue;

use Auth;

class VenueRepository implements VenueInterface
{
	protected $venue;

	public function __construct(Venue $venue)
	{
		$this->venue = $venue;
	}

	public function getVenues()
	{
		if(Auth::user()->roles->first()->name == "superadmin") {
			return $this->venue
						->with('company')
						->latest()
						->get();
		} else {
			return $this->venue
						->with('company')
						->where('company_id', Auth::user()->company_id)
						->latest()->get();
		}
	}

	public function getVenue($id)
	{
		return $this->venue->findOrFail($id);
	}

	public function addVenue($data)
	{
		return $this->venue->create($data);
	}

	public function editVenue($data, $id)
	{
		return $this->venue
					->findOrFail($id)
					->update($data);
	}

	public function deleteVenue($id)
	{
		return $this->venue
					->findOrFail($id)
					->delete();
	}
}

