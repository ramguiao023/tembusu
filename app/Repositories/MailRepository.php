<?php

namespace App\Repositories;

use App\Mail;

class MailRepository
{

	protected $mail = null;

	public function __construct(Mail $mail)
	{
		$this->mail = $mail;
	}

	public function saveMail($request)
	{
		$mail = $this->mail
					 ->create($request);

		return $mail;
	}

	public function updateMail($request, $id)
	{
		$mail = $this->mail
					 ->find($id)
					 ->update($request);

		$mail = $this->mail->find($id);

		return $mail;
	}

	public function getMails($id)
	{
		$mails = $this->mail->where('event_id', $id)
							->get();

		return $mails;
	}

	public function getMail($id)
	{
		$mail = $this->mail->findOrFail($id);

		return $mail;
	}

	public function deleteMail($id)
	{
		$mail = $this->mail->findOrFail($id)->delete();

		return $mail;
	}
}