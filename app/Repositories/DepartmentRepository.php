<?php
namespace App\Repositories;

use App\Interfaces\DepartmentInterface;

use App\Department;

use Auth;

class DepartmentRepository implements DepartmentInterface
{
	protected $department = null;

	public function __construct(Department $department)
	{
		$this->department = $department;
	}

	public function getAllDepartmentsOfACompany($company_id)
	{
		$departments = $this->department
						->where('company_id', $company_id)
						->get();

		return $departments;
	}
}
