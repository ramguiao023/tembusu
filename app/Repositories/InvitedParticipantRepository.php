<?php
namespace App\Repositories;

use App\Interfaces\InvitedParticipantInterface;

use App\InvitedParticipant;
use App\Participant;
use App\EventParticipant;
use App\Mail as Mailer;
use App\Event;

use Auth;

use App\Mail\EventInvitation;

use Mail;

class InvitedParticipantRepository implements InvitedParticipantInterface
{
	protected $invited_participant;
	protected $participant;
	protected $event;

	public function __construct(
							InvitedParticipant $invited_participant,
							Participant $participant,
							Event $event
							)
	{
		$this->participant = $participant;
		$this->invited_participant = $invited_participant;
		$this->event = $event;
	}

	public function inviteParticipants($data)
	{
		$participants = [];

		foreach($data as $participant) {
			$invited_participants = $this->invited_participant
							 	 ->firstOrCreate($participant);

			$event_participant = EventParticipant::where('participant_id', $invited_participants->participant_id)->first();

			if(@$event_participant->status == 3) {
				$event_participant->status = 4;
				$event_participant->save();
			}

			$participants[] = $this->participant->find($invited_participants->participant_id);
		}

		$event = $this->event
					  ->with('venue')
					  ->with('company')
					  ->find($data[0]['event_id']);

		$mail_content = Mailer::where('event_id', "")
                                ->where('name', 'Invitation')
                                ->first();

		foreach($participants as $participant) {
			Mail::to($participant->email)->send(new EventInvitation($participant, $event, $mail_content));
		}
		
		return $participants;
	}
}