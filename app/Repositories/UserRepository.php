<?php
namespace App\Repositories;

use App\Interfaces\UserInterface;

use App\User;

use App\Role;
use App\Permission;
use App\RoleUser;
use App\PermissionRole;

use Storage;

class UserRepository implements UserInterface
{
	protected $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	public function getAllUsers()
	{
		return $this->user->orderBy('first_name', 'DESC')->get();
	}
	public function getAllRoles()
	{
		return Role::all();
	}
	public function getRole($id)
	{
		return Role::find($id);
	}

	public function getPermissionsPerRole($role_id)
	{
		$permissions = PermissionRole::join('permissions', 'permissions.id', '=', 'permission_id')
									->where('role_id', $role_id)
									->get();

		return $permissions;
	}

	public function getRolesPerPermission($permission_id)
	{
		$permissions = PermissionRole::join('roles', 'roles.id', '=', 'role_id')
									->where('permission_id', $permission_id)
									->get();

		return $permissions;
	}

	public function getPermission($id)
	{
		return Permission::find($id);
	}

	public function updateRole($request, $id)
	{
		$role = Role::find($id);

		$role->update($request->except(['_token', '_method', 'save']));

		return $role;
	}

	public function updatePermission($request, $id)
	{
		$permission = Permission::find($id);

		$permission->update($request->except(['_token', '_method', 'save']));

		return $permission;
	}

	public function saveNewRole($request)
	{
		$role = new Role;

		$role->create($request->except(['_token', '_method', 'save']));

		return $role;
	}
	public function saveNewPermission($request)
	{
		$permission = new Permission;

		$permission->create($request->except(['_token', '_method', 'save']));

		return $permission;
	}
	public function getAUser($id)
	{
		return $this->user
					->with('role')
					->find($id);
	}
	public function createUser($request)
	{


		$user = $this->user->create($request->except('save', 'role'));

		$roleuser = new RoleUser;

		$roleuser->user_id = $user->id;
		$roleuser->role_id = $request->role;

		$roleuser->save();

		return $user;
	}
	public function updateProfile($request, $id)
	{
		$user = $this->user->find($id);

		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;

		if(@$request->email) {
			$user->email = $request->email;
		}
		if(@$request->password) {
			$user->password = $request->password;
		}

		$user->save();

		return $user;
	}

	public function updateMyProfile($request)
	{
		$user = $this->user->find($request->id);

		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;

		if(@$request->password) {
			$user->password = $request->password;
		}

		if(@$request->profile_photo_path) {
			//delete old photo
			@unlink("uploads/profile-picture/".$user->profile_photo);

			$user->profile_photo = $request->profile_photo_path;
		}

		if(@$request->email) {
			$user->email = $request->email;
		}

		$user->save();

		if($request->role) {
			$roleuser = RoleUser::where('user_id', $user->id)->first();
			
			if($roleuser) {
				$roleuser->update(
					[
						'role_id' => $request->role
					]
				);
			} else {
				$new_role_user = new RoleUser;

				$new_role_user->user_id = $user->id;
				$new_role_user->role_id = $request->role;

				$new_role_user->save();
			}
		}
		
		return $user;
	}

	public function updateProfilePicture($id, $path)
	{
		$user = $this->user->find($id);

		$user->profile_photo = $path;

		$user->save();

		return $user;
	}
	public function deleteUser($id)
	{
		$user = $this->user->find($id);

		$user->status = 0;

		$user->save();

		$user->delete();

		return $user;
	}
	public function blockUser($user)
	{

	}
	public function deleteRole($id)
	{
		$roleuser = RoleUser::where('role_id', $id)->first();

		$roleuser->delete();

		$role = Role::whereId($id)->delete();

		return true;
	}
	
	public function deletePermission($id)
	{
		$permission_role = PermissionRole::where('role_id', $id)->first();

		$permission_role->delete();

		$permission = Permission::whereId($id)->delete();

		return true;
	}

	public function getAllPermissions()
	{
		$permissions = Permission::all();

		foreach($permissions as $permission) {
			$roles = $this->getRolesPerPermission($permission->id);
			$roles_array = [];
			foreach($roles as $role) {
				$roles_array[] = $role->display_name;

			}

			$permission->roles = $roles_array;
		}

		return $permissions;
	}

	public function assignPermissionToRole($permission_id, $role_id)
	{
		$permission = new PermissionRole;

		$permission->permission_id = $permission_id;
		$permission->role_id = $role_id;

		$permission->save();

		return $permission;
	}

	public function revokePermissionToRole($permission_id, $role_id)
	{
		$permission_to_delete = PermissionRole::where('permission_id', $permission_id)
									->where('role_id', $role_id);

		$permission = $permission_to_delete->get();

		$permission_to_delete->delete();

		return $permission;
	}

	public function getUsersByCompany($id)
	{
		$users = $this->user
					  ->where('company_id', $id)
					  ->latest()
					  ->get();

		return $users;
	}
}

?>