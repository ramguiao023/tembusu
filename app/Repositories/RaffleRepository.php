<?php

namespace App\Repositories;

use App\Raffle;
use App\Prize;
use App\Winner;
use Auth;
use Image;

class RaffleRepository
{
	protected $raffle = null;
	protected $prize = null;
	protected $winner = null;

	public function __construct(Raffle $raffle, Prize $prize, Winner $winner)
	{
		$this->raffle = $raffle;
		$this->prize = $prize;
		$this->winner = $winner;
	}

	public function createRaffleWithPrizes($request)
	{
		$bg_image = "";

		// if manual, use base64 image
		if(@$request['upload_manually'] == 1) {
			if(@$request['lucky_draw_background_image'] != "") {
				$bg_image = $this->uploadImage($request['lucky_draw_background_image'], 'raffles');
			}
		} else {
			if(@$request['lucky_draw_background_image'] != "") {
				$bg_image = $this->uploadImageFile($request, 'raffles');
			}
		}

		$new_raffle = $this->raffle
					   ->create([
					   		'name' => $request['lucky_draw_name'],
					   		'image' => $bg_image,
					   		'created_by' => Auth::user()->id,
					   		'event_id' => $request['event_id'],
					   		'shift_personnel' => $request['shift_personnel']
					   ]);

		if(@$request['upload_manually'] == 1) {
			if(@count($request['prizes']) > 0) {
				for($i = 0; $i < count($request['prizes']); $i++) {
					$prize = $this->prize
								  ->create([
								  	'name' => $request['prizes'][$i],
								  	'raffle_id' => $new_raffle->id,
								  	'number_of_winners' => $request['winners'][$i],
								  	'type' => $request['type'][$i],
								  ]);
				}
			}
		}

		$raffle = $this->getRaffle($new_raffle->id);

		return $raffle;
	}

	public function getRaffle($id, $prize_type = 0)
	{
		$raffle = $this->raffle
					   ->with('user')
					   ->with('event')
					   ->where('id', $id)
					   ->first();

		if($prize_type == 0) {
			$prizes = $this->prize
					   ->with('winners.events_participants.participant')
					   ->where('raffle_id', $id)
					   ->get();
		} else {
			$prizes = $this->prize
					   ->with('winners.events_participants.participant')
					   ->where('raffle_id', $id)
					   ->where('type', $prize_type)
					   ->get();
		}

		$raffle->prizes = $prizes;

		return $raffle;
	}

	public function getRafflesByEventId($event_id)
	{
		$raffles = $this->raffle
					    ->with('prizes')
					    ->with('user')
					    ->where('event_id', $event_id)
					    ->latest()
					    ->get();

		return $raffles;
	}

	public function updateRaffleWithPrizes($request, $id)
	{
		$bg_image = "";

		if($request['lucky_draw_background_image'] != "") {
			$bg_image = $this->uploadImage($request['lucky_draw_background_image'], 'raffles');
		}

		if($bg_image == "") {
			$edit_raffle = $this->raffle
						   ->find($id)
						   ->update([
						   		'name' => $request['lucky_draw_name'],
						   		'created_by' => Auth::user()->id,
						   		'event_id' => $request['event_id'],
						   		'shift_personnel' => $request['shift_personnel']
						   ]);
		} else {
			$edit_raffle = $this->raffle
						   ->find($id)
						   ->update([
						   		'name' => $request['lucky_draw_name'],
						   		'image' => $bg_image,
						   		'created_by' => Auth::user()->id,
						   		'event_id' => $request['event_id'],
						   		'shift_personnel' => $request['shift_personnel']
						   ]);
		}
		
		// $delete_prize = $this->deletePrizesOfRaffle($id);

		if(@count($request['prizes']) > 0) {
			for($i = 0; $i < count($request['prizes']); $i++) {
				if(@$request['prizes_id'][$i]) {
					$prize = $this->prize
							  ->find($request['prizes_id'][$i])
							  ->update([
							  	'name' => $request['prizes'][$i],
							  	'raffle_id' => $id,
							  	'number_of_winners' => $request['winners'][$i],
							  ]);
				} else {
					$prize = $this->prize
							  ->create([
							  	'name' => $request['prizes'][$i],
							  	'raffle_id' => $id,
							  	'number_of_winners' => $request['winners'][$i],
							  ]);
				}
				
			}
		}

		$raffle = $this->getRaffle($id);

		return $raffle;
	}

	public function deletePrizesOfRaffle($raffle_id)
	{
		return $this->prize
					->where('raffle_id', $raffle_id)
					->delete();
	}

	public function getPrize($id)
	{
		$prize = $this->prize->find($id);
		
		return $prize;
	}

	public function saveWinner($request)
	{
		$winner = $this->winner->create($request);

		return $winner;
	}

	public function getWinners($raffle_id, $prize_id = 0)
	{
		if($prize_id == 0) {
			$winners = $this->winner
						   ->with('events_participants.participant')
						   ->with('prize')
						   ->where('raffle_id', $raffle_id)
						   ->get();
		} else {
			$winners = $this->winner
						   ->with('events_participants.participant')
						   ->with('prize')
						   ->where('raffle_id', $raffle_id)
						   ->where('prize_id', $prize_id)
						   ->get();
		}
		
		return $winners;
	}

	public function claimPrize($winner_id)
	{
		$winner = $this->winner->find($winner_id)
						->update([
							'status' => 1
						]);

		$winner_info = $this->winner->find($winner_id);

		return $winner_info;
	}

	public function getWinnersClaimed($raffle_id, $prize_id = 0)
	{
		if($prize_id == 0) {
			$winners = $this->winner
						   ->with('events_participants.participant')
						   ->with('prize')
						   ->where('raffle_id', $raffle_id)
						   ->where('winners.status', 1)
						   ->get();
		} else {
			$winners = $this->winner
						   ->with('events_participants.participant')
						   ->with('prize')
						   ->where('raffle_id', $raffle_id)
						   ->where('prize_id', $prize_id)
						   ->where('winners.status', 1)
						   ->get();
		}
		
		return $winners;
	}

	public function getWinner($winner_id)
	{
		$winners = $this->winner
					   ->with('events_participants.participant')
					   ->find($winner_id);

		return $winners;
	}

	public function deleteWinner($winner_id)
	{
		$winner = $this->winner->find($winner_id);

		$winner->delete();

		return $winner;
	}

	private function uploadImageFile($image, $path)
	{
		$data = $image['lucky_draw_background_image'];

        $png_url = "raffle-".time().".jpg";

        $path = public_path() . "/uploads/" . $path . "/";

        $data->move($path, $png_url);

        return $png_url;
	}



	private function uploadImage($image, $path)
    {
        $data = $image;

        $png_url = "raffle-".time().".jpg";

        $path = public_path() . "/uploads/" . $path . "/" . $png_url;

        Image::make(file_get_contents("data:image/png;base64,".$data))->save($path); 

        return $png_url;
    }
}
