<?php

namespace App\Repositories;

use App\Interfaces\EventInterface;
use App\Event;
use App\Participant;
use App\EventParticipant;
use App\Answer;
use App\Contact;
use App\Question;
use App\Mail as MailModel;

use App\Mail\ParticipantConfirmation;
use App\Mail\OrganizerConfirmation;
use Mail;

class EventRepository implements EventInterface
{
	protected $event = null;
	protected $answer = null;
	protected $participant = null;
	protected $event_participant = null;
	protected $contact = null;
	protected $question = null;
	protected $mail = null;

	public function __construct(Event $event,
								Answer $answer,
								EventParticipant $event_participant,
								Participant $participant,
								Contact $contact,
								Question $question,
								MailModel $mail)
	{
		$this->participant = $participant;
		$this->answer = $answer;
		$this->event = $event;
		$this->contact = $contact;
		$this->event_participant = $event_participant;
		$this->question = $question;
		$this->$mail = $mail;
	}

	public function getEvents()
	{
		$events = $this->event
					   ->with('company')
					   ->with('user')
					   ->with('venue')
					   ->with('contacts')
					   ->orderBy('start_date', 'DESC')
					   ->get();

		return $events;
	}

	public function updateQuestion($request)
	{
		for($i = 0; $i < count($request['questions']); $i++) {
			if($request['question_id'][$i] == "") {
				if($request['questions'][$i] != "") {
					$question = $this->question
								 ->create([
									'event_id' => $request['event_id'][$i],
									'question' => $request['questions'][$i],
									'type' => $request['type'][$i],
									'choices' => $request['answers'][$i]
								 ]);
				}
			} else {
				if($request['questions'][$i] != "") {
					$question = $this->question
								 ->find($request['question_id'][$i])
								 ->update([
									'event_id' => $request['event_id'][$i],
									'question' => $request['questions'][$i],
									'type' => $request['type'][$i],
									'choices' => $request['answers'][$i]
								 ]);
				} else {
					$question = $this->question
								 ->find($request['question_id'][$i])
								 ->delete();
				}
			}
		}

		return $request;
	}

	public function getParticipantsOfAnEvent($event_id)
	{
		$participants = $this->event_participant
							 ->with('participant')
							 ->where('event_id', $event_id)
							 ->get();

		foreach($participants as $participant) {
			$questions = $this->question
						  ->where('event_id', $event_id)
						  ->get();

			foreach($questions as $question) {
				$answer = $this->answer
							   ->where('question_id', $question->id)
							   ->where('participant_id', $participant->participant_id)
							   ->first();

				$question->answer = $answer;
			}

			$participant->questions = $questions;
		}

		return $participants;
	}

	public function getEvent($id)
	{
		$event = $this->event
					   ->with('company')
					   ->with('user')
					   ->with('venue')
					   ->with('contacts')
					   ->with('questions')
					   ->findOrFail($id);

		$is_close = false;

		if(date('Y-m-d') > $event->registration_closing_date) {
			$is_close = true;
		}

		$event->is_close = $is_close;

		return $event;
	}

	public function createEvent($data)
	{
		$event = $this->event->create($data);

		$mails = $this->createDefaultMails($event->id);

		return $event;
	}

	public function createDefaultMails($event_id)
	{
		$json = file_get_contents(url('event-mail.json'));

		$array = json_decode($json);

		foreach($array->mails as $mail) {
			$array_to_save = [
								"title" => $mail->title,
								"subject" => $mail->subject,
								"content" => $mail->content,
								"event_id" => $event_id
							];

			$mail = new MailModel;

			$mail->name = $array_to_save['title'];
			$mail->subject = $array_to_save['subject'];
			$mail->content = $array_to_save['content'];
			$mail->event_id = $array_to_save['event_id'];

			$mail->save();
		}

		$mails = MailModel::where('event_id', $event_id)->get();

		return $mails;
	}

	public function convertProvisionToActualData($content, $event_id)
	{
		$event = $this->getEvent($event_id);
		
		return preg_replace( "/\[event-name]/", $event->title, $content);
	}

	public function updateEvent($data, $id)
	{

		$mails = MailModel::where('event_id', $id)->get();

		if(count($mails) <= 0) {
			$mails = $this->createDefaultMails($id);
		}

		return $this->event
					->find($id)
					->update($data);
	}

	public function deleteEvent($id)
	{
		return $this->event
					->find($id)
					->delete();
	}

	public function setStatus($id, $status)
	{
		return $this->event
					->find($id)
					->update([
						'status' => $status
					]);
	}

	public function joinEvent($request)
	{
		$request['created_at'] = date("Y/m/d H:i:s");

		$raffle_code = "";

		if(@$request['raffle_code'] == "") {
			$raffle_code = $this->generateRaffleCode($request['event_id']);
		} else {
			$raffle_code = $request['raffle_code'];
		}
		$event_participant = $this->event_participant
									  ->where('participant_id', $request['participant_id'])
									  ->where('event_id', $request['event_id'])
									  ->update(
									  	$request
									  );
		// if(@$request['table_number'] != "") {
		// 	$event_participant = $this->event_participant
		// 							  ->update(
									  	
		// 							  	$request
		// 							  );
		// } else {
		// 	$event_participant = $this->event_participant
		// 							  ->update(
									  	
		// 							  	$request
		// 							  );
		// }
		

		$participant = $this->participant->find($request['participant_id']);

		$event = $this->event->with('venue')->with('company')->find($request['event_id']);

		$mails = MailModel::where('event_id', $event->id)
						->where('name', 'Confirmation')
						->first();

		if($request['status'] == 1) {
            @Mail::to($participant->email)->send(new ParticipantConfirmation($participant, $event, $mails));
		}

		$this->event_participant
						->where('participant_id', $request['participant_id'])
						->first();

		return $event_participant;
	}

	public function answerQuestion($request)
	{
		foreach($request as $answer) {
			$ans = $this->answer
						->create([
							'question_id' => $answer['question_id'],
							'answer' => $answer['answer'],
							'participant_id' => $answer['participant_id']
						]);
		}

		return $request;
	}

	public function generateRaffleCode($event_id)
	{
		$raffle_code = rand(111111, 999999);

		$checking = $this->event_participant
					     ->where('event_id', $event_id)
					     ->where('raffle_code', $raffle_code)
					     ->get();

		if(count($checking) > 0) {
			$this->generateRaffleCode($event_id);
		} else {
			return $raffle_code;
		}
	}

	public function checkIn($event_id, $participant_id)
	{
		$event_participant = $this->event_participant
								  ->where('event_id', $event_id)
								  ->where('participant_id', $participant_id)
								  ->update(['status' => 2]);
		

		return $event_participant;
	}

	public function updateEventContacts($data, $event_id)
	{
		$contact = null;

		for($i = 0; $i<count($data['data'][0]); $i++) {
			$contact = $this->contact->create([
						'event_id' => $event_id,
						'contact_person' => $data['data'][0][$i],
						'contact_person_email' => $data['data'][1][$i],
						'contact_person_number' => $data['data'][2][$i],
					]);
		}

		return $contact;
	}

	public function deleteEventContacts($event_id)
	{
		$event = $this->contact
					  ->where('event_id', $event_id);

		$info = $event->get();


		$event->delete();

		return $event;
	}

	public function getRandomEventsParticipantsPerEventId($event_id, $status = 1)
	{
		$event_participant = $this->event_participant
								   ->select(
								   	'events_participants.id',
								   	'events_participants.participant_id',
								   	'raffle_code'
								   )
								   ->with('participant')
								   ->leftJoin('winners', 'winners.events_participants_id', '=', 'events_participants.id')
								   ->where('event_id', $event_id)
								   ->where('events_participants.status', $status)
								   ->whereNull('winners.events_participants_id')
								   ->get();

		if(count($event_participant) > 0) {
			return $event_participant->random(1)->first();
		} else {
			return $event_participant;
		}
	}

	public function getRandomEventsParticipantsPerEventIdMultiple($event_id, $status = 1, $number_of_participants)
	{
		$event_participant = $this->event_participant
								   ->select(
								   	'events_participants.id',
								   	'events_participants.participant_id',
								   	'raffle_code'
								   )
								   ->with('participant')
								   ->leftJoin('winners', 'winners.events_participants_id', '=', 'events_participants.id')
								   ->where('event_id', $event_id)
								   ->where('status', $status)
								   ->whereNull('winners.events_participants_id')
								   ->get();

		if(count($event_participant) > 0) {
			return $event_participant->random($number_of_participants);
		} else {
			return $event_participant;
		}
	}
}