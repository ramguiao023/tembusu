<?php

namespace App\Repositories;

use App\Interfaces\BusinessTypeInterface;

use App\BusinessType;

class BusinessTypeRepository implements BusinessTypeInterface
{
	protected $business_type;

	public function __construct(BusinessType $business_type)
	{
		$this->business_type = $business_type;
	}

	public function fetchBusinessTypes()
	{
		return $this->business_type->get();
	}
}