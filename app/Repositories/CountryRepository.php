<?php

namespace App\Repositories;

use App\Interfaces\CountryInterface;

use App\Country;

class CountryRepository implements CountryInterface
{
	protected $country;

	public function __construct(Country $country)
	{
		$this->country = $country;
	}

	public function fetchCountries()
	{
		return $this->country->get();
	}
}