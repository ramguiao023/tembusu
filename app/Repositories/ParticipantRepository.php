<?php
namespace App\Repositories;

use App\Interfaces\ParticipantInterface;

use App\Participant;
use App\EventParticipant;

use Auth;

class ParticipantRepository implements ParticipantInterface
{
	protected $participant = null;
	protected $event_participant = null;

	public function __construct(Participant $participant, EventParticipant $event_participant)
	{
		$this->participant = $participant;
		$this->event_participant = $event_participant;
	}

	public function getEventParticipant($id)
	{
		$event_participant = $this->event_participant
								  ->where('participant_id', $id)
								  ->first();

		return $event_participant;
	}

	public function getParticipants()
	{
		if(Auth::user()->roles->first()->name == "superadmin") {
			$participants = $this->participant
							 ->with('company')
							 ->orderBy('name', 'ASC')
							 ->get();
		} else {
			$participants = $this->participant
							 ->with('company')
							 ->where('company_id', Auth::user()->company_id)
							 ->orderBy('name', 'ASC')
							 ->get();
		}
		

		return $participants;
	}

	public function verifyParticipant($request)
	{
		$participant = $this->participant
							->with('company')
							->whereRaw('LOWER(name) = ?', [strtolower($request->name)])
							->whereRaw('LOWER(employee_number) = ?', [strtolower($request->employee_number)])
							->where('birthdate', $request->birthdate)
							->first();

		return $participant;
	}

	public function getParticipantsPerCompanyId($id)
	{
		$participants = $this->participant
							 ->with('company')
							 ->where('company_id', $id)
							 ->get();

		return $participants;
	}

	public function getInvitedParticipants($event_id)
	{
		$participants = $this->event_participant
							 ->with('participant')
							 ->where('event_id', $event_id)
							 ->get();

		return $participants;
	}

	public function addParticipant($request)
	{
		$participant = $this->participant
							->create($request->except(['_token', 'event_id', 'table_number', 'raffle_code']));

		$update_participant = $this->participant
							->find($participant->id)
							->update(['qrid' => $this->generateQRID()]);

		$participant = $this->participant
							->find($participant->id);

		return $participant;
	}

	public function generateQRID()
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randstring = '';
	    for ($i = 0; $i < 10; $i++) {
	        $randstring = $characters[rand(0, strlen($characters)-1)];
	    }

	    return $randstring.uniqid().time();
	}

	public function getParticipant($id, $event_id = "")
	{
		if($event_id != "") {
			$participant = $this->participant->with(array('event_participant' => function($query) use ($event_id) {
									$query->where('events_participants.event_id', $event_id);
								}))
								->with('company')
								->where('id', $id)
								->first();
		} else {
			$participant = $this->participant
								->with('event_participant')
								->with('company')
								->find($id);
		}
		
		return $participant;
	}

	public function getParticipantByUsername($username)
	{
		$participant = $this->participant
							->with('company')
							->where('username', $username)
							->first();

		return $participant;
	}

	public function getParticipantViaEmployeeNumber($employee_number)
	{
		$participant = $this->participant
							->with('event_participant')
							->with('company')
							->where('employee_number', $employee_number)
							->first();

		return $participant;
	}

	public function getLoggedInParticipant()
	{
		$id = Auth::user()->id;

		$participant = $this->participant
							->with('company')
							->find($id);

		return $participant;
	}

	public function updateParticipant($request, $id)
	{
		$participant = $this->participant
							->find($id)
							->update($request);

		$participant_info = $this->getParticipant($id);

		return $participant_info;
	}

	public function updateEventParticipant($request)
	{
		$participant = $this->event_participant
							->where('participant_id', $request['participant_id'])
							->update([
								'raffle_code' =>  $request['raffle_code'],
								'table_number' =>  $request['table_number'],
							]);

		$participant_info = $this->getParticipant($request['participant_id']);

		return $participant_info;
	}

	public function updateParticipantSentStatus($id, $status = 1)
	{
		$participant = $this->participant
							->find($id)
							->update(['is_email_sent' => $status]);

		$participant_info = $this->getParticipant($id);

		return $participant_info;
	}

	public function deleteParticipant($id)
	{
		$participant = $this->participant
							->find($id);

		$participant_info = $participant;

		$participant->delete();

		return $participant_info;
	}

	public function changePassword($request)
	{
		$participant = $this->participant
							->find($request['id']);


        if($request['password'] != "") {
			$participant->password = $request['password'];
		}
		
		$participant->status = 1;

		$participant->save();

		$participant_info = $this->getParticipant($request['id']);

		return $participant_info;
	}

	public function getParticipantViaQRID($qrid, $event_id)
	{
		$participant = $this->participant
							->with('company')
							->where('qrid', $qrid)
							->first();

		$event_participant = $this->event_participant
								  ->where('participant_id', $participant->id)
								  ->where('event_id', $event_id)
								  ->first();

		$participant->event_participant = $event_participant;

		return $participant;
	}
}
