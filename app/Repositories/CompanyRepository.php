<?php
namespace App\Repositories;

use App\Interfaces\CompanyInterface;

use App\Company;

use Auth;

class CompanyRepository implements CompanyInterface
{
	protected $company = null;

	public function __construct(Company $company)
	{
		$this->company = $company;
	}

	public function findCompany($id)
	{
		return $this->company->findOrFail($id);
	}

	public function deleteCompany($id)
	{
		return $this->company->findOrFail($id)
							 ->delete();
	}

	public function getCompanyViaSlug($slug)
	{
		$company = $this->company
						->where('slug', $slug)
						->first();

		return $company;
	}

	public function getAllCompanies()
	{
		$companies = $this->company
						->get();

		return $companies;
	}

	public function createCompany($request)
	{
		return $this->company->create($request);
	}

	public function updateCompany($request, $id)
	{
		return $this->company
						->findOrFail($id)
						->update($request);
	}
}
