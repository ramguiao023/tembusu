<?php

namespace App\Traits;

trait Response
{
	public function responseJson($resource, $message = "", $status = 200)
	{
		$json = ['data' => $resource, 'message' => $message];

		return response()
                ->json($json, $status);
	}
}

?>