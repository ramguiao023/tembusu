<?php

if(!function_exists('is_event_happening')) {
	function is_event_happening($start_date, $end_date = "")
	{
		if($end_date == "") {
			if(Carbon\Carbon::today() > Carbon\Carbon::parse($start_date)) {
				return "2";
			} else if(($start_date != "" or $start_date != null ) and Carbon\Carbon::parse($start_date) > Carbon\Carbon::today()) {
				return "0";
			} else if(($start_date != "" or $start_date != null ) and Carbon\Carbon::parse($start_date) == Carbon\Carbon::today()) {
				return "1";
			}
		} else {
			if(Carbon\Carbon::today()->between(Carbon\Carbon::parse($start_date), Carbon\Carbon::parse($end_date))) {
				return "1";
			}else if(Carbon\Carbon::today() > Carbon\Carbon::parse($start_date)) {
				return "2";
			} else if(Carbon\Carbon::parse($start_date) > Carbon\Carbon::today()) {
				return "0";
			}
		}
	}
}