<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public $guarded = [];

    public function getLogoAttribute($value)
    {
    	if($value) {
    		return asset('uploads/company/'.$value);
    	} else {
    		return asset('admin_assets/logo.png');
    	}
    }
}
