<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventParticipant extends Model
{
	public $table = "events_participants";
    public $guarded = [];

    public function event()
    {
    	return $this->hasOne('App\Event', 'id', 'event_id');
    }

    public function participant()
    {
    	return $this->hasOne('App\Participant', 'id', 'participant_id');
    }
}
