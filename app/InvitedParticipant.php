<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvitedParticipant extends Model
{
    public $guarded = [];

    public function participant()
    {
    	$this->hasOne('App\Participant', 'id', 'participant_id');
    }
}
