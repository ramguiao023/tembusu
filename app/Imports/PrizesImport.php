<?php

namespace App\Imports;

use App\Prize;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class PrizesImport implements ToCollection
{
    public $lucky_draw_id = null;

    public function __construct($lucky_draw_id)
    {
        $this->lucky_draw_id = $lucky_draw_id;
    }

    public function collection(Collection $rows)
    {
        foreach($rows as $row)
        {
            $prize = new Prize;

            $prize->name = $row[0];
            $prize->number_of_winners = $row[1];
            $prize->raffle_id = $this->lucky_draw_id;
            $prize->type = $row[2];

            $prize->save();
        }
    }
}
