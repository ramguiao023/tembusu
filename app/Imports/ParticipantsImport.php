<?php

namespace App\Imports;

use App\Participant;
use App\EventParticipant;

use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Validators\Failure;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsFailures;

class ParticipantsImport implements ToCollection, WithValidation, SkipsOnFailure
{

    use Importable, SkipsFailures;

    public $company_id = null;
    public $event_id = null;

    public function __construct($company_id, $event_id)
    {
        $this->company_id = $company_id;
        $this->event_id = $event_id;
    }

    /**
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        // Handle the failures how you'd like.
    }
     /**
     * @param \Throwable $e
     */
    public function onError(\Throwable $e)
    {
        // Handle the exception how you'd like.
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        $password = $this->generatePassword();

        $ctr_success = 0;
        $ctr_failed = 0;

        $ctr = [];

        foreach($rows as $row)
        {
            $check_participant = Participant::where('email', $row[5])
                                            ->orWhere('employee_number', $row[4])
                                            ->first();

            if($check_participant) {
                $participant = Participant::find($check_participant->id);

                $participant->salutation = $row[0];
                $participant->name = $row[1];
                $participant->birthdate = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[2]));
                $participant->company_id = $this->company_id;
                $participant->department = $row[3];
                $participant->employee_number = $row[4];
                $participant->email = $row[5];
                $participant->contact_number = $row[6];
                $participant->username = $this->generateUsername($row[1]);
                $participant->password = $password;
                $participant->temporary_password = $password;
                $participant->diet_preferences = $row[7];
                $participant->diet_notes = $row[8];
                $participant->is_vip = $row[9];
                $participant->qrid = $this->generateQRID();

                $participant->save();

                // Check Event Invitation
                $event_participant = EventParticipant::where('event_id', $this->event_id)
                                                        ->where('participant_id', $participant->id)
                                                        ->first();

                if($event_participant) {
                    $update_event_participant = EventParticipant::find($event_participant->id);

                    $update_event_participant->raffle_code = @$row[11];
                    $update_event_participant->table_number = @$row[10];

                    $update_event_participant->save();
                } else {
                    $new_event_participant = new EventParticipant;

                    $new_event_participant->event_id = $this->event_id;
                    $new_event_participant->participant_id = $participant->id;
                    $new_event_participant->raffle_code = @$row[11];
                    $new_event_participant->table_number = @$row[10];
                    $new_event_participant->status = 3;

                    $new_event_participant->save();
                }
            } else {
                $participant = new Participant;

                $participant->salutation = $row[0];
                $participant->name = $row[1];
                $participant->birthdate = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[2]));
                $participant->company_id = $this->company_id;
                $participant->department = $row[3];
                $participant->employee_number = $row[4];
                $participant->email = $row[5];
                $participant->contact_number = $row[6];
                $participant->username = $this->generateUsername($row[1]);
                $participant->password = $password;
                $participant->temporary_password = $password;
                $participant->diet_preferences = $row[7];
                $participant->diet_notes = $row[8];
                $participant->is_vip = $row[9];
                $participant->qrid = $this->generateQRID();

                $participant->save();

                $new_event_participant = new EventParticipant;

                $new_event_participant->event_id = $this->event_id;
                $new_event_participant->participant_id = $participant->id;
                $new_event_participant->raffle_code = @$row[11];
                $new_event_participant->table_number = @$row[10];
                $new_event_participant->status = 3;

                $new_event_participant->save();
            }
        }

        $ctr[] = $ctr_success;
        $ctr[] = $ctr_failed;

        return $ctr;
    }

    public function generateQRID()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 10; $i++) {
            $randstring = $characters[rand(0, strlen($characters)-1)];
        }

        return $randstring.uniqid().time();
    }

    private function generateUsername($name)
    {
        $random_string = '1234567890';
        
        $username = str_replace(' ', '_', $name) . substr(str_shuffle($random_string), 0, 3);

        $username = strtolower($username);

        $username = preg_replace('/[^a-zA-Z0-9_]/', '', $username);

        $participant = Participant::with('company')
                                    ->where('username', $username)
                                    ->first();

        if($participant) {
          $this->generateUsername($name);
        } else {
          return $username;
        }
    }

    public function rules(): array
    {
        return [
            'employee_number' => "unique:participants",
            'username' => "unique:participants",
        ];
    }

    private function generatePassword($chars = 8)
    {
        $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
        
        return substr(str_shuffle($data), 0, $chars);
    }
}
