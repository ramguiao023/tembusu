<?php

namespace App\Imports;

use App\Participant;
use App\EventParticipant;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class UpdateParticipant implements ToCollection
{
	public $event_id = null;

	public function __construct($event_id)
	{
		$this->event_id = $event_id;
	}

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
    	foreach($rows as $row) {
    		$employee_id = $row[4];
    		$salutation = $row[0];
    		$name = $row[1];
            $birthdate = $row[2];
            $email = $row[5];
            $contact_number = $row[6];
            $diet_preferences = $row[7];
            $diet_notes = $row[8];
            $is_vip = $row[9];
    		$table_number = $row[10];
        	$raffle_code = $row[11];

    		$participant = Participant::where('employee_number', $employee_id)
    								  ->first();
            
    		if($participant) {
    			$event_participant = EventParticipant::where('event_id', $this->event_id)
    												  ->where('participant_id', $participant->id)
    												  ->first();

                if($event_participant) {
                    $event_participant->table_number = $table_number;
                    $event_participant->raffle_code = $raffle_code;

                    $event_participant->save();
                }

    			$participant->salutation = $salutation;
    			$participant->name = $name;
                $participant->birthdate = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($birthdate));
                $participant->email = $email;
                $participant->contact_number = $contact_number;
                $participant->diet_preferences = $diet_preferences;
                $participant->diet_notes = $diet_notes;
                $participant->is_vip = $is_vip;

    			$participant->save();
    		}
    	}
    }
}
