<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prize extends Model
{
    public $guarded = [];

    public function winners()
    {
    	return $this->hasMany('App\Winner');
    }
}
