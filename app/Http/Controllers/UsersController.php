<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Traits\Response;

use App\Repositories\UserRepository;

use Auth;

use Image;

use App\Rules\IsImage;

class UsersController extends Controller
{

    use Response;

    protected $user = null;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return $request->user();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'password' => 'min:8'
        ]);

        $user = $this->user->updateProfile($request, $id);

        return $this->responseJson($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->user->deleteUser($id);
        
        return $this->responseJson($user);
    }

    /**
     * Get logged in user information
     *
     * @param none
     * @return JSON
     */
    public function getLoggedInUser()
    {
        return $this->responseJson(Auth::user());
    }

    /**
     * Upload Profile Picture
     *
     * @param none
     * @return JSON
     */
    public function updateProfilePicture(Request $request)
    {
        request()->validate([
            'profile_photo' => [new IsImage]
        ]);

        $id = Auth::user()->id;
        $profile_photo = $this->uploadImage($request->profile_photo, "profile-picture");

        $user = $this->user->updateProfilePicture($id, $profile_photo);

        return $this->responseJson($user);
    }

    /**
     * Upload product image
     *
     * @param Base64 Encoded Image and Path
     * @return image name
     *
     */
    private function uploadImage($image, $path)
    {
        $data = $image;

        $png_url = "profile-photo-".time().".jpg";

        $path = public_path() . "/uploads/" . $path . "/" . $png_url;

        Image::make(file_get_contents($data))->save($path); 

        return $png_url; 
    }
}
