<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\EventRepository;
use App\Repositories\RaffleRepository;
use App\Repositories\UserRepository;

use App\Imports\PrizesImport;
use Maatwebsite\Excel\Facades\Excel;

use PhpOffice\PhpPresentation\PhpPresentation;
use PhpOffice\PhpPresentation\IOFactory;
use PhpOffice\PhpPresentation\Style\Color;
use PhpOffice\PhpPresentation\Style\Alignment;


class LuckyDrawController extends Controller
{
	protected $event = null;
    protected $raffle = null;
	protected $user = null;

	public function __construct(EventRepository $event,
								RaffleRepository $raffle,
                                UserRepository $user)
	{
		$this->event = $event;
        $this->raffle = $raffle;
		$this->user = $user;
	}

    public function index($id)
    {
    	$event = $this->event
    				  ->getEvent($id);

    	$raffles = $this->raffle
    					->getRafflesByEventId($id);


    	return view('admin.luckydraw.index')
    						->with(compact('event', 'raffles'));
    }

    public function getClientDrawPage($event_id, $raffle_id, $prize_id)
    {
        $event = $this->event
                      ->getEvent($event_id);

        // get lucky draw prizes
        $raffle = $this->raffle
                       ->getRaffle($raffle_id, 1);

        $prize = $this->raffle
                      ->getPrize($prize_id);

        $winners = $this->raffle
                        ->getWinners($raffle_id, $prize_id);

        $ctr = count($raffle->prizes);
        $prize_number = count($raffle->prizes);

        foreach($raffle->prizes as $prize) {
            if($prize_id == $prize->id) {
                $prize_number = $ctr;
                // echo $prize_id . " = " . $prize->id . " -- " . $prize_number;
                
                break;
            } else {
                // echo $prize_id . " = " . $prize->id;

            }

            // echo "<br />";

            $ctr--;
        }

        // exit;

        $total_prizes = count($raffle->prizes);

        return view('admin.luckydraw.draw-client')
                        ->with(compact('event', 'raffle', 'prize', 'winners', 'prize_number', 'total_prizes'));

    }

    public function triggerDraw($event_id, $raffle_id, $prize_id, $luckydraw_number, $name, $employee_number)
    {
        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
        );

        $pusher = new \Pusher\Pusher(
            '0047d5efb8a4f3221be3',
            '04144d6b4a2386dc789f',
            '845403',
            $options
        );

        $data['message'] = json_encode([$luckydraw_number, $name, $employee_number]);
        $pusher->trigger("prize".$prize_id, 'my-event', $data);
    }

    public function generatePPT($event_id, $raffle_id)
    {
        $event = $this->event
                      ->getEvent($event_id);

        $winners = $this->raffle
                        ->getWinners($raffle_id);

        $objPHPPowerPoint = new PhpPresentation();

        // Create slide
        $currentSlide = $objPHPPowerPoint->getActiveSlide();

        // Create a shape (text)
        $shape = $currentSlide->createRichTextShape()
                              ->setHeight(300)
                              ->setWidth(800)
                              ->setOffsetX(130)
                              ->setOffsetY(180);

        $shape->getActiveParagraph()->getAlignment()->setHorizontal( Alignment::HORIZONTAL_LEFT );

        $textRun = $shape->createTextRun('Winners of ' . $event->title);
        $textRun->getFont()->setBold(true)
                           ->setSize(60)
                           ->setColor( new Color( 'FFE06B20' ) );

        $ctr = 10;
        $index_ctr = 1;
        $space = 100;

        $winner_ctr = 1;

        foreach($winners as $winner) {
            $oSlide = null;
            
            if($ctr % 10 == 0) {
                $oSlide = $objPHPPowerPoint->createSlide();
                // echo $index_ctr . "<br />";
                $objPHPPowerPoint->setActiveSlideIndex($index_ctr);
                $index_ctr++;
                $space = 100;
            } else {
                $oSlide = $objPHPPowerPoint->getActiveSlide();
            }

            $shape = $oSlide->createRichTextShape()
                            ->setHeight(300)
                            ->setWidth(800)
                            ->setOffsetX(130)
                            ->setOffsetY($space);

            $shape->getActiveParagraph()->getAlignment()->setHorizontal( Alignment::HORIZONTAL_LEFT );

        $textRun = $shape->createTextRun($winner_ctr . ") " . $winner->events_participants->participant->name . " - " . $winner->prize->name);
            $textRun->getFont()->setBold(true)
                           ->setSize(30)
                           ->setColor( new Color( '000' ) );

            $ctr++;
            $winner_ctr++;
            $space+=50;
        }

        try {
            $oWriterPPTX = IOFactory::createWriter($objPHPPowerPoint, 'PowerPoint2007');
            $oWriterPPTX->save("generated-ppt/".$event->title."-winners".".pptx");
            $oWriterODP = IOFactory::createWriter($objPHPPowerPoint, 'ODPresentation');
            $oWriterODP->save("generated-ppt/".$event->title."-winners".".odp");

            return redirect("/generated-ppt/".$event->title."-winners".".pptx");
        }
        catch (exception $e) {
            echo "Please make sure to close all of the PowerPoint Application.";
        }
    }

    public function getDraw($event_id, $raffle_id, $prize_id)
    {
        $event = $this->event
                      ->getEvent($event_id);

        // get lucky draw prizes
        $raffle = $this->raffle
                       ->getRaffle($raffle_id, 1);

        $prize = $this->raffle
                      ->getPrize($prize_id);

        $winners = $this->raffle
                        ->getWinners($raffle_id, $prize_id);

        $ctr = count($raffle->prizes);
        $prize_number = count($raffle->prizes);

        foreach($raffle->prizes as $prize) {
            if($prize_id == $prize->id) {
                $prize_number = $ctr;
                // echo $prize_id . " = " . $prize->id . " -- " . $prize_number;
                
                break;
            } else {
                // echo $prize_id . " = " . $prize->id;

            }

            // echo "<br />";

            $ctr--;
        }

        // exit;

        $total_prizes = count($raffle->prizes);

        return view('admin.luckydraw.draw')
                        ->with(compact('event', 'raffle', 'prize', 'winners', 'prize_number', 'total_prizes'));
    }

    public function getWinnersPage($event_id, $raffle_id)
    {
        $winners = $this->raffle->getWinners($raffle_id);
        $event = $this->event->getEvent($event_id);

        return view('admin.luckydraw.winners')
                            ->with(compact('winners','event', 'raffle_id'));
    }

    public function getPrize($event_id, $raffle_id)
    {
        $event = $this->event
                      ->getEvent($event_id);

        $raffle = $this->raffle
                       ->getRaffle($raffle_id, 1);

        
        foreach($raffle->prizes as $prize) {
            if(count($prize->winners) < $prize->number_of_winners) {
                return redirect('admin/events/'.$raffle->event_id . '/lucky-draw/' . $raffle->id . '/draw/' . $prize->id);
                
            }
        }

        $warning = "All prizes has already been won. You can select prize below if you want to re-draw.";

        return view('admin.luckydraw.prize')
                   ->with(compact('event', 'raffle', 'warning'));
    }

    public function create($id)
    {
    	$event = $this->event->getEvent($id);

        $users = $this->user
                      ->getUsersByCompany($event->company_id);

    	return view('admin.luckydraw.create')
    						->with(compact('event', 'users'));
    }

    public function edit($id, $raffle_id)
    {
        $event = $this->event->getEvent($id);

        $users = $this->user
                      ->getUsersByCompany($event->company_id);

        $raffle = $this->raffle
                       ->getRaffle($raffle_id);

        return view('admin.luckydraw.edit')
                                ->with(compact('event', 'users', 'raffle'));
    }

    public function store(Request $request)
    {
    	$raffle = $this->raffle->createRaffleWithPrizes($request->all());
        
        if(@$request['upload_manually'] != 1) {
            $result = array($request->file('xls')->getClientOriginalExtension());

            $file = $request->file('xls');

            $import_file_name = "import-".time().".".$result[0];

            $path = public_path() . "/uploads/imports/";

            $file->move($path, $import_file_name);

            Excel::import(new PrizesImport($raffle->id), 'uploads/imports/'.$import_file_name);
            
            return redirect("/admin/events/".$raffle->event_id."/lucky-draw")
                                    ->with('success', 'Created Successfully!');
        } else {
            return $raffle;
        }

    }

    public function update(Request $request, $id, $raffle_id)
    {
        $raffle = $this->raffle->updateRaffleWithPrizes($request->all(), $raffle_id);

        return $raffle;
    }

    public function getWinner($event_id)
    {
    	$event_participant = $this->event->getRandomEventsParticipantsPerEventId($event_id, 2);

    	return $event_participant;
    }

    public function claimPrize($winner)
    {
        $winner = $this->raffle->claimPrize($winner);

        return $winner;
    }

    public function deleteWinner($winner_id)
    {
        $winner = $this->raffle->deleteWinner($winner_id);

        return $winner;
    }

    public function getWinners($raffle_id, $prize_id)
    {
        $winners = $this->raffle
                        ->getWinners($raffle_id, $prize_id);

        return $winners;
    }

    public function getWinnersClaimed($raffle_id, $prize_id)
    {
        $winners = $this->raffle
                        ->getWinnersClaimed($raffle_id, $prize_id);

        return $winners;
    }

    public function saveWinner(Request $request)
    {
        $winner = $this->raffle
                       ->saveWinner($request->except(['_token']));

        return $winner;
    }

    public function getPreDraw($draw_id)
    {
        // get pre draw prizes
        $raffle = $this->raffle
                       ->getRaffle($draw_id, 2);

        return view('admin.luckydraw.pre-draw')
                            ->with(compact('raffle'));
    }
}
