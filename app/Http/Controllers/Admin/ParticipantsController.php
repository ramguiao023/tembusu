<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\ParticipantRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\DepartmentRepository;
use App\Repositories\EventRepository;

use App\Imports\ParticipantsImport;
use App\Imports\UpdateParticipant;

use Maatwebsite\Excel\Facades\Excel;

use App\Mail\CompleteRegistration;
use QrCode;
use Mail;

class ParticipantsController extends Controller
{
    protected $participant = null;
    protected $company = null;
    protected $event = null;
    protected $department = null;

    public function __construct(
                ParticipantRepository $participant,
                CompanyRepository $company,
                DepartmentRepository $department,
                EventRepository $event
            )
    {
        $this->participant = $participant;
        $this->company = $company;
        $this->department = $department;
        $this->event = $event;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $participants = $this->participant->getParticipants();
        
        return view('admin.participants.index')
                            ->with('participants', $participants);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($event_id)
    {
        $companies = $this->company
                          ->getAllCompanies();

        $event = $this->event
                          ->getEvent($event_id);

        return view('admin.participants.create')
                            ->with(compact('companies', 'event'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(
          [
            'employee_number' => 'required|unique:participants',
            'name' => 'required',
            'birthdate' => 'required',
            'company_id' => 'required',
            'table_number' => 'nullable',
            'raffle_code' => 'unique:events_participants|min:6|max:6|nullable',
            'email' => 'required|email|unique:participants',
          ],
          [
            'raffle_code.unique' => 'The lucky draw number should be unique. You\'ve entered a code that is already in used. '
          ]
        );

        // Add auto-generated password
        $password = $this->generatePassword();
        $username = $this->generateUsername($request->name);

        $request['username'] = $username;
        $request['password'] = $password;
        $request['temporary_password'] = $password;

        $participant = $this->participant->addParticipant($request);

        $event_participant_array = [
                                      'event_id' => $request->event_id,
                                      'participant_id' => $participant->id,
                                      'table_number' => $request->table_number,
                                      'raffle_code' => $request->raffle_code,
                                      'status' => 3
                                  ];

        $event_participant = $this->event->joinEvent($event_participant_array);

        // check if admin wants to send registration email to participant
        if(@$request->is_email_sent) {
            Mail::to($participant->email)->send(new CompleteRegistration($participant));
        }

        return redirect('/admin/events/'.$request->event_id.'/participants')
                            ->with('success', 'Saved Successfully.');
    }

    private function generateUsername($name)
    {
        $random_string = '1234567890';
        
        $username = str_replace(' ', '_', $name) . substr(str_shuffle($random_string), 0, 3);

        $username = strtolower($username);

        $username = preg_replace('/[^a-zA-Z0-9_]/', '', $username);

        $participant = $this->participant
                            ->getParticipantByUsername($username);

        if($participant) {
          $this->generateUsername($name);
        } else {
          return $username;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    public function getBulkEventPage($event_id)
    {
      $event = $this->event
                    ->getEvent($event_id);

      return view('admin.participants.bulk-event')
                        ->with(compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $event_id)
    {
        $event = $this->event->getEvent($event_id);

        $companies = $this->company
                          ->getAllCompanies();

        $participant = $this->participant->getParticipant($id);

        $diet_preferences = explode(",", $participant->diet_preferences);

        $departments = $this->department->getAllDepartmentsOfACompany($participant->company_id);

        return view('admin.participants.edit')
                            ->with(compact('companies', 'participant', 'departments', 'diet_preferences', 'event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDietPage($id)
    {
        $companies = $this->company
                          ->getAllCompanies();

        $participant = $this->participant->getParticipant($id);

        $diet_preferences = explode(",", $participant->diet_preferences);

        $departments = $this->department->getAllDepartmentsOfACompany($participant->company_id);

        return view('admin.participants.diet')
                            ->with(compact('companies', 'participant', 'departments', 'diet_preferences'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event_participant = $this->participant
                                  ->getEventParticipant($id);
        if($event_participant) {
            request()->validate(
              [
                'employee_number' => 'required',
                'name' => 'required',
                'email' => 'required|email|unique:participants,email,'.$id,
                'table_number' => 'nullable',
                'raffle_code' => 'nullable|min:6|max:6|unique:events_participants,raffle_code,'.$event_participant->id,
                'username' => 'required|min:4|alpha_dash|unique:participants,username,'.$id,
              ],
              [
                'raffle_code.unique' => "Lucky draw number has already been taken.",
                'raffle_code.max' => "Lucky draw number should not be greater than 6 characters.",
                'raffle_code.min' => "Lucky draw number must be at least 6 characters.",
              ]
          );
        } else {
            request()->validate(
              [
                'employee_number' => 'required',
                'name' => 'required',
                'email' => 'required|email|unique:participants,email,'.$id,
                'table_number' => 'required',
                'raffle_code' => 'unique:events_participants,raffle_code,'.$event_participant,
                'username' => 'required|min:4|alpha_dash|unique:participants,username,'.$id,
              ],
              [
                'raffle_code.unique' => "Lucky draw number has already been taken."
              ]
          );
        }
        

        $participant = $this->participant->updateParticipant($request->except(['_token', '_method', 'resent_email', 'type', 'participant_id', 'id', 'table_number', 'raffle_code', 'event_id']), $id);

        $request['participant_id'] = $participant->id;

        $event_participant = $this->participant->updateEventParticipant($request->all());

        // check if admin wants to send registration email to participant
        if(@$request->resent_email == 1 || @$request->is_email_sent) {
            Mail::to($participant->email)->send(new CompleteRegistration($participant));
        }

        return redirect('/admin/participants/' . $id . '/edit/'.$request->event_id)
                            ->with('success', 'Saved Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $participant = $this->participant->deleteParticipant($id);

        return redirect('admin/participants')
                                ->with('success', 'Deleted Successfully.');
    }

    public function showBulkUploadForm($event_id)
    {
      $companies = $this->company
                          ->getAllCompanies();

      $event = $this->event
                    ->getEvent($event_id);

      return view('admin.participants.bulk')
                                  ->with(compact('companies', 'event_id', 'event'));
    }

    private function generatePassword($chars = 8)
    {
        $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
        
        return substr(str_shuffle($data), 0, $chars);
    }

    public function updateImport(Request $request)
    {
      $extensions = array("xls","xlsx","xlm","xla","xlc","xlt","xlw", "csv");

        $result = array($request->file('participants')->getClientOriginalExtension());

        if(in_array($result[0],$extensions)){
            $file = $request->file('participants');

            $import_file_name = "import-".time().".".$result[0];

            $path = public_path() . "/uploads/imports/";

            $file->move($path, $import_file_name);

            // $participant_import = new updateParticipant();
            // $participant_import->import('uploads/imports/'.$import_file_name);

            Excel::import(new updateParticipant($request->event_id), 'uploads/imports/'.$import_file_name);

            return redirect('admin/events/'.$request->event_id.'/bulk')
                            ->with('success', 'Imported Successfully!');
        }else{
            return redirect('admin/events/'.$request->event_id.'/bulk')
                                    ->with('error', 'Invalid file format. Please import XLS, XLSX, and CSV only.');
        }
    }
    public function import(Request $request) 
    {
        request()->validate([
            'participants' => 'required',
            'company_id' => 'required'
        ]);

        $extensions = array("xls","xlsx","xlm","xla","xlc","xlt","xlw", "csv");

        $result = array($request->file('participants')->getClientOriginalExtension());

        if(in_array($result[0],$extensions)){
            $file = $request->file('participants');

            $import_file_name = "import-".time().".".$result[0];

            $path = public_path() . "/uploads/imports/";

            $file->move($path, $import_file_name);

            $participant_import = new ParticipantsImport($request->company_id, $request->event_id);
            $import_return = $participant_import->import('uploads/imports/'.$import_file_name);

            // Excel::import(new ParticipantsImport, 'uploads/imports/'.$import_file_name);

            return redirect('admin/participants/bulk-upload/'.$request->event_id)
                            ->with('success', 'Imported Successfully!');
        }else{
            return redirect('admin/participants/bulk-upload')
                                    ->with('error', 'Invalid file format. Please import XLS, XLSX, and CSV only.');
        }
    }

    public function updateProfile(Request $request)
    {
        $diet_preferences = "";

        if($request->diet_preferences) {
            $diet_preferences = implode(",", $request->diet_preferences);
        }

        $request['diet_preferences'] = $diet_preferences;

        $id = $request->participant_id;

        $participant = $this->participant->updateParticipant($request->except(['participant_id']), $id);
    
        return redirect('/admin/participants/' . $id . '/diet')
                            ->with('complete_success', 'Saved Successfully.');
    }

    public function deleteMany(Request $request)
    {
        foreach($request->participant_id as $participant_id) {
            $participant = $this->participant->deleteParticipant($participant_id);
        }

        return $participant;
    }

    public function sendToMany(Request $request)
    {
        foreach($request->participant_id as $participant_id) {
            $participant = $this->participant->getParticipant($participant_id);


            Mail::to($participant->email)->send(new CompleteRegistration($participant));

            $request = [
                'is_email_sent' => 1
            ];

            $participant = $this->participant->updateParticipantSentStatus($participant->id);
        }

        return $participant;
    }

    public function getQR($id)
    {
      $participant = $this->participant->getParticipant($id);

      return view('admin.participants.qr')
                          ->with(compact('participant'));
    }

    public function getParticipantViaQRID($qrid, $event_id)
    {
        $participant = $this->participant->getParticipantViaQRID($qrid, $event_id);

        return $participant;
    }
}
