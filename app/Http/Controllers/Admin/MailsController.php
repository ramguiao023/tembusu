<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\EventRepository;
use App\Repositories\MailRepository;

class MailsController extends Controller
{
    protected $event = null;
    protected $mail = null;

    public function __construct(EventRepository $event,
                                MailRepository $mail)
    {
        $this->event = $event;
        $this->mail = $mail;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $event = $this->event->getEvent($id);
        $mails = $this->mail->getMails($id);

        return view('admin.events.mails')
                            ->with(compact('event', 'mails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $event = $this->event->getEvent($id);

        return view('admin.events.create-mail')
                            ->with(compact('event'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'subject' => 'required',
            'content' => 'required'
        ]);

        $mail = $this->mail->saveMail($request->all());

        return redirect('admin/events/'.$request->event_id.'/mails')
                                            ->with('success', 'Saved successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $mail_id)
    {
        $event = $this->event->getEvent($id);

        $mail = $this->mail->getMail($mail_id);

        return view('admin.events.edit-mail')
                            ->with(compact('event', 'mail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $mail_id)
    {
        request()->validate([
            'name' => 'required',
            'subject' => 'required',
            'content' => 'required'
        ]);

        $mail = $this->mail->updateMail($request->all(), $mail_id);

        return redirect('admin/events/'.$request->event_id.'/mails/'.$mail_id)
                                            ->with('success', 'Saved successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $mail_id)
    {
        $mail = $this->mail->deleteMail($mail_id);
     
        return redirect('admin/events/'.$id.'/mails')
                                            ->with('success', 'Deleted successfully!'); 
    }
}
