<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\EventRepository;


class QuestionsController extends Controller
{
	protected $event;

	public function __construct(EventRepository $event)
	{
		$this->event = $event;
	}

	public function index($event_id)
	{
		$event = $this->event->getEvent($event_id);

		return view('admin.events.questions.index')
							->with(compact('event'));
	}

	public function store(Request $request)
	{
		$all = $request->all();

		$questions = $this->event->updateQuestion($all);
	}
}