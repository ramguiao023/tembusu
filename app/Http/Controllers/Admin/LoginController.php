<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class LoginController extends Controller
{
	/**
     * Display Login Page
     *
     * @return view HTML
     */
    public function index()
    {
    	if(Auth::check()){
		    return redirect()->intended('admin/dashboard');
    	}
    	return view('admin.login.login');
    }

    /**
     * Try to login user
     *
     * @param Request
     * @return view HTML
     */
    public function login(Request $request)
    {
    	$this->validate(
    			$request,
    			[
    				'email' => "required",
    				'password' => "required"
    			]
    	);

    	$email = $request->email;
    	$password = $request->password;
    	$remember_me = $request->has('remember') ? true : false;


    	if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 1], $remember_me)) {
    		$user = auth()->user();

    		if($remember_me) {
        		Auth::login($user->id, true);
    		}
		    
		    return redirect()->intended('admin/dashboard');
		} else {
            return redirect("/admin")->with('error', 'Invalid Login Credentials.');
        }
    }
    
    public function logout()
    {
    	Auth::logout();

    	return redirect('admin');
    }
}
