<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\VenueRepository;
use App\Repositories\CompanyRepository;

use App\Http\Requests\StoreVenue;
use App\Http\Requests\UpdateVenue;

class VenuesController extends Controller
{
	protected $venue;
	protected $company;

	public function __construct(VenueRepository $venue,
								CompanyRepository $company)
	{
		$this->venue = $venue;
		$this->company = $company;
	}

    public function index()
    {
    	$venues = $this->venue->getVenues();

    	return view('admin.venues.index')
    							->with(compact('venues'));
    }

    public function show($id)
    {
    	$companies = $this->company->getAllCompanies();

    	$venue = $this->venue->getVenue($id);

    	return view('admin.venues.edit')
    							->with(compact('companies', 'venue'));
    }

    public function create()
    {
    	$companies = $this->company->getAllCompanies();

    	return view('admin.venues.create')
    							->with(compact('companies'));
    }

    public function store(StoreVenue $request)
    {
    	$data = $request->except(['type', 'create']);

    	if($request->primary_image) {
    		$data['primary_image'] = $this->uploadImage($request->primary_image, 'venues');
    	}

    	if($request->street_map_image) {
    		$data['street_map_image'] = $this->uploadImage($request->street_map_image, 'venue_street');
    	}

    	if($request->indoor_map_image) {
    		$data['indoor_map_image'] = $this->uploadImage($request->indoor_map_image, 'venue_indoor');
    	}

    	$venue = $this->venue->addVenue($data);

    	return redirect('admin/venues')
    						->with('success', 'Saved Successfully.');
    }

    public function update(UpdateVenue $request, $id)
    {
    	$data = $request->except(['type', 'create', 'remove_street_image', 'remove_indoor_image']);

    	if($request->primary_image) {
    		$data['primary_image'] = $this->uploadImage($request->primary_image, 'venues');
    	}

    	if($request->remove_street_image) {
    		$data['street_map_image'] = null;
    	} else if($request->street_map_image) {
    		$data['street_map_image'] = $this->uploadImage($request->street_map_image, 'venue_street');
    	}

    	if($request->remove_indoor_image) {
    		$data['indoor_map_image'] = null;
    	} else if($request->indoor_map_image) {
    		$data['indoor_map_image'] = $this->uploadImage($request->indoor_map_image, 'venue_indoor');
    	}

    	$venue = $this->venue->editVenue($data, $id);

    	return redirect('admin/venues/'.$id)
    						->with('success', 'Saved Successfully.');
    }

    public function destroy($id)
    {
    	$venue = $this->venue->deleteVenue($id);

    	return redirect('admin/venues/')
    						->with('success', 'Deleted Successfully.');
    }

    /**
     * Upload events photo
     *
     * @param Image and Path
     * @return image name
     *
     */
    private function uploadImage($image, $path)
    {
        $image_url = "venue-".time().uniqid().".jpg";

        $path = public_path() . "/uploads/" . $path;

        $image->move($path, $image_url);

        return $image_url; 
    }
}
