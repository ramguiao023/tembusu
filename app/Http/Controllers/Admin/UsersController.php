<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;

use Auth;

class UsersController extends Controller
{
    protected $user = null;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMyProfile()
    {
        $id = Auth::user()->id;

        $user = $this->user->getAUser($id);

        return view('admin.users.profile')
                            ->with(compact('user'));
    }

    /**
     * Save User Profile
     *
     * @param Request
     * @return redirect
     */
    public function saveProfile(Request $request)
    {
        if($request->password) {
            request()->validate(
                [
                    'first_name' => "required",
                    'last_name' => "required",
                    'password' => "min:8",
                    'profile_photo' => "image|mimes:jpeg,png,jpg,gif,svg|max:2048"
                ]
            );
        } else {
            request()->validate(
                [
                    'first_name' => "required",
                    'last_name' => "required",
                    'profile_photo' => "image|mimes:jpeg,png,jpg,gif,svg|max:2048"
                ]
            );
        }

        if($request->profile_photo) {
            $image = $request->file('profile_photo');

            $path = $this->uploadImage($image, 'profile-picture');
            
            $request['profile_photo_path'] = $path;
        }

        $user = $this->user->updateMyProfile($request);

        return redirect('admin/my-profile')
                                ->with('success', 'Saved Successfully.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->user->getAllUsers();

        return view('admin.users.index')
                            ->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->user->getAllRoles();

        return view('admin.users.create')
                        ->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'role' => 'required',
                'email' => 'required|email|unique:users',
                'password' => "required|min:8",
            ]
        );

        $user = $this->user->createUser($request);

        return redirect('admin/users')
                            ->with('success', 'User created successfully!');
    }

    /**
     * Show form for Permission to Role
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addPermissionToRole($id)
    {
        $role = $this->user->getRole($id);

        $current_permissions = $this->user->getPermissionsPerRole($id);

        $current_permissions_id = [];

        foreach($current_permissions as $permission) {
            $current_permissions_id[] = $permission->id;
        }

        $permissions = $this->user->getAllPermissions($id);

        return view('admin.users.add-permission-to-role')
                                ->with('role', $role)
                                ->with('current_permissions', $current_permissions)
                                ->with('permissions', $permissions)
                                ->with('current_permissions_id', $current_permissions_id);
    }

    /**
     * Assign Permission to Role
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function assignPermissionToRole($permission_id, $role_id)
    {
        $permission = $this->user->assignPermissionToRole($permission_id, $role_id);

        return $permission;
    }

    /**
     * Revoke Permission to Role
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function revokePermissionToRole($permission_id, $role_id)
    {
        $permission = $this->user->revokePermissionToRole($permission_id, $role_id);

        return $permission;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->user->getAUser($id);

        
        $roles = $this->user->getAllRoles();

        return view('admin.users.edit')
                        ->with('user', $user)
                        ->with('roles', $roles);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->password) {
            request()->validate(
                [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'role' => 'required',
                    'email' => 'required|email|unique:users,email,'.$id,
                    'password' => "min:8",
                ]
            );
        } else {
            request()->validate(
                [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'role' => 'required',
                    'email' => 'required|email|unique:users,email,'.$id,
                ]
            );
        }
        
        $user = $this->user->updateMyProfile($request);

        return redirect('admin/users/'.$id)
                            ->with('success', 'User updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $user = $this->user->getAUser($id);

       $user->delete();

       return redirect('admin/users')
                            ->with('success', 'User deleted successfully.');
    }

    /**
     * Upload user photo
     *
     * @param Base64 Encoded Image and Path
     * @return image name
     *
     */
    private function uploadImage($image, $path)
    {
        $image_url = "profile-photo-".time().".jpg";

        $path = public_path() . "/uploads/" . $path;

        $image->move($path, $image_url);

        return $image_url; 
    }

    /**
     * Show Roles Listing Page
     *
     * @return view
     *
     */
    public function getRoles()
    {
        $roles = $this->user->getAllRoles();

        return view('admin.users.roles')
                            ->with('roles', $roles);
    }

    /**
     * Show Permissions Listing Page
     *
     * @return view
     *
     */
    public function getPermissions()
    {
        $permissions = $this->user->getAllPermissions();
        return view('admin.users.permissions')
                            ->with('permissions', $permissions);
    }

    /**
     * Get single role
     *
     * @return view
     *
     */
    public function getRole($id)
    {
        $role = $this->user->getRole($id);

        return view('admin.users.role')
                            ->with('role', $role);
    }

    /**
     * Update role data
     *
     * @param $request, $id int
     * @return view
     *
     */
    public function updateRole(Request $request, $id)
    {
        request()->validate(
            [
                'display_name' => 'required',
                'description' => 'required'
            ]
        );

        $this->user->updateRole($request, $id);

        return redirect('admin/users/roles/'.$id)
                                    ->with('success', 'Saved Successfully!');
    }

    /**
     * Create Role Form
     *
     * @return view
     *
     */
    public function getCreateRole()
    {
        return view('admin.users.add-role');
    }

    /**
     * Create Permission Form
     *
     * @return view
     *
     */
    public function getCreatePermission()
    {
        return view('admin.users.add-permission');
    }

    /**
     * Get Single Permission Form
     *
     * @return view
     *
     */
    public function getPermission($id)
    {
        $permission = $this->user->getPermission($id);

        return view('admin.users.permission')
                                ->with('permission', $permission);
    }

    /**
     * Save Role
     *
     * @return view
     *
     */
    public function saveRole(Request $request)
    {
        request()->validate(
            [
                'name' => 'required|unique:roles|alpha_dash',
                'display_name' => 'required',
                'description' => 'required'
            ]
        );

        $this->user->saveNewRole($request);

        return redirect('admin/users/roles')
                                    ->with('success', 'Saved Successfully!');
    }

    /**
     * Save Permission
     *
     * @return view
     *
     */
    public function savePermission(Request $request)
    {
        request()->validate(
            [
                'name' => 'required|unique:roles|alpha_dash',
                'display_name' => 'required',
                'description' => 'required'
            ]
        );

        $this->user->saveNewPermission($request);

        return redirect('admin/users/permissions')
                                    ->with('success', 'Saved Successfully!');
    }

    /**
     * Update Permission
     *
     * @return view
     *
     */
    public function updatePermission(Request $request, $id)
    {
        request()->validate(
            [
                'display_name' => 'required',
                'description' => 'required'
            ]
        );

        $this->user->updatePermission($request, $id);

        return redirect('admin/users/permissions/' . $id)
                                    ->with('success', 'Saved Successfully!');
    }


    /**
     * Delete role
     *
     * @param $id int
     * @return redirect
     *
     */
    public function deleteRole($id)
    {
        $role = $this->user->deleteRole($id);

        return redirect('admin/users/roles')
                                    ->with('success', 'Deleted Successfully!');
    }

    /**
     * Delete permission
     *
     * @param $id int
     * @return redirect
     *
     */
    public function deletePermission($id)
    {
        $role = $this->user->deletePermission($id);

        return redirect('admin/users/permissions')
                                    ->with('success', 'Deleted Successfully!');
    }
}
