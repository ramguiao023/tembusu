<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\StoreEvents;
use App\Http\Requests\UpdateEvents;
use App\Winner;

use App\Jobs\SendEmails;
use App\Jobs\EmailReminder;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ParticipantsOfEvents;
use App\Repositories\EventRepository;
use App\Repositories\VenueRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\ParticipantRepository;
use App\Repositories\InvitedParticipantRepository;
use App\Repositories\RaffleRepository;

use Auth;

class EventsController extends Controller
{
	protected $event;
    protected $company;
	protected $venue;
    protected $invited_participant;
    protected $raffle;

    public $types = ['Community', 'D&D', 'Organization'];

	public function __construct(
							EventRepository $event,
                            CompanyRepository $company,
                            VenueRepository $venue,
							ParticipantRepository $participant,
                            InvitedParticipantRepository $invited_participant,
                            RaffleRepository $raffle
						)
	{
        $this->raffle = $raffle;
        $this->participant = $participant;
		$this->event = $event;
        $this->company = $company;
		$this->venue = $venue;
        $this->invited_participant = $invited_participant;
	}

    public function index()
    {
    	$events = $this->event->getEvents();

    	return view('admin.events.index')
    							->with(compact('events'));
    }

    public function eventChecker($event_id, $participant_id)
    {
        $event = $this->event->getEvent($event_id);

        // if via username and password
        if($event->access_type == 1) {
            
            return redirect(url('/corp/'.$event->id. "/" . $event->company->slug));

        // if via info verification
        } else if($event->access_type == 2) {
            
            return redirect(url('/corp/'.$event->company->slug . '/' . $event_id . '/' . 'verification'));
        // if via just link
        } else if($event->access_type == 3) {
            
            return redirect(url('/corp/'.$event->company->slug . '/' . $participant_id . '/' . $event_id . "/" . 'login-via-id'));
        }
    }

    public function create()
    {
    	$companies = $this->company->getAllCompanies();

    	return view('admin.events.create')
    						->with(compact('companies'))
                            ->with('types', $this->types);	
    }

    public function show($id)
    {
        $companies = $this->company->getAllCompanies();
        $event =  $this->event->getEvent($id);

        return view('admin.events.edit')
                                ->with(compact('companies', 'event'))
                                ->with('types', $this->types);  
    }

    public function getRemindEmailPage($id, $company_id)
    {
        $companies = $this->company->getAllCompanies();
        $event = $this->event->getEvent($id);
        $company = $this->company->findCompany($company_id);
        $participants = $this->participant->getInvitedParticipants($id);


        return view('admin.events.send-reminder')
                                ->with(compact(
                                        'companies',
                                        'event',
                                        'company_id',
                                        'participants',
                                        'company'
                                    )
                                );
    }

    public function getEventImagesPage($id)
    {
        $companies = $this->company->getAllCompanies();
        $event =  $this->event->getEvent($id);

        return view('admin.events.image')
                                ->with(compact('companies', 'event'));
    }

    public function getEventVenuePage($id)
    {
        $companies = $this->company->getAllCompanies();
        $event =  $this->event->getEvent($id);
        $venues = $this->venue->getVenues();

        return view('admin.events.venue')
                                ->with(compact('companies', 'event', 'venues'));
    }

    public function getEventContactsPage($id)
    {
        $companies = $this->company->getAllCompanies();
        $event =  $this->event->getEvent($id);

        return view('admin.events.contacts')
                                ->with(compact('companies', 'event'));
    }
    
    public function participantDownload($id)
    {
        return Excel::download(new ParticipantsOfEvents($id), 'participants-'.date('Y-m-d').'.csv');
    }

    public function getParticipants($id)
    {
        $participants = $this->event->getParticipantsOfAnEvent($id);
        
        $event = $this->event->getEvent($id);

        return view('admin.events.participants')
                                ->with(compact('participants', 'event'));
    }

    public function jjssoonn()
    {
        $this->event->createDefaultMails(1);
    }

    public function store(StoreEvents $request)
    {
    	$data = $request->except(['create']);

    	$event = $this->event->createEvent($data);

    	return redirect('admin/events')
    							->with('success', 'Saved Successfully.');
    }

    public function updateContacts(Request $request, $event_id)
    {
        $events = $this->event->deleteEventContacts($event_id);
        $event = $this->event->updateEventContacts($request->all(), $event_id);

        return $event;
    }

    public function update(StoreEvents $request, $id)
    {
        $data = $request->except(['_token', '_method', 'create', 'start_time_display', 'end_time_display', 'type', 'event_id']);

        $event = $this->event->updateEvent($data, $id);

        $redirect_type = "";

        if($request->type == "venue") {
            $redirect_type = "venue";
        } else if($request->type == "contact") {
            $redirect_type = "contacts";
        }

        return redirect('admin/events/'.$id."/".$redirect_type)
                            ->with('success', 'Saved Successfully!');
    }

    public function updateImages(StoreEvents $request, $id)
    {
        $data = $request->except(['_token', '_method', 'create', 'start_time_display', 'end_time_display', 'type']);

        if($request->wallpaper) {            
            $wallpaper_path = $this->uploadImage($request->wallpaper, "events");
            $data['wallpaper'] = $wallpaper_path;
        }

        if($request->banner_image) {            
            $banner_image_path = $this->uploadImage($request->banner_image, "events");
            $data['banner_image'] = $banner_image_path;
        }

        $event = $this->event->updateEvent($data, $id);

        return redirect('admin/events/'.$id.'/images')
                                    ->with('success', 'Saved Successfully!');
    }

    public function disable($id)
    {
        $event = $this->event->setStatus($id, 0);

        return redirect('admin/events/')
                                ->with('success', 'Disabled Successfully.');
    }

    public function activate($id)
    {
        $event = $this->event->setStatus($id, 1);

        return redirect('admin/events/')
                                ->with('success', 'Activated Successfully.');
    }

    public function destroy($id)
    {
        $event = $this->event->deleteEvent($id);

        return redirect('admin/events/')
                                ->with('success', 'Deleted Successfully.');
    }

    /**
     * Show Invite Page
     * @param  int $id - event id
     * @return view
     */
    public function getInvitePage($id)
    {
        $companies = $this->company->getAllCompanies();
        $event = $this->event->getEvent($id);

        return view('admin.events.invite')
                                ->with(compact('companies', 'event'));
    }

    public function getInviteCompanyPage($id, $company_id)
    {
        $companies = $this->company->getAllCompanies();
        $event = $this->event->getEvent($id);
        $company = $this->company->findCompany($company_id);
        $participants = $this->event->getParticipantsOfAnEvent($id);

        return view('admin.events.invite-company')
                                ->with(compact(
                                        'companies',
                                        'event',
                                        'company_id',
                                        'participants',
                                        'company'
                                    )
                                );
    }

    public function sendReminder(Request $request)
    {
        request()->validate(
                    [
                        'event_id' => 'required',
                        'company_id' => 'required',
                    ]
        );
        
        $data = [];

        if($request->send_to_company_wide) {
            $all_employees = $this->participant->getInvitedParticipants($request->event_id);
            foreach($all_employees as $employee) {
                $data[] = $this->buildDataFromObject($request->event_id, $request->company_id, $employee->id);
            }
        } else {
            $data = $this->buildData($request->event_id, $request->company_id, $request->participants);
        }

        if(count($data) <= 0) {
            return redirect('admin/events/'.$request->event_id."/remind/".$request->company_id)
                                ->with('error', 'There are no participants selected. Please make sure to select participants below.');
        }

        EmailReminder::dispatch($data)->delay(now()->addSeconds(5));

        $event = $this->event->getEvent($request->event_id);

        return redirect('admin/events')
                            ->with('success', 'Event reminder ' . '(' . $event->title . ')' . ' has been successfully sent.');
    }

    public function invite(Request $request)
    {

        request()->validate(
                    [
                        'event_id' => 'required',
                        'company_id' => 'required',
                    ]
        );
        
        $data = [];

        if($request->send_to_company_wide) {
            $all_employees = $this->participant->getParticipantsPerCompanyId($request->company_id);
            foreach($all_employees as $employee) {
                $data[] = $this->buildDataFromObject($request->event_id, $request->company_id, $employee->id);
            }
        } else {
            $data = $this->buildData($request->event_id, $request->company_id, $request->participants);
        }

        if(count($data) <= 0) {
            return redirect('admin/events/'.$request->event_id."/invite/".$request->company_id)
                                ->with('error', 'There are no participants in your invite. Please make sure to select participants below.');
        }



        // $participants = $this->invited_participant->inviteParticipants($data);
        
        SendEmails::dispatch($data)->delay(now()->addSeconds(5));

        $event = $this->event->getEvent($request->event_id);

        return redirect('admin/events')
                            ->with('success', 'Event invitation ' . '(' . $event->title . ')' . ' has been successfully sent.');
    }

    public function buildDataFromObject($event_id, $company_id, $participant_id)
    {
        $data = [];

        return [
            'event_id' => $event_id,
            'company_id' => $company_id,
            'participant_id' => $participant_id,
            'status' => 0
        ];
    }
    
    public function buildData($event_id, $company_id, $participants)
    {
        $data = [];

        if($participants) {
            foreach($participants as $participant) {
                $data[] = [
                    "event_id" => $event_id,
                    "company_id" => $company_id,
                    "participant_id" => $participant,
                    "status" => 0,
                ];
            }
        }

        return $data;
    }

    public function getKioskPage($event_id)
    {
        $participant_id = @$_GET['participant_id'];
        
        $participant = $this->participant->getParticipant($participant_id);

        $event = $this->event->getEvent($event_id);

        return view('admin.events.kiosk')
                                ->with(compact('event_id','participant', 'event'));
    }

    public function checkIn($event_id, $participant_id)
    {
        $event_participant = $this->event->checkIn($event_id, $participant_id);

        return $event_participant;
    }

    public function preDraw(Request $request)
    {
        $event_id = $request->event_id;
        $raffle_id = $request->raffle_id;
        $prize_id = $request->prize_id;
        $number_of_draws = $request->number_of_draws;

        $event_participant = $this->event->getRandomEventsParticipantsPerEventIdMultiple($event_id, 2, $number_of_draws);

        foreach($event_participant as $participant) {
            $winner = new Winner();

            $winner->events_participants_id = $participant->id;
            $winner->raffle_id = $raffle_id;
            $winner->prize_id = $prize_id;

            $winner->save();
        }

        $winners = $this->raffle->getWinners($raffle_id, $prize_id);

        return $winners;
    }

    public function reDraw(Request $request)
    {
        $new_winner = [];

        $new_winner = $this->event->getRandomEventsParticipantsPerEventIdMultiple($request->event_id, 2, 1);

        $redraw_winner = $this->raffle
                              ->deleteWinner($request->winner_id);

        if(count($new_winner) <= 0) {
            $new_winner = $this->event->getRandomEventsParticipantsPerEventIdMultiple($request->event_id, 2, 1);
        }

        $save_winner = $this->raffle
                       ->saveWinner([
                            'events_participants_id' => $new_winner[0]->id,
                            'raffle_id' => $request->raffle_id,
                            'prize_id' => $request->prize_id,
                       ]);

        $winner = $this->raffle->getWinner($save_winner->id);

        return $winner;
    }

    /**
     * Upload events photo
     *
     * @param Image and Path
     * @return image name
     *
     */
    private function uploadImage($image, $path)
    {
        $image_url = "events-".time().uniqid().".jpg";

        $path = public_path() . "/uploads/" . $path;

        $image->move($path, $image_url);

        return $image_url; 
    }
}
