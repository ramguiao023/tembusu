<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\CompanyRepository;
use App\Repositories\CountryRepository;
use App\Repositories\BusinessTypeRepository;

class CompaniesController extends Controller
{
    protected $company;
    protected $country;
    protected $business_type;

    public function __construct(
                CompanyRepository $company,
                CountryRepository $country,
                BusinessTypeRepository $business_type)
    {
        $this->company = $company;
        $this->country = $country;
        $this->business_type = $business_type;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = $this->company->getAllCompanies();

        return view('admin.companies.index')
                                ->with(compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = $this->country->fetchCountries();
        $types = $this->business_type->fetchBusinessTypes();

        return view('admin.companies.create')
                                ->with(compact('countries', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'slug' => 'required|unique:companies|alpha_dash',
            'address' => 'required',
            'country' => 'required',
            'type' => 'required',
            'mobile_number' => 'required',
            'contact_person' => 'required',
            'contact_email' => 'required|email',
            'contact_number' => 'required'
        ], [
            'name.required' => "The business name is required.",
            'address.required' => 'The businness address is required',
            'type.required' => 'Please choose a business type.',
            'mobile_number.required' => 'Please indicate business contact number.',
            'contact_email.required' => 'Please indicate the email address of contact person',
            'contact_number.required' => 'Please indicate the mobile/telephone number of contact person',
        ]);

        $company = $this->company->createCompany($request->all());

        return redirect('admin/companies')
                                ->with('success', "Saved Successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = $this->company->findCompany($id);
        $countries = $this->country->fetchCountries();
        $types = $this->business_type->fetchBusinessTypes();

        return view('admin.companies.edit')
                                ->with(compact('company', 'countries', 'types'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name' => 'required',
            'slug' => 'required|alpha_dash|unique:companies,id,'.$id,
            'address' => 'required',
            'country' => 'required',
            'type' => 'required',
            'mobile_number' => 'required',
            'contact_person' => 'required',
            'contact_email' => 'required|email',
            'contact_number' => 'required'
        ], [
            'name.required' => "The business name is required.",
            'address.required' => 'The businness address is required',
            'type.required' => 'Please choose a business type.',
            'mobile_number.required' => 'Please indicate business contact number.',
            'contact_email.required' => 'Please indicate the email address of contact person',
            'contact_number.required' => 'Please indicate the mobile/telephone number of contact person',
        ]);

        $company = $this->company->updateCompany($request->all(), $id);

        return redirect('admin/companies/'.$id)
                                ->with('success', "Saved Successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = $this->company->deleteCompany($id);

        return redirect('admin/companies')
                                ->with('success', "Deleted Successfully.");
    }
}
