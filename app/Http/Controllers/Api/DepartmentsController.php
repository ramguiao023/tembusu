<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\DepartmentRepository;

use App\Traits\Response;

class DepartmentsController extends Controller
{
	use Response;

	protected $department = null;

	public function __construct(DepartmentRepository $department)
	{
		$this->department = $department;
	}
    public function getDepartmentsOfACompany($company_id)
    {
    	$departments = $this->department->getAllDepartmentsOfACompany($company_id);

    	return $this->responseJson($departments);
    }
}
