<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CompanyRepository;

use App\Traits\Response;

class CompaniesController extends Controller
{
	use Response;

	protected $company = null;

	public function __construct(CompanyRepository $company)
	{
		$this->company = $company;
	}

    public function show($slug)
    {
    	$company = $this->company->getCompanyViaSlug($slug);

    	return $this->responseJson($company);
    }

    public function showById($id)
    {
    	$company = $this->company->findCompany($id);

    	return $this->responseJson($company);
    }
}
