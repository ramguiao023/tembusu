<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\ParticipantRepository;

use App\Traits\Response;
use App\Http\Requests\Verification;

use Image;

class ParticipantsController extends Controller
{
	use Response;
	
	protected $participant = null;

	public function __construct(ParticipantRepository $participant)
	{
		$this->participant = $participant;
	}

    public function verify(Verification $request)
    {
        $participant = $this->participant->verifyParticipant($request);

        return $this->responseJson($participant);
    }

    public function show($id, $event_id)
    {
    	$participant = $this->participant->getParticipant($id, $event_id);
    	
    	return $this->responseJson($participant);
    }

    public function showViaEmployeeNumber($employee_number)
    {
        $participant = $this->participant->getParticipantViaEmployeeNumber($employee_number);
        
        return $this->responseJson($participant);
    }

    public function update(Request $request, $id)
    {
        if($request->type == "info") {
            request()->validate([
                'name' => 'required',
            ]);
        }
        
        $participant = $this->participant->updateParticipant($request->except(['type']), $id);

        return $this->responseJson($participant);
    }

    public function getLoggedInParticipantData()
    {
    	$participant = $this->participant->getLoggedInParticipant();
    	
    	return $this->responseJson($participant);
    }

    public function changePassword(Request $request)
    {
        if($request->password != "") {
            request()->validate(
                [
                    'password' => 'min:8|required_with:password_confirmation|same:password_confirmation',
                    ''
                ]
            );
        }

        $participant = $this->participant->changePassword($request->all());

        return $this->responseJson($participant);
    }

    public function updateProfilePhoto(Request $request)
    {
        $path = $this->uploadImage($request->profile_image, 'profile-picture');

        return $path;
    }

    /**
     * Upload profile image
     *
     * @param Base64 Encoded Image and Path
     * @return image name
     *
     */
    private function uploadImage($image, $path)
    {
        $data = $image;

        $png_url = "profile-".time().".jpg";

        $path = public_path() . "/uploads/" . $path . "/" . $png_url;

        Image::make(file_get_contents($data))->save($path); 

        return $png_url; 
    }

    /**
     * Log out current logged in user
     *
     * @param  none
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     *  Get current company of the logged in participannt
     *  
     *  @param  $id participant id
     *  @return JSON
     */
    public function getCurrentCompany($id)
    {
        $participant = $this->participant->getParticipant($id);

        return $this->responseJson($participant);
    }
}
