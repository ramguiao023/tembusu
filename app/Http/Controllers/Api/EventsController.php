<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Response;
use App\Http\Requests\StoreRsvp;
use App\Repositories\EventRepository;
use App\Repositories\ParticipantRepository;

class EventsController extends Controller
{
	use Response;

    protected $event = null;
	protected $participant = null;

	public function __construct(EventRepository $event,
                                ParticipantRepository $participant)
	{
        $this->event = $event;
		$this->participant = $participant;
	}

    /**
     * Show event single informatuon
     * @param  int $id event id
     * @return json     event information
     */
    public function show($id)
    {
    	$event = $this->event
    				  ->getEvent($id);

    	return $this->responseJson($event);
    }

    /**
     * Join Event (RSVP)
     * @param  StoreRsvp $request validation for Rsvp data
     * @return json             participants and events data
     */
    public function store(StoreRsvp $request)
    {
        $event_participant_data = [
            'status' => 1,
            'event_id' => $request->event_id,
            'participant_id' => $request->id,
            'note' => ''
        ];

        $participant_data = [
            'name' => $request->name,
            'diet_preferences' => $request->diet_preferences,
            'diet_notes' => $request->diet_notes,
        ];

        $join_event = $this->event->joinEvent($event_participant_data);

        $participant = $this->participant->updateParticipant($participant_data, $request->id);

        return $this->responseJson($participant);
    }

    public function answerEvent(Request $request)
    {
        $answer = $this->event->answerQuestion($request->all());

        return $this->responseJson($answer);
    }

    /**
     * decline event (RSVP)
     * @param  Request $request rsvp data
     * @return json           participants and event data
     */
    public function declineEvent(Request $request)
    {
        $decline_event = $this->event->joinEvent($request->all());
    }
}
