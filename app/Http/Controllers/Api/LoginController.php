<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Response;

use App\Participant;

use DB;

class LoginController extends Controller
{
	use Response;

	public function login($id)
	{
		$participant = Participant::find($id);

		$password = $participant->password;
		$participant->password = $participant->temporary_password;
		$participant->save();

		$oauth_client = DB::table('oauth_clients')
							->where('id', '2')
							->first();

		$http = new \GuzzleHttp\Client;

		$url = "";

		if(strpos($_SERVER['HTTP_HOST'], "test")) {
			$url = "http://tembusu.test/";
		} else {
			$url = "https://legendrotor.com/";
		}

	    $response = $http->post($url . 'oauth/token', [
	        'form_params' => [
	            'grant_type' => 'password',
			'client_secret' => $oauth_client->secret,
			'client_id' => 2,
			'username'   => $participant->username,
			'password' => $participant->temporary_password
	        ],
	    ]);

		$participant = Participant::find($id);
		$participant->ignoreMutator = true;
	    $participant->password = $password;
		$participant->save();

	    return json_decode((string) $response->getBody(), true);
	}
}