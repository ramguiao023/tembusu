<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEvents extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        if($this->input('type') == 'images') {
            // $rules = [
            //         'wallpaper' => 'mimes:jpeg,jpg,png,gif|max:5000|dimensions:width=1080,height=768',
            //         'banner_image' => 'mimes:jpeg,jpg,png,gif|max:5000|dimensions:width=600,height=400',
            //     ];
            $rules = [
                    'wallpaper' => 'mimes:jpeg,jpg,png,gif|max:5000',
                    'banner_image' => 'mimes:jpeg,jpg,png,gif|max:5000',
                ];
        } else if($this->input('type') == 'venue') {
            $rules = [
                ];
        } else if($this->input('type') == 'contact') {
            $rules = [
                    'contact_person' => 'required',
                    'contact_person_number' => 'required',
                    'contact_person_email' => 'required|email',
                ];
        } else if($this->input('type') == 'editbasic') {
            $rules = [
                    'start_date' => 'required',
                    'end_date' => 'required',
                    'registration_closing_date' => 'required',
                    'title' => 'required',
                    'caption' => 'required',
                    'write_up' => 'required',
                    'company_id' => 'required',
                    'created_by' => 'required'
                ];
        } else {
            $rules = [
                    'title' => 'required',
                    'caption' => 'required',
                    'write_up' => 'required',
                    'event_type' => 'required',
                    'start_date' => 'required',
                    'end_date' => 'required',
                    'registration_closing_date' => 'required',
                    'company_id' => 'required',
                    'created_by' => 'required'
                ];
        }


        return $rules;
    }

    /**
     * Set validation error message
     *
     * @return array
     */

    public function messages()
    {
        return [
            'title.required' => 'Please specify the event title.',
            'caption.required' => 'Please include event caption',
            'write_up.required' => 'Please include event write-up',
            'company_id.required' => 'Please select a company',
            'banner_image.dimensions' => 'Please make sure to upload a 600x400 image for the banner image',
            'wallpaper.dimensions' => 'Please make sure to upload a 1080x768 image for the wallpaper',
        ];
    }
}
