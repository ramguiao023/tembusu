<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateVenue extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'address' => 'required',
            'lng' => 'required',
            'lat' => 'required',
            'primary_image' => 'mimes:jpeg,jpg,png,gif|max:5000'
        ];
    }

    public function messages()
    {
        return [
            'lng.required' => 'Please select a location the google map in order to have a longitude.',
            'lat.required' => 'Please select a location the google map in order to have a latitude.',
        ];
    }
}
