var el = document.querySelector('.odometer');

od = new Odometer({
  el: el,
  value: 100000,
  duration: 10000,
  // Any option (other than auto and selector) can be passed in here
  format: '',
});

$(".odometer-value").text(0);


$(document).on('click', '.show-next-button', function(data) {
	var self = $(this),
		raffle_id = self.data('raffleid'),
		prize_id = self.data('prizeid'),
		winner_id = self.data('winnerid');

	var total_count = parseInt($("#total-count").html()); 

	
	var url = $("#baseurl").attr('content');

	$.get(url + "/admin/lucky-draw/get-winners/"+raffle_id+"/"+prize_id, function(winnerData) {
		$("#current-count").html(winnerData.length);

		if(parseInt(winnerData.length) == total_count) {
			$("#start-draw").hide();
			$("#success-draw").show();
		}

		$.get(url + "/admin/lucky-draw/claim-prize/" + winner_id, function(claimData) {
			
			$("#exampleModal").modal('toggle');

			self.removeClass("claim_prize");
			self.removeClass("btn-success");
			self.addClass('btn-primary');
			self.addClass('show-next-button');
			self.html("<i class='fas fa-check'></i> Claimed (Show Next)");

			$("html, body").animate({ scrollTop: $(document).height() }, 1000);
		});

		$("#winner_id").val(winnerData[0].id);
		// $("#re-draw").show();
	});

});
$(document).on('click', '.claim_prize', function(data) {
	var self = $(this),
		raffle_id = self.data('raffleid'),
		prize_id = self.data('prizeid'),
		winner_id = self.data('winnerid');

	var total_count = parseInt($("#total-count").html()); 

	
	var url = $("#baseurl").attr('content');

	$.get(url + "/admin/lucky-draw/get-winners/"+raffle_id+"/"+prize_id, function(winnerData) {
		$("#current-count").html(winnerData.length);

		if(parseInt(winnerData.length) == total_count) {
			$("#start-draw").hide();
			$("#success-draw").show();
		}

		$.get(url + "/admin/lucky-draw/claim-prize/" + winner_id, function(claimData) {
			
			$("#exampleModal").modal('toggle');

			self.removeClass("claim_prize");
			self.removeClass("btn-success");
			self.addClass('btn-primary');
			self.addClass('show-next-button');
			self.html("<i class='fas fa-check'></i> Claimed (Show Next)");

			$("html, body").animate({ scrollTop: $(document).height() }, 1000);
		});

		$("#winner_id").val(winnerData[0].id);
		// $("#re-draw").show();
	});

});

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

$("#start-draw").click(function() {
	var self = $(this);
	var audio = document.getElementById("successAudio"),
		url = $("#baseurl").attr('content'),
		raffle_id = $("#raffle_id").val(),
		event_id = $("#event_id").val(),
		prize_id = $("#prize_id").val(),
		total_count = parseInt($("#total-count").html()); 

	$("#winner").hide();

	$("#re-draw").hide();
	self.hide();

	$.get(url + '/admin/lucky-draw/event/'+event_id, function(data) {
		console.log(data);
		if(!isEmpty(data)) {
			// audio.play();

			od.update(data.raffle_code);

			$.get(url + '/admin/trigger-draw/'+event_id+'/'+raffle_id+'/'+prize_id+'/' + data.raffle_code + "/" + data.participant.name + "/" + data.participant.employee_number, function(triggerData) {
				console.log(triggerData);
			});

			$.post(url + '/admin/lucky-draw/save-winner', {
				'_token' : $("#token").attr('content'),
				'events_participants_id' : data.id,
				raffle_id,
				prize_id
			}, function(saveData) {
				setTimeout(function() {
					$.get(url + "/admin/lucky-draw/get-winners/"+raffle_id+"/"+prize_id, function(winnerData) {
						$("#current-count").html(winnerData.length);
						if(parseInt(winnerData.length) == total_count) {
							$("#start-draw").hide();
							// $("#success-draw").show();

						}else {
							self.show();
						}
						$("#winner_id").val(winnerData[0].id);
						$("#re-draw").show();
					});

					$("#winner").show();
					$("#winner-name").html(data.participant.name);		
					$("#winner-employee-number").html(data.participant.employee_number);		
				}, 4000);
				
			});
		} else {
			$("#failed-draw").show();
		}
	});
});

$(document).on('click', '#re-draw', function() {
	var button = $(this)
		winner_id = $("#winner_id").val(),
		count = parseInt($("#current-count").html()) - 1,
		url = $("#baseurl").attr('content');

	$("#re-draw").hide();
	$("#success-draw").hide();

	$("#current-count").html(count);

	$.get(url + "/admin/lucky-draw/delete-winner/" + winner_id, function(data) {
		$("#start-draw").trigger('click');
	});
});

$("#show-prizes").click(function() {
	var show = $(this).data('show');

	if(show == 1) {
		$("#show-prizes").data('show', 2);
		$(".prizes-wrap").css({
			'display': 'flex'
		});
	} else {
		$("#show-prizes").data('show', 1);
		$(".prizes-wrap").css({
			'display': 'none'
		});
	}
});

$(".winner-wrap").click(function() {

	var url = $("#baseurl").attr('content');

	var raffle_id = $("#raffle_id").val(),
		prize_id = $("#prize_id").val();

	var html = "";

	$.get(url + "/admin/lucky-draw/get-winners/" + raffle_id + "/" + prize_id, function(data) {
		for(var i = 0; i < data.length; i++) {
			var status_class = "claim_prize btn btn-sm btn-success";
			var status_html = "<i class='fas fa-check'></i> Claim Prize";

			if(data[i].status == 1) {
				status_class = "btn btn-sm btn-primary show-next-button";
				status_html = "<i class='fas fa-check'></i> Claimed (Show Next)";
			}

			html = html + "<tr>" +
		      			"<td>"+data[i].events_participants.participant.salutation + " " +
		      			data[i].events_participants.participant.name +
		      			"</td>" +
		      			"<td>" + data[i].events_participants.participant.employee_number + "</td>" +
		      			"<td>" +
		      				"<a href='javascript:void(0)' data-prizeid='"+prize_id+"' data-raffleid='"+raffle_id+"' data-winnerid='"+data[i].id+"' class='"+status_class+"' style='padding: 1px 5px;'>" +
	      						status_html +
	      					"</a> " +
		      				"<a href='javascript:void(0)' data-winnerid='"+data[0].id+"' class='invalidate btn btn-sm btn-danger' style='padding: 1px 7px;'>" +
	      						"<i class='fas fa-times icon'></i> Re-draw" +
	      					"</a>" +
	      				"</td>" +
		      		"</tr>";
		}
		$("#winners-html").html(html)

		console.log(html);
	});
});

$(document).on('click', '.invalidate', function() {
	var button = $(this)
		count = parseInt($("#current-count").html()) - 1,
		url = $("#baseurl").attr('content');

	$("#winner_id").val(button.data('winnerid'));	

	var	winner_id = $("#winner_id").val();

	$("#re-draw").hide();
	$("#success-draw").hide();

	$("#current-count").html(count);

	$("#exampleModal").modal('toggle');

	$.get(url + "/admin/lucky-draw/delete-winner/" + winner_id, function(data) {
		$("#start-draw").trigger('click');
	});
});