$("#auto-upload").hide();

$("#upload_manually").click(function() {
	if($(this).prop("checked") == true) {
		$("#manual-upload").show();
		$("#auto-upload").hide();
	} else {
		$("#manual-upload").hide();
		$("#auto-upload").show();
	}
});



$("#add-prize").click(function() {
	var html = '<div class="row">' +
					'<div class="col-md-4">' +
						'<label>Prize <span class="required">*</span></label>' +
						'<input type="text" class="form-control prizes" placeholder="What\'s the prize?" placeholder="" />' +
					'</div>' +
					'<div class="col-md-4">' +
						'<label>Number of Winners <span class="required">*</span></label>' +
						'<input type="number" class="form-control number_of_winners" placeholder="How many winners can get this prize?" />' +
					'</div>' +
					'<div class="col-md-4">' +
						'<label>Prize Type <span class="required">*</span></label>' +
						'<select class="form-control type">' +
							'<option value="1">Lucky Draw Prize</option>' +
							'<option value="2">Pre-Draw Prize</option>'
						'</select>'
					'</div>' +
				'</div><hr />';

	$("#prizes .alert").hide();
	$("#prizes").append(html);
});



$("#create-lucky-draw").click(function() {
	var errors = [];
	var lucky_draw_name = $("#lucky_draw_name").val();
	var prizes_check = 0;
	var winners_check = 0;
	var event_id = $("#event_id").val();

	if($("#upload_manually").prop("checked") == false) {
		$("#raffle-form").submit();
	} else {
		$(".prizes").each(function() {
			var self = $(this);

			if(self.val() == "") {
				prizes_check = 1;
			}
		});

		$(".number_of_winners").each(function() {
			var self = $(this);

			if(self.val() == "" || self.val() <= 0) {
				winners_check = 1;
			}
		});

		if(prizes_check == 1) {
			errors.push('Please make sure to include the prizes');
		}

		if(winners_check == 1) {
			errors.push('Please make sure to include number of winners');
		}

		// validate lucky draw name
		if(lucky_draw_name == "") {
			errors.push("Please include a name of this lucky draw.");
		}

		// check if there are errors in array and show
		if(errors.length > 0) {
			var html_errors = "<ul>";

			for(var i=0; i<errors.length; i++) {
				html_errors = html_errors + "<li>" + errors[i] + "</li>";
			}

			html_errors = html_errors + "</ul>";

			$("#errors").html("");
			$("#errors").append(html_errors);
			$("#error-wrap").show();
			window.scrollTo(0, 0);
		} else {
			$("#errors").html("");
			$("#error-wrap").hide();

			var prizes = [];
			var winners = [];
			var type = [];

			$(".prizes").each(function() {
				prizes.push($(this).val());
			});
			$(".number_of_winners").each(function() {
				winners.push($(this).val());
			});
			$(".type").each(function() {
				type.push($(this).val());
			});

			console.log(type);

			

			var file = document.querySelector('#lucky_draw_image').files[0];
			
			var data = {
				lucky_draw_name,
				lucky_draw_background_image: $("#base64textarea").val(),
				prizes,
				winners,
				type,
				event_id,
				shift_personnel: $("#shift_personnel").val(),
				_token: $("#token").attr('content'),
				upload_manually: $("#upload_manually").val()
			}

			console.log(data);

			var url = $("#baseurl").attr('content');

			$.post(url + "/admin/events/"+event_id+"/lucky-draw", data, function(data) {
				window.location = url + "/admin/events/"+event_id+"/lucky-draw?success=true";
			});
		}
	}
});

$("#update-lucky-draw").click(function() {
	var raffle_id = $(this).data('id');
	var errors = [];
	var lucky_draw_name = $("#lucky_draw_name").val();
	var prizes_check = 0;
	var winners_check = 0;
	var event_id = $("#event_id").val();

	$(".prizes").each(function() {
		var self = $(this);

		if(self.val() == "") {
			prizes_check = 1;
		}
	});

	$(".number_of_winners").each(function() {
		var self = $(this);

		if(self.val() == "" || self.val() <= 0) {
			winners_check = 1;
		}
	});

	if(prizes_check == 1) {
		errors.push('Please make sure to include the prizes');
	}

	if(winners_check == 1) {
		errors.push('Please make sure to include number of winners');
	}

	// validate lucky draw name
	if(lucky_draw_name == "") {
		errors.push("Please include a name of this lucky draw.");
	}

	// check if there are errors in array and show
	if(errors.length > 0) {
		var html_errors = "<ul>";

		for(var i=0; i<errors.length; i++) {
			html_errors = html_errors + "<li>" + errors[i] + "</li>";
		}

		html_errors = html_errors + "</ul>";

		$("#errors").html("");
		$("#errors").append(html_errors);
		$("#error-wrap").show();
		window.scrollTo(0, 0);
	} else {
		$("#errors").html("");
		$("#error-wrap").hide();

		var prizes = [];
		var winners = [];
		var prizes_id = [];
		var type = [];

		$(".prizes").each(function() {
			prizes.push($(this).val());
		});
		$(".prizes_id").each(function() {
			prizes_id.push($(this).val());
		});
		$(".number_of_winners").each(function() {
			winners.push($(this).val());
		});
		$(".type").each(function() {
			type.push($(this).val());
		});

		

		var file = document.querySelector('#lucky_draw_image').files[0];
		
		var data = {
			_method: "PUT",
			lucky_draw_name,
			lucky_draw_background_image: $("#base64textarea").val(),
			prizes,
			prizes_id,
			winners,
			type,
			event_id,
			shift_personnel: $("#shift_personnel").val(),
			_token: $("#token").attr('content')
		}

		console.log(data);

		var url = $("#baseurl").attr('content');

		$.post(url + "/admin/events/"+event_id+"/lucky-draw/"+raffle_id, data, function(data) {
			window.location = url + "/admin/events/"+event_id+"/lucky-draw/"+raffle_id+"/edit?success=true";
		});
	}
});

var handleFileSelect = function(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
        var reader = new FileReader();

        reader.onload = function(readerEvt) {
            var binaryString = readerEvt.target.result;
            document.getElementById("base64textarea").value = btoa(binaryString);
        };

        reader.readAsBinaryString(file);
    }
};

if (window.File && window.FileReader && window.FileList && window.Blob) {
    document.getElementById('lucky_draw_image').addEventListener('change', handleFileSelect, false);
} else {
    alert('The File APIs are not fully supported in this browser.');
}
