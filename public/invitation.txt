Dear [participant-salutation] [participant-name],
<br /><br />
[event-banner]
<br /><br />
On behalf of the management team and Organising Committee, it gives us great pleasure to invite you to the [event-title]
<br /><br />
<strong>Event details are as follows:</strong>
<table>
	<tr>
		<td style='color: blue;'>Date:</td>
		<td style='color: blue;'>
			[event-date]
		</td>
	</tr>
	<tr>
		<td style='color: blue;'>Time:</td>
		<td style='color: blue;'>
			[event-time]
		</td>
	</tr>
	<tr>
		<td style='color: blue;'>Venue:</td>
		<td style='color: blue;'>
			[event-venue]
		</td>
	</tr>
</table>
<br />
<br />
To register for the event, simply click [event-link, 'here'] for registration page.
<br />
<br />
Registration closes on [event-reg-close] and we encourage you to sign up as early as possible.
<br /><br />
If you encounter problems registering, please contact us at [event-contact-email]
<br />
<br />
We look forward to your confirmation and to an enjoyable evening!
<br /><br />
Warm Regards,<br />
[event-title] Organising Committee